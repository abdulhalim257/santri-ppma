<?php require('sadmin/konek.php');
if (isset($_POST['login'])) {
  session_start();
  $username       = $_POST['username'];
  $password       = md5($_POST['password']);

  $result = mysqli_query($kon, "SELECT * FROM santri WHERE nis='$username' and password = '$password' and status_santri='Aktif'");
  $row = mysqli_num_rows($result);
  if ($row) {
    $data = mysqli_fetch_array($result);
    $_SESSION["nis"] = $data['nis'];
    $_SESSION["angkatan"] = $data['angkatan'];
    $_SESSION["nama_lengkap"] = $data['nama_lengkap'];
    $_SESSION["foto"] = $data['foto'];
    $_SESSION["status_santri"] = $data['status_santri'];
    $_SESSION["nickname"] = "Santri";
    header('location: santri/index.php');
  } else {
    echo "<script type='text/javascript'>alert(\"Login gagal, NIS/password salah!\");window.location=\"index.php\";</script>";
  }
}
?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Santri PPMA | Login</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition login-page">
  <div class="login-box">
    <div class="login-logo">
      <a href="index.php"><b>Santri </b>PPMA</a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
      <p class="login-box-msg">Silahkan login menggunakan akun santri</p>

      <form action="" method="post">
        <div class="form-group has-feedback">
          <input type="text" class="form-control" placeholder="NIS" name="username" required>
          <span class="glyphicon glyphicon-user form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
          <input type="password" class="form-control" placeholder="Password" name="password" required>
          <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="row">
          <div class="col-xs-8">
            <div class="checkbox icheck">
              <label>
                <input type="checkbox"> Remember Me
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-xs-4">
            <input type="submit" name='login' class="btn btn-primary btn-block btn-flat" value='Login'>
          </div>
          <!-- /.col -->
        </div>
      </form>

      <!-- <div class="social-auth-links text-center">
      <p>- OR -</p>
      <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using
        Facebook</a>
      <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using
        Google+</a>
    </div> -->
      <!-- /.social-auth-links -->

      <a href="" data-toggle="modal" data-target="#modal-default">Lupa password?</a><br>
      <a href="loadmin.php">Login Sebagai Admin</a><br>
      <!-- <a href="register.html" class="text-center">Register a new membership</a> -->

    </div>
    <!-- /.login-box-body -->
  </div>
  <!-- /.login-box -->

  <div class="modal fade" id="modal-default">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Lupa Password?</h4>
        </div>
        <div class="modal-body">
          <p>Silahkan hubungi admin melalui kontak dibawah ini:</p>
          <div class="text-center">
            <a href="https://www.facebook.com/MAWARASSALAM" target="()" class="btn btn-social-icon btn-facebook"><span class="fa fa-facebook"></span></a>
            <!-- <a href="https://gmail.com/Pp.manbaulanwar@gmail.com/" target="()" class="btn btn-social-icon btn-google"><span class="fa fa-google"></span></a> -->
            <a href="https://www.instagram.com/ppma_mawar/" target="()" class="btn btn-social-icon btn-instagram"><span class="fa fa-instagram"></span></a>
            <!-- <a href="https://www.twitter.com/" target="()" class="btn btn-social-icon btn-twitter"><span class="fa fa-twitter"></span></a> -->
            <a href="https://wa.me/+6282241685027/" target="()" class="btn btn-social-icon btn-twitter" style="background-color:#06c739"><span class="fa fa-whatsapp"></span></a>
          </div>
          <!-- /.row -->
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
          <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

  <!-- jQuery 3 -->
  <script src="bower_components/jquery/dist/jquery.min.js"></script>
  <!-- Bootstrap 3.3.7 -->
  <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- iCheck -->
  <script src="plugins/iCheck/icheck.min.js"></script>
  <script>
    $(function() {
      $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' /* optional */
      });
    });
  </script>
</body>

</html>