<?php require_once('atas.php');
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Dashboard
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Tabungan</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Daftar Tabungan</h3>
                </div>
                <div class="box-body">
                    <div class="table-responsive" style="border: 0px solid #ddd;">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Id Tabungan</th>
                                    <th>Tanggal</th>
                                    <th>Jumlah</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;
                                $s = mysqli_query($kon, "select * from tabungan  where nis='$_SESSION[nis]' order by tgl_tab desc");

                                while ($f = mysqli_fetch_array($s)) {
                                    $hasil_rupiah = "Rp " . number_format($f['jumlah_tab'], 2, ',', '.');
                                    echo "
                                <tr>
                                    <td>$no</td>
                                    <td><b>$f[id_tabungan]</b></td>
                                    <td>" . tgl_indo($f['tgl_tab']) . "</td>
                                    <td>$hasil_rupiah</td>
                                </tr>
                                ";
                                    $no++;
                                }
                                $s = mysqli_query($kon, "select sum(jumlah_tab) as jtab from tabungan  where nis='$_SESSION[nis]' order by tgl_tab desc");

                                $f = mysqli_fetch_array($s);
                                ?>
                            </tbody>
                        </table>
                        <h4><b>Total : </b><?php echo "Rp " . number_format($f['jtab'], 2, ',', '.'); ?></h4>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
        </section>
        <!-- /.Left col -->
    </div>
    <!-- /.row (main row) -->

</section>
<?php require_once('bawah.php'); ?>