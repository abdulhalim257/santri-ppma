<?php require_once('atas.php');
$s = mysqli_query($kon, "select * from santri left join jenjang on santri.id_jenjang=jenjang.id_jenjang where santri.nis='$_SESSION[nis]'");
// echo "$_SESSION[nis]";
$f = mysqli_fetch_array($s);
if (isset($_POST["save"])) {
    $alamat = $_POST['alamat'];
    $nm_ayah = $_POST['nm_ayah'];
    $nm_ibu = $_POST['nm_ibu'];
    $no_telp_wali = $_POST['no_telp_wali'];

    $passwordlm = $_POST['passwordlm'];
    $passwordl = md5($_POST['passwordl']);
    $password1 = md5($_POST['password1']);
    $password2 = md5($_POST['password2']);
    if (empty($_POST['passwordl'])) {
        $a = mysqli_query($kon, "update santri set
                    alamat='$alamat',
                    nm_ayah='$nm_ayah',
                    nm_ibu='$nm_ibu',
                    no_telp_wali='$no_telp_wali'
                    where nis='$_SESSION[nis]'");
        echo "<script type='text/javascript'>alert(\"Berhasil!\");window.location=\"esantri.php\";</script>";
    } elseif ($passwordlm == $passwordl && $password1 == $password2) {
        $a = mysqli_query($kon, "update santri set
                    alamat='$alamat',
                    nm_ayah='$nm_ayah',
                    nm_ibu='$nm_ibu',
                    no_telp_wali='$no_telp_wali',
                    password='$password2'
                    where nis='$_SESSION[nis]'");
        echo "<script type='text/javascript'>alert(\"Berhasil!\");window.location=\"esantri.php\";</script>";
    } else {
        echo "<script type='text/javascript'>alert(\"Gagal!\");history.back();</script>";
    }
}
$d = date('m/d/Y', strtotime($f['tgl_lhr']));
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Dashboard
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Santri</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <section class="col-lg-4 connectedSortable">

            <!-- Profile Image -->
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <img class="profile-user-img img-responsive" src="../files/<?php echo $f['foto']; ?>" alt="User profile picture">

                    <h3 class="profile-username text-center"><?php echo $f['nama_lengkap']; ?></h3>

                    <p class="text-muted text-center"><?php echo $f['nis']; ?></p>
                    <strong><i class="fa fa-book margin-r-5"></i> Profil</strong>

                    <p class="text-muted">
                        <?php if ($f['jkel'] == 'L') {
                            echo "Laki-laki";
                        } else {
                            echo "Perempuan";
                        } ?>
                        <br>
                        <?php echo $f['tempat_lhr']; ?>, <?php echo tgl_indo($f['tgl_lhr']); ?>
                        <br>
                        <?php echo $f['alamat']; ?>
                    </p>
                    <hr>

                    <strong><i class="fa fa-file-text-o margin-r-5"></i> Notes</strong>

                    <p>
                    <div class="table-responsive" style="border: 0px solid #ddd;">
                        <table id="example1" class="table table-bordered table-striped">
                            <tr>
                                <td>Nama Ayah</td>
                                <td>:</td>
                                <td><?php echo $f['nm_ayah']; ?></td>
                            </tr>
                            <tr>
                                <td>Nama Ibu</td>
                                <td>:</td>
                                <td><?php echo $f['nm_ibu']; ?></td>
                            </tr>
                            <tr>
                                <td>Jenjang Pendidikan </td>
                                <td>:</td>
                                <td><?php echo $f['jenjang']; ?></td>
                            </tr>
                            <tr>
                                <td>No Telp Wali</td>
                                <td>:</td>
                                <td><?php echo $f['no_telp_wali']; ?></td>
                            </tr>
                            <tr>
                                <td>Status</td>
                                <td>:</td>
                                <td><?php echo $f['status_santri']; ?></td>
                            </tr>
                        </table>
                    </div>
                    </p>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </section>
        <section class="col-lg-8 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-user"></i> Edit Santri</h3>
                </div>
                <form action="" method="POST" enctype="multipart/form-data">
                    <div class="box-body">
                        <div class="form-group">
                            <label>Alamat</label>
                            <input type="text" name='alamat' class="form-control" placeholder="Masukkan Alamat" value='<?php echo $f['alamat']; ?>' required>
                        </div>
                        <div class="form-group">
                            <label>Nama Ayah</label>
                            <input type="text" name='nm_ayah' class="form-control" placeholder="Masukkan Nama Ayah" value='<?php echo $f['nm_ayah']; ?>' required>
                        </div>
                        <div class="form-group">
                            <label>Nama Ibu</label>
                            <input type="text" name='nm_ibu' class="form-control" placeholder="Masukkan Nama Ibu" value='<?php echo $f['nm_ibu']; ?>' required>
                        </div>
                        <div class="form-group">
                            <label>No Telp Wali</label>
                            <input type="text" name='no_telp_wali' class="form-control" placeholder="Masukkan No Telp Wali" value='<?php echo $f['no_telp_wali']; ?>' required>
                        </div>
                        <div class="form-group">
                            <label>Password Lama</label> *
                            <input type="hidden" name='passwordlm' class="form-control" placeholder="Masukkan Password" value="<?php echo $f['password']; ?>">
                            <input type="text" name='passwordl' class="form-control" placeholder="Masukkan Password">
                        </div>
                        <div class="form-group">
                            <label>Password Baru</label> *
                            <input type="text" name='password1' class="form-control" placeholder="Masukkan Password Baru">
                        </div>
                        <div class="form-group">
                            <label>Konfirmasi Password</label> *
                            <input type="password" name='password2' class="form-control" placeholder="Masukkan Konfirmasi Password">
                        </div>
                        * Jika tidak dirubah biarkan kosong saja.<br>
                        ** Jika ingin melakukan perubahan diluar data ini silahkan hubungi admin PPMA.
                    </div>
                    <div class="box-footer">
                        <input type="submit" name='save' class="btn btn-primary" value="Update">
                        <input type="Reset" class="btn btn-danger" value="Reset">
                        <a href="" data-toggle="modal" data-target="#modal-default"><button type="button" class="btn btn-info">Hubungi kami</button></a>
                    </div>
                </form>
                <!-- /.box-body -->
            </div>
        </section>
        <!-- /.Left col -->
    </div>
    <!-- /.row (main row) -->

</section>

<div class="modal fade" id="modal-default">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Lupa Password?</h4>
            </div>
            <div class="modal-body">
                <p>Silahkan hubungi admin melalui kontak dibawah ini:</p>
                <div class="text-center">
                    <a href="https://www.facebook.com/MAWARASSALAM" target="()" class="btn btn-social-icon btn-facebook"><span class="fa fa-facebook"></span></a>
                    <!-- <a href="https://gmail.com/Pp.manbaulanwar@gmail.com/" target="()" class="btn btn-social-icon btn-google"><span class="fa fa-google"></span></a> -->
                    <a href="https://www.instagram.com/ppma_mawar/" target="()" class="btn btn-social-icon btn-instagram"><span class="fa fa-instagram"></span></a>
                    <!-- <a href="https://www.twitter.com/" target="()" class="btn btn-social-icon btn-twitter"><span class="fa fa-twitter"></span></a> -->
                    <a href="https://wa.me/+6282241685027/" target="()" class="btn btn-social-icon btn-twitter" style="background-color:#06c739"><span class="fa fa-whatsapp"></span></a>
                </div>
                <!-- /.row -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<?php require_once('bawah.php'); ?>