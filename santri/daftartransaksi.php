<?php require_once('atas.php');
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Dashboard
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Transaksi</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Daftar Transaksi</h3>
                </div>
                <div class="box-body">
                    <div class="table-responsive" style="border: 0px solid #ddd;">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Id Trans</th>
                                    <th>Tanggal</th>
                                    <th>Jumlah</th>
                                    <th>Jenis Trans</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;
                                $s = mysqli_query($kon, "select * from trans left join santri on trans.nis=santri.nis left join jtrans on trans.id_jtrans=jtrans.id_jtrans  where santri.nis='$_SESSION[nis]' order by tgl_trans desc");


                                while ($f = mysqli_fetch_array($s)) {
                                    $hasil_rupiah = "Rp " . number_format($f['jumlah_trans'], 2, ',', '.');
                                    echo "
                            <tr>
                                <td>$no</td>
                                <td><b>$f[id_trans]</b></td>
                                <td>" . tgl_indo($f['tgl_trans']) . "</td>
                                <td>$hasil_rupiah</td>
                                <td>$f[ket_jtrans]</td>
                            </tr>
                            ";
                                    $no++;
                                }
                                $s = mysqli_query($kon, "select sum(jumlah_trans) as jtrans from trans left join santri on trans.nis=santri.nis left join jtrans on trans.id_jtrans=jtrans.id_jtrans where santri.nis='$_SESSION[nis]' order by tgl_trans desc");

                                $f = mysqli_fetch_array($s);
                                ?>
                            </tbody>
                        </table>
                        <h4><b>Total : </b><?php echo "Rp " . number_format($f['jtrans'], 2, ',', '.'); ?></h4>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
        </section>
        <!-- /.Left col -->
    </div>
    <!-- /.row (main row) -->

</section>
<?php require_once('bawah.php'); ?>