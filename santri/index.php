<?php require_once('atas.php'); ?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Dashboard
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Dashboard</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <!-- Small boxes (Stat box) -->
  <div class="row">
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-green">
        <div class="inner">
          <?php
          $j = mysqli_query($kon, "select count(nis) as niss from tabungan where nis='$_SESSION[nis]'");
          $jj = mysqli_fetch_array($j);
          ?>
          <h3><?php echo $jj['niss']; ?><sup style="font-size: 20px"> Kali</sup></h3>

          <p>Tabungan</p>
        </div>
        <div class="icon">
          <i class="fa fa-money"></i>
        </div>
        <a href="daftartabungan.php" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-yellow">
        <div class="inner">
          <?php
          $j = mysqli_query($kon, "select count(nis) as niss from trans where nis='$_SESSION[nis]'");
          $jj = mysqli_fetch_array($j);
          ?>
          <h3><?php echo $jj['niss']; ?><sup style="font-size: 20px"> Kali</sup></h3>

          <p>Transaksi</p>
        </div>
        <div class="icon">
          <i class="fa fa-usd"></i>
        </div>
        <a href="daftartransaksi.php" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-red">
        <div class="inner">
          <?php
          $j = mysqli_query($kon, "select count(nis) as niss from tagihan where nis='$_SESSION[nis]'");
          $jj = mysqli_fetch_array($j);
          ?>
          <h3><?php echo $jj['niss']; ?><sup style="font-size: 20px"> Buah</sup></h3>

          <p>Tagihan</p>
        </div>
        <div class="icon">
          <i class="fa fa-list"></i>
        </div>
        <a href="daftartagihan.php" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-aqua">
        <div class="inner">
          <h3><sup style="font-size: 20px"><?php echo "$_SESSION[nickname]"; ?></sup></h3>
          <p>Profil</p>
        </div>
        <div class="icon">
          <i class="ion ion-person-add"></i>
        </div>
        <a href="esantri.php?e=<?php echo "$_SESSION[nis]"; ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
  </div>
  <!-- /.row -->
  <!-- Main content -->
  <section class="content">
    <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Selamat Datang!</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
            <i class="fa fa-minus"></i></button>
          <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fa fa-times"></i></button>
        </div>
      </div>
      <div class="box-body">
        Selamat datang diaplikasi Santri PPMA, aplikasi ini akan membantu anda dalam mengelola keuangan santri. Selamat Mencoba, :)
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </section>
  <!-- /.content -->
</section>
<?php require_once('bawah.php'); ?>