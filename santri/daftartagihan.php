<?php require_once('atas.php');
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Dashboard
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Tagihan</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Daftar Tagihan</h3>
                </div>
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Bulan</th>
                                <th>Syahriah</th>
                                <th>Kos Makan</th>
                                <th>Sekolah</th>
                                <th>Ket</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            $s = mysqli_query($kon, "select * from tagihan left join santri on tagihan.nis=santri.nis where santri.nis='$_SESSION[nis]' order by tagihan.waktu_tagihan desc");
                            while ($f = mysqli_fetch_array($s)) {
                                $td = substr($f['waktu_tagihan'], 5, 2);
                                switch ($td) {

                                    case '01':
                                        $tdd = "Januari";
                                        break;

                                    case '02':
                                        $tdd = "Februari";
                                        break;

                                    case '03':
                                        $tdd = "Maret";
                                        break;

                                    case '04':
                                        $tdd = "April";
                                        break;

                                    case '05':
                                        $tdd = "Mei";
                                        break;

                                    case '06':
                                        $tdd = "Juni";
                                        break;

                                    case '07':
                                        $tdd = "Juli";
                                        break;

                                    case '08':
                                        $tdd = "Agustus";
                                        break;

                                    case '09':
                                        $tdd = "September";
                                        break;

                                    case '10':
                                        $tdd = "Oktober";
                                        break;

                                    case '11':
                                        $tdd = "November";
                                        break;

                                    case '12':
                                        $tdd = "Desember";
                                        break;

                                    default:
                                        # code...
                                        break;
                                }
                                echo "
                                    <tr>
                                        <td>$no</td>
                                        <td>$tdd " . substr($f['waktu_tagihan'], 0, 4) . "</td>
                                        <td>Rp " . number_format($f['syahriah'], 2, ',', '.') . "</td>
                                        <td>Rp " . number_format($f['kos_makan'], 2, ',', '.') . "</td>
                                        <td>Rp " . number_format($f['sekolah'], 2, ',', '.') . "</td>
                                        <td>$f[ket]</td>
                                    </tr>
                                    ";
                                $no++;
                            }
                            $s = mysqli_query($kon, "select sum(syahriah) as jsyahriah, sum(sekolah) as jsekolah, sum(kos_makan) as jkos_makan from tagihan left join santri on tagihan.nis=santri.nis where santri.nis='$_SESSION[nis]' order by tagihan.waktu_tagihan desc");

                            $f = mysqli_fetch_array($s);
                            $total = $f['jsyahriah'] + $f['jsekolah'] + $f['jkos_makan'];
                            ?>
                        </tbody>
                    </table>
                    <h4><b>Total : </b><?php echo "Rp " . number_format($total, 2, ',', '.'); ?></h4>
                </div>
                <!-- /.box-body -->
            </div>
        </section>
        <!-- /.Left col -->
    </div>
    <!-- /.row (main row) -->

</section>
<?php require_once('bawah.php'); ?>