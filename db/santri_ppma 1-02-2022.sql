/*
 Navicat Premium Data Transfer

 Source Server         : localhost1
 Source Server Type    : MySQL
 Source Server Version : 50733
 Source Host           : localhost:3306
 Source Schema         : santri_ppma

 Target Server Type    : MySQL
 Target Server Version : 50733
 File Encoding         : 65001

 Date: 01/03/2022 19:32:15
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin`  (
  `id_admin` char(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `no_telp` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `username` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `password` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `level` enum('adminl','adminp','sadmin') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `foto` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `status_admin` enum('Aktif','Tidak Aktif') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_admin`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES ('1577953383', '08123432142', 'Muhammad Irham', '21232f297a57a5a743894a0e4a801fc3', 'sadmin', '1599789171_Koala.jpg', 'Aktif');
INSERT INTO `admin` VALUES ('1595507881', '08586899356', 'M Irham', 'e10adc3949ba59abbe56e057f20f883e', 'sadmin', '1595507881_0034.jpg', 'Aktif');
INSERT INTO `admin` VALUES ('1623288261', '081252459496', 'jorklowor', '92973c586fbf487aaa88b3b95077a8b6', 'adminl', '1623288261_nodejs-wallpaper-nsfw.jpg', 'Aktif');
INSERT INTO `admin` VALUES ('1623296882', '081252459497', 'tiara', 'd41d8cd98f00b204e9800998ecf8427e', 'adminp', '1623296882_195150235_1125174814644180_3060114202340485270_n.jpg', 'Aktif');

-- ----------------------------
-- Table structure for answers
-- ----------------------------
DROP TABLE IF EXISTS `answers`;
CREATE TABLE `answers`  (
  `id_answer` int(30) NOT NULL AUTO_INCREMENT,
  `answer` json NOT NULL,
  `id_survey_set` int(30) NOT NULL,
  `Respon` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_answer`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of answers
-- ----------------------------

-- ----------------------------
-- Table structure for jenjang
-- ----------------------------
DROP TABLE IF EXISTS `jenjang`;
CREATE TABLE `jenjang`  (
  `id_jenjang` bigint(20) NOT NULL AUTO_INCREMENT,
  `jenjang` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `syahriah` bigint(20) NOT NULL,
  PRIMARY KEY (`id_jenjang`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of jenjang
-- ----------------------------
INSERT INTO `jenjang` VALUES (2, 'SMA/MAN', 120000);
INSERT INTO `jenjang` VALUES (3, 'SMP/MTs', 150000);

-- ----------------------------
-- Table structure for jtrans
-- ----------------------------
DROP TABLE IF EXISTS `jtrans`;
CREATE TABLE `jtrans`  (
  `id_jtrans` int(11) NOT NULL AUTO_INCREMENT,
  `ket_jtrans` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `status_jtrans` enum('Aktif','Tidak Aktif') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_jtrans`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of jtrans
-- ----------------------------
INSERT INTO `jtrans` VALUES (1, 'Saldo Awal', 'Aktif');
INSERT INTO `jtrans` VALUES (2, 'Uang Saku', 'Aktif');
INSERT INTO `jtrans` VALUES (3, 'Makan', 'Aktif');
INSERT INTO `jtrans` VALUES (4, 'Beli Sabun', 'Aktif');

-- ----------------------------
-- Table structure for pembayaran
-- ----------------------------
DROP TABLE IF EXISTS `pembayaran`;
CREATE TABLE `pembayaran`  (
  `id_bayar` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_pembayaran` bigint(20) NOT NULL,
  `nominal` bigint(20) NOT NULL,
  `waktu` datetime NOT NULL,
  `create_by` char(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_bayar`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of pembayaran
-- ----------------------------

-- ----------------------------
-- Table structure for questions
-- ----------------------------
DROP TABLE IF EXISTS `questions`;
CREATE TABLE `questions`  (
  `id_question` int(30) NOT NULL AUTO_INCREMENT,
  `form_type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `question` json NOT NULL,
  `id_survey_set` int(30) NOT NULL,
  `id_admin` int(11) NULL DEFAULT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_question`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of questions
-- ----------------------------

-- ----------------------------
-- Table structure for santri
-- ----------------------------
DROP TABLE IF EXISTS `santri`;
CREATE TABLE `santri`  (
  `nis` int(11) NOT NULL,
  `angkatan` int(11) NOT NULL,
  `nama_lengkap` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tempat_lhr` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tgl_lhr` date NOT NULL,
  `jkel` enum('L','P') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `alamat` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nm_ayah` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nm_ibu` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `no_telp_wali` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `foto` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `create_by` char(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `create_waktu` datetime NULL DEFAULT NULL,
  `edit_by` char(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `edit_waktu` datetime NULL DEFAULT NULL,
  `status_santri` enum('Aktif','Tidak Aktif') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_jenjang` bigint(20) NOT NULL,
  `password` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`nis`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of santri
-- ----------------------------
INSERT INTO `santri` VALUES (201410001, 2014, 'Syarif Hidayatullah', 'Pekalongan', '2000-04-07', 'P', 'Karangsari, Sidosari, Kesesi, Pekalongan', 'daus', 'tarsuny', '0', '1579794983_syarif hidayatullah.jpg', '1578490768', '2020-01-23 04:56:23', '', '0000-00-00 00:00:00', 'Aktif', 2, '');
INSERT INTO `santri` VALUES (201510001, 2015, 'Adam Fatihan Lutfi', 'Wonosobo', '2001-09-02', 'L', 'Luwihan, Kalikajar, Wonosobo', 'Untung Sugiono', 'Marfuah', '081390077609', '1578230193_adam.jpg', '1577953383', '2020-01-05 02:16:33', '', '0000-00-00 00:00:00', 'Aktif', 2, '');
INSERT INTO `santri` VALUES (201510002, 2015, 'lukman', 'Wonosobo', '1990-01-01', 'L', 'Dalangan, Purwojati, Kertek, Wonosobo', 'minato', 'kusina', '0000', '1578489472_luqman setyawan.jpg', '1577953383', '2020-01-08 02:17:52', '', '0000-00-00 00:00:00', 'Aktif', 2, '');
INSERT INTO `santri` VALUES (201510003, 2015, 'Hafid Nur Karim', 'Wonosobo', '2005-01-01', 'L', 'Ngariman, Purwojati, Kertek, WOnosobo', 'Kambari', 'Khoziah', '00', '1579245709_hafid.jpg', '1577953383', '2020-01-17 08:21:49', '', '0000-00-00 00:00:00', 'Aktif', 3, '');
INSERT INTO `santri` VALUES (201610001, 2016, 'Angga Setiyawan ', 'Aceh', '2006-04-15', 'L', 'Jakarta', 'Said Hasbi', 'Latiah', '087888376651', '1578225685_angga.jpg', '1577953383', '2020-01-05 01:01:25', '', '0000-00-00 00:00:00', 'Aktif', 3, '');
INSERT INTO `santri` VALUES (201710001, 2017, 'Tubagus Much Baron Q', 'Pekalongan', '2004-01-04', 'L', 'Paesan Tengah, Kedungwuni, Pekalongan', 'Abdullah', 'Aida Afidah', '00', '1578228833_baron.jpg', '1577953383', '2020-01-05 01:53:53', '1577953383', '2020-01-05 02:06:31', 'Aktif', 3, '');
INSERT INTO `santri` VALUES (201710002, 2017, 'A Kharizta Nudzik', 'Pekalongan', '2004-08-04', 'L', 'Capgawen Selatan, Kedungwuni, Pekalongan', 'Dzikaun', 'Nurul Ulum', '08562586195', '1578228963_aris.jpg', '1577953383', '2020-01-05 01:56:03', '', '0000-00-00 00:00:00', 'Aktif', 3, '');
INSERT INTO `santri` VALUES (201710003, 2017, 'Yogi Ardiyan', 'Wonosobo', '2004-03-04', 'L', 'Dempel, Pegerrejo, Kertek, Wonosobo', 'Sugino', 'Jamiah', '083875758359', '1578229066_yogi.jpg', '1577953383', '2020-01-05 01:57:46', '', '0000-00-00 00:00:00', 'Aktif', 3, '');
INSERT INTO `santri` VALUES (201710004, 2017, 'Ahmad Nazli Mursidan', 'Wonosobo', '2002-05-23', 'L', 'Bomerto, Wonosobo', 'supangat', 'Nur Fadhilah', '00', '1578236405_pangak.jpg', '1577953383', '2020-01-05 04:00:05', '1578490768', '2020-01-08 02:47:13', 'Aktif', 2, '');
INSERT INTO `santri` VALUES (201710005, 2017, 'Feri Ardiyanto', 'Wonosobo', '2004-12-15', 'L', 'Dumpil, Tegal Ombo , Kalikajar, Wonosobo', 'suroso', 'Turni', '085329504517', '1579795093_feri.jpg', '1578490768', '2020-01-23 04:58:13', '', '0000-00-00 00:00:00', 'Aktif', 2, '');
INSERT INTO `santri` VALUES (201810002, 2018, 'Adam Firmansyah', 'Wonosobo', '2005-03-17', 'L', 'Bomerto, Wonosobo', 'Badrudin', 'Laila Chodariah', '00', '1578227194_adam.jpg', '1577953383', '2020-01-05 01:26:34', '', '0000-00-00 00:00:00', 'Aktif', 3, '');
INSERT INTO `santri` VALUES (201810003, 2018, 'Aghus Jalaludin', 'Wonosobo', '2006-02-09', 'L', 'Bomerto, Wonosobo', 'm. amin', 'Jumiah', '085289412601', '1578227263_aghus.jpg', '1577953383', '2020-01-05 01:27:43', '1577953383', '2020-01-05 01:28:01', 'Aktif', 3, '');
INSERT INTO `santri` VALUES (201810004, 2018, 'Dirga Badruz Zaman', 'Batang', '2006-03-31', 'L', 'Sekidang, donorejo, Limpung, Batang', 'Zamrodin', 'Sokhifatun', '082313919491', '1578227410_dirga.jpg', '1577953383', '2020-01-05 01:30:10', '', '0000-00-00 00:00:00', 'Aktif', 3, '');
INSERT INTO `santri` VALUES (201810005, 2018, 'Ega Nur Rohmatullah', 'Wonosobo', '2005-07-02', 'L', 'Dalangan, Purwojati, Kertek, Wonosobo', 'Nur Rohman', 'Turminah', '085200488218', '1578227503_ega.jpg', '1577953383', '2020-01-05 01:31:43', '', '0000-00-00 00:00:00', 'Aktif', 3, '');
INSERT INTO `santri` VALUES (201810006, 2018, 'Ficho Alexanderia', 'Temanggung', '2005-12-05', 'L', 'Bakal Campurejo Tretep Temanggung', 'Abidin', 'Rohasih', '085803977927', '1578227607_ficho.jpg', '1577953383', '2020-01-05 01:33:27', '', '0000-00-00 00:00:00', 'Aktif', 3, '');
INSERT INTO `santri` VALUES (201810007, 2018, 'Jilbran Bahtiar Alam', 'Batang', '2006-01-22', 'L', 'Kecing, Donorejo, Limpung, Batang', 'Ahmad Harjo', 'Tintin Winarni', '085290043357', '1578227696_jilbran.jpg', '1577953383', '2020-01-05 01:34:56', '', '0000-00-00 00:00:00', 'Aktif', 3, '');
INSERT INTO `santri` VALUES (201810008, 2018, 'M Aqil Lazuardi Najib', 'Pekalongan', '2006-06-05', 'L', 'Paesan Tengah, Kedungwuni, Pekalongan', 'Abdullah', 'Aida Afidah', '085642805002', '1578227791_aqil.jpg', '1577953383', '2020-01-05 01:36:31', '', '0000-00-00 00:00:00', 'Aktif', 3, '');
INSERT INTO `santri` VALUES (201810009, 2018, 'M Sidhiq Saada', 'Wonosobo', '2005-04-01', 'L', 'Bomerto, Wonosobo', 'Sarman', 'Khoziah', '00', '1578227891_sidhiq.jpg', '1577953383', '2020-01-05 01:38:11', '1577953383', '2020-07-15 02:16:44', 'Aktif', 3, '');
INSERT INTO `santri` VALUES (201810010, 2018, 'M yafi Asrof Maulidi', 'Wonosobo', '2005-05-27', 'L', 'Luwihan, Kalikajar, Wonosobo', 'Sabarudin', 'Walimah', '083108138510', '1578228008_yafi.jpg', '1577953383', '2020-01-05 01:40:08', '', '0000-00-00 00:00:00', 'Aktif', 3, '');
INSERT INTO `santri` VALUES (201810011, 2018, 'Nur Abadi', 'Temanggung', '2005-09-22', 'L', 'Bendan Kebonsari wonoboyo Temanggung', 'Kambari', 'Wahyuni', '00', '1578228078_resa alfiansyah.jpg', '1577953383', '2020-01-05 01:41:18', '', '0000-00-00 00:00:00', 'Aktif', 3, '');
INSERT INTO `santri` VALUES (201810012, 2018, 'Wildan Anis Fuadi', 'Batang ', '2006-07-17', 'L', 'karanganyar, Limpung, Batang', 'mukhlisin', 'Widiyawati', '081391755009', '1578228178_wildan.jpg', '1577953383', '2020-01-05 01:42:58', '', '0000-00-00 00:00:00', 'Aktif', 3, '');
INSERT INTO `santri` VALUES (201810013, 2018, 'Ilham Kholiq', 'Wonosobo', '2005-12-06', 'L', 'Sidodadi, Sindupaten, Kertek, Wonosobo', 'Sahman', 'Tunjiah', '085328049821', '1578229315_ilham k.jpg', '1577953383', '2020-01-05 02:01:55', '', '0000-00-00 00:00:00', 'Aktif', 3, '');
INSERT INTO `santri` VALUES (201810016, 2018, 'chairul ahmad syafruddin', 'Pekalongan', '2006-06-11', 'L', 'Karangsari, Sidosari, Kesesi, Pekalongan', 'daus', 'tarsuny', '082146191502', '1578589050_irul.jpg', '1578490768', '2020-01-09 05:57:30', '', '0000-00-00 00:00:00', 'Aktif', 3, '');
INSERT INTO `santri` VALUES (201810017, 2018, 'Ahmad Refaldi Amin', 'Wonosobo', '1998-10-18', 'L', 'Kebrengan, Mojotengah, Wonosobo', 'Siryono', 'Khasanah', '0', '1579795194_faldi.jpg', '1578490768', '2020-01-23 04:59:54', '', '0000-00-00 00:00:00', 'Aktif', 2, '');
INSERT INTO `santri` VALUES (201810018, 2018, 'Aditya Mahfud S', 'Wonosobo', '2002-03-31', 'L', 'Bomerto, Wonosobo', 'Acak Sunarya', 'Irmah', '00', '1579796013_kletuk.jpg', '1578490768', '2020-01-23 05:12:33', '1578490768', '2020-01-23 05:13:33', 'Aktif', 2, '');
INSERT INTO `santri` VALUES (201810019, 2018, 'M Ilham Arrafat', 'Wonosobo', '2003-02-11', 'L', 'Bumen, Bumirejo, Mojotengah, wonosobo', 'Nur Suharno', 'Nikmah', '000', '1579796101_ilham a.jpg', '1578490768', '2020-01-23 05:15:01', '', '0000-00-00 00:00:00', 'Aktif', 2, '');
INSERT INTO `santri` VALUES (201910001, 2019, 'Ahmad Muzaki', 'Wonosobo', '2005-10-18', 'L', 'Bomerto, Wonosobo', 'Farikhin Bejo Santoso', 'Riani Rahayu', '0', '1578225397_zaki b.jpg', '1577953383', '2020-01-04 02:03:52', '1577953383', '2020-01-05 12:56:37', 'Aktif', 3, '');
INSERT INTO `santri` VALUES (201910002, 2019, 'Ahmad Nur Faizin', 'Wonosobo', '2006-04-14', 'L', 'Sumberjati, wonosobo', 'Hermiyanto', 'Remben', '0', '1578225532_faizin.jpg', '1577953383', '2020-01-04 02:09:42', '1577953383', '2020-01-05 12:58:52', 'Aktif', 3, '');
INSERT INTO `santri` VALUES (201910003, 2019, 'Ahmad Riziq Alfatah', 'Pekalongan', '2007-07-07', 'L', 'Karangsari, Sidosari, Kesesi, Pekalongan', 'M. Askuri', 'Rizkiana', '085642665179', '1578225103_labib.jpg', '1577953383', '2020-01-05 12:51:43', '1577953383', '2020-01-05 12:59:23', 'Aktif', 3, '');
INSERT INTO `santri` VALUES (201910004, 2019, 'Anwarul Mazalik', 'Batang', '2006-01-06', 'L', 'Pranten, Bawang, Batang', 'Purwanto', 'Turmi', '0', '1578225864_anwarul.jpg', '1577953383', '2020-01-05 01:04:24', '', '0000-00-00 00:00:00', 'Aktif', 3, '');
INSERT INTO `santri` VALUES (201910005, 2019, 'M. Hanif Annaja', 'Wonosobo', '2006-07-17', 'L', 'Luwihan, Kalikajar, Wonosobo', 'Widodo', 'Zatiah', '082110130328', '1578225982_hanif.jpg', '1577953383', '2020-01-05 01:06:22', '', '0000-00-00 00:00:00', 'Aktif', 3, '');
INSERT INTO `santri` VALUES (201910006, 2019, 'Hikmal Latif Afawa', 'Banjarnegara', '2001-01-01', 'L', 'dsa', 'dsad', 'das', '000', '1578226046_hikmal.jpg', '1577953383', '2020-01-05 01:07:26', '', '0000-00-00 00:00:00', 'Aktif', 3, '');
INSERT INTO `santri` VALUES (201910007, 2019, 'Ifan budiman', 'Batang', '2004-09-18', 'L', 'Pranten, Bawang, Batang', 'Mahruf', 'Slamet', '085742833170', '1578226373_ifan.jpg', '1577953383', '2020-01-05 01:12:53', '1577953383', '2020-01-05 01:15:16', 'Aktif', 3, '');
INSERT INTO `santri` VALUES (201910008, 2019, 'M. Fauzia Rohman', 'Wonosobo', '2006-07-22', 'L', 'Bomerto, Wonosobo', 'Sarman', 'Khoziah', '085328044676', '1578226611_fauzi.jpg', '1577953383', '2020-01-05 01:16:51', '', '0000-00-00 00:00:00', 'Aktif', 3, '');
INSERT INTO `santri` VALUES (201910009, 2019, 'M. Mughni Labib', 'Pekalongan', '2007-07-07', 'L', 'Karangsari, Sidosari, Kesesi, Pekalongan', 'M. Askuri', 'Rizkiana', '085642665179', '1578226697_labib.jpg', '1577953383', '2020-01-05 01:18:17', '', '0000-00-00 00:00:00', 'Aktif', 3, '');
INSERT INTO `santri` VALUES (201910010, 2019, 'M Zaky Al arof', 'Wonosobo', '2006-06-29', 'L', 'Ngariman, Purwojati, Kertek, WOnosobo', 'AMinudin Al arof', 'Indah', '085228139279', '1578226813_zaki ngariman.jpg', '1577953383', '2020-01-05 01:20:13', '1578490768', '2020-01-08 02:46:33', 'Aktif', 3, '');
INSERT INTO `santri` VALUES (201910011, 2019, 'Rendi Nur Arifin', 'Wonosobo', '2006-11-12', 'L', 'Siwadas, Watumalang, Wonosobo', 'Anang Nur Syafaat', 'Sartinem', '00', '1578226894_rendi.jpg', '1577953383', '2020-01-05 01:21:34', '', '0000-00-00 00:00:00', 'Aktif', 3, '');
INSERT INTO `santri` VALUES (201910012, 2019, 'Tegar Andriansyah', 'Wonosobo', '2003-11-02', 'L', 'Dalangan, Purwojati, Kertek, Wonosobo', 'Mislikhun', 'yumailah', '00', '1578227027_tegar.jpg', '1577953383', '2020-01-05 01:23:47', '', '0000-00-00 00:00:00', 'Aktif', 3, '');
INSERT INTO `santri` VALUES (201910013, 2019, 'Angga Dwi Saputra', 'Wonosobo', '2005-02-17', 'L', 'Kalikuning, Sendangsari, Garung, Wonosobo', 'Ahmad Sadiyuwono', 'Puji Astuti', '082329689189', '1578228298_angga.jpg', '1577953383', '2020-01-05 01:44:58', '1577953383', '2020-01-05 01:48:01', 'Aktif', 3, '');
INSERT INTO `santri` VALUES (201910014, 2019, 'M Ngilman Khafifi', 'Temanggung', '2003-05-05', 'L', 'Karanganyar, Purwosari, Wonoboyo, Temanggung', 'Suprapto', 'Rumsiah', '00', '1578228452_syarif hidayatullah.jpg', '1577953383', '2020-01-05 01:47:32', '', '0000-00-00 00:00:00', 'Aktif', 3, '');
INSERT INTO `santri` VALUES (201910015, 2019, 'Yusrol Hani', 'Temanggung', '2002-08-24', 'L', 'Joho, Wonoboyo, Temanggung', 'Umaryadi', 'Wiwin Sustianan', '085326644629', '1578229212_hani.jpg', '1577953383', '2020-01-05 02:00:12', '', '0000-00-00 00:00:00', 'Aktif', 2, '');
INSERT INTO `santri` VALUES (201910016, 2019, 'M Adin Isrofi', 'Batang', '2005-01-01', 'L', 'limpung', 'aa', 'aa', '00', '1579246042_adin.jpg', '1577953383', '2020-01-17 08:27:22', '', '0000-00-00 00:00:00', 'Aktif', 2, '');
INSERT INTO `santri` VALUES (201910017, 2019, 'M Zidan Arsala N', 'Pekalongan', '2000-01-01', 'L', 'aa', 'aa', 'aa', '000', '1579246221_zidan.jpg', '1577953383', '2020-01-17 08:30:21', '', '0000-00-00 00:00:00', 'Aktif', 3, '');
INSERT INTO `santri` VALUES (202010001, 2020, 'Faiza Rahmawan', 'Wonosobo', '0002-11-22', 'L', 'MJK', 'K', 'K', 'K', '1594489803_Flora-Bayam-Merah-Tidak-Hanya-Diincar-Penderita-Anemia.jpg', '1577953383', '2020-07-11 07:50:03', '', '0000-00-00 00:00:00', 'Aktif', 3, '');
INSERT INTO `santri` VALUES (202010002, 2020, 'M Najmi Gufron', 'Wonosobo', '0004-02-24', 'L', 'Dalangan kertek', 'j', 'F', 'F', '1594489847_ee305f8985fe4658a2f57a7a5ff75512-1.jpg', '1577953383', '2020-07-11 07:50:47', '', '0000-00-00 00:00:00', 'Aktif', 3, '');
INSERT INTO `santri` VALUES (202010003, 2020, 'MRAZID RAFIDZAL', 'Wonosobo', '0005-05-04', 'L', 'bandungan', 'd', 'fds', 'h', '1594489954_dfd.jpg', '1577953383', '2020-07-11 07:52:34', '1577953383', '2020-07-11 08:12:07', 'Aktif', 3, '');
INSERT INTO `santri` VALUES (202010004, 2020, 'Afkar Maulana A', 'Wonosobo', '0005-05-04', 'L', 'bomerto wsb', 'koren', 'f', '9', '1594489984_IMG_20200206_111259.jpg', '1577953383', '2020-07-11 07:53:04', '', '0000-00-00 00:00:00', 'Aktif', 3, '');
INSERT INTO `santri` VALUES (202010005, 2020, 'M KHUSNI H', 'Wonosobo', '0006-05-05', 'L', 'Dalangan kertek', 'f', 'd', '2', '1594490026_IMG_20200501_153130.jpg', '1577953383', '2020-07-11 07:53:46', '', '0000-00-00 00:00:00', 'Aktif', 3, '');
INSERT INTO `santri` VALUES (202010006, 2020, 'Bustanul Arif', 'Wonosobo', '0005-05-04', 'L', 'bomerto wsb', 'j', 's', '2', '1594490068_IMG_20200427_141739.jpg', '1577953383', '2020-07-11 07:54:28', '', '0000-00-00 00:00:00', 'Aktif', 3, '');
INSERT INTO `santri` VALUES (202010007, 2020, 'Khoiru Nur Karim', 'Wonosobo', '0002-04-05', 'L', 'bomerto wsb', 'a', 'fds', '9', '1594490119_Logo_PPP.svg.png', '1577953383', '2020-07-11 07:55:07', '1577953383', '2020-07-11 07:55:19', 'Aktif', 3, '');
INSERT INTO `santri` VALUES (202010008, 2020, 'REZA FERDIAN', 'Wonosobo', '0025-02-04', 'L', 'bomerto wonosobo', 'f', 's', 'd', '1594490174_man2.jpg', '1577953383', '2020-07-11 07:56:14', '1595507881', '2020-07-31 08:30:30', 'Aktif', 3, '');
INSERT INTO `santri` VALUES (202010009, 2020, 'AHMAD SABILAL', 'Wonosobo', '0004-05-25', 'L', 'a', 'dc', 'f', 'd', '1594490222_kisspng-t-shirt-sunglasses-clip-art-thug-life-5ac1b277e164b2.3273607315226435759232.png', '1577953383', '2020-07-11 07:57:02', '', '0000-00-00 00:00:00', 'Aktif', 3, '');
INSERT INTO `santri` VALUES (202010010, 2020, 'AJI SAPUTRA', 'wsb', '2000-04-05', 'L', 'sidosari', 'j', 'd', '9', '1594490281_IMG_20200501_153047.jpg', '1577953383', '2020-07-11 07:58:01', '', '0000-00-00 00:00:00', 'Aktif', 3, '');
INSERT INTO `santri` VALUES (202010011, 2020, 'Hazil Ahmad Rizqullah', 'wsb', '0052-08-05', 'L', 'bandungan', 'a', 'h', '1', '1594490328_img019.jpg', '1577953383', '2020-07-11 07:58:48', '', '0000-00-00 00:00:00', 'Aktif', 3, '');
INSERT INTO `santri` VALUES (202010012, 2020, 'khoirul anam', 'Wonosobo', '0005-06-05', 'L', 'bomerto wonosobo', 'dc', 'f', '1', '1594490362_img010.jpg', '1577953383', '2020-07-11 07:59:22', '', '0000-00-00 00:00:00', 'Aktif', 3, '');
INSERT INTO `santri` VALUES (202010013, 2020, 'Ahmad Maulana', 'Wonosobo', '0003-05-05', 'L', 'bandungan', 'koren', 'fds', 'd', '1594490393_WhatsApp Image 2020-05-17 at 22.01.14.jpeg', '1577953383', '2020-07-11 07:59:53', '', '0000-00-00 00:00:00', 'Aktif', 3, '');
INSERT INTO `santri` VALUES (202010014, 2020, 'M Haikal Fahri', 'Wonosobo', '0005-05-05', 'L', 'Dalangan kertek', 'f', 'h', 'h', '1594490431_ee305f8985fe4658a2f57a7a5ff75512-1.jpg', '1577953383', '2020-07-11 08:00:31', '', '0000-00-00 00:00:00', 'Aktif', 3, '');
INSERT INTO `santri` VALUES (202010015, 2020, 'Dian Cahaya R', 'Pekalongan', '0005-06-05', 'L', 'PEKALONGAN', 'jk', 'J', 'J', '1594815384_IMG_20200427_112937.jpg', '1577953383', '2020-07-15 02:16:24', '', '0000-00-00 00:00:00', 'Aktif', 3, '');
INSERT INTO `santri` VALUES (202010016, 2020, 'M FAHRUR ROZIQIN', 'Wonosobo', '0005-04-05', 'L', 'K', 'KJ', 'KJ', 'KJ', '1594904195_stiker 2017.jpg', '1577953383', '2020-07-16 02:56:35', '', '0000-00-00 00:00:00', 'Aktif', 3, '');
INSERT INTO `santri` VALUES (202010017, 2020, 'M FACHRIL HIDAYAT', 'BATANG', '2007-07-09', 'L', 'SEKIDANG BATANG', 'KHAERUN', 'WIKOYANTI', '082325765610', '1594913179_IMG_20200501_153047.jpg', '1577953383', '2020-07-16 05:26:19', '', '0000-00-00 00:00:00', 'Aktif', 3, '');
INSERT INTO `santri` VALUES (202010018, 2020, 'M RAMADHANI', 'SEMARANG', '0004-04-04', 'L', 'bandungan', 'j', 'd', 'd', '1594969497_IMG_20200502_171642_295.jpg', '1577953383', '2020-07-17 09:04:57', '', '0000-00-00 00:00:00', 'Aktif', 3, '');
INSERT INTO `santri` VALUES (202010019, 2020, 'MUHAMAD ZAKI', 'batang', '0005-05-08', 'L', 'TERSONO', 'G', 'G', 'G', '1595000852_IMG-20200430-WA0056.jpg', '1577953383', '2020-07-17 05:47:32', '', '0000-00-00 00:00:00', 'Aktif', 2, '');
INSERT INTO `santri` VALUES (202010020, 2020, 'AGUS KHILMI', 'Wonosobo', '0002-04-05', 'L', 'HJM', 'JH', 'H', 'JHG', '1598629074_Islamic Mosque Backgrounds.jpg', '1595507881', '2020-08-28 05:37:54', '', '0000-00-00 00:00:00', 'Aktif', 2, '');
INSERT INTO `santri` VALUES (202010021, 2020, 'Faza Misbahul Islah', 'Wonosobo', '2007-12-05', 'L', 'Dalangan kertek', 'TAUFIQ', 'SITI MUTHOLAAH', '01', '1598690655_WhatsApp Image 2020-02-18 at 04.44.09.jpeg', '1595507881', '2020-08-29 10:44:04', '1577953383', '2021-06-10 03:21:32', 'Aktif', 3, '4bfa3ecae0216dcab4c25d9df254c5cb');
INSERT INTO `santri` VALUES (202210001, 2022, 'joko', 'wonosobo', '2021-06-09', 'L', 'wsb', 'a', 'a', 'a', '1623287503_habib ali.jpg', '1577953383', '2021-06-10 03:11:43', '', '0000-00-00 00:00:00', 'Aktif', 2, 'ff57c5dcc62465b72d0d2537bad55919');

-- ----------------------------
-- Table structure for survey_set
-- ----------------------------
DROP TABLE IF EXISTS `survey_set`;
CREATE TABLE `survey_set`  (
  `id_survey_set` int(30) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `id_admin` int(30) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `kategori_Survey_set` enum('survey','PSB') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `Status` enum('Y','T') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id_survey_set`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of survey_set
-- ----------------------------

-- ----------------------------
-- Table structure for tabungan
-- ----------------------------
DROP TABLE IF EXISTS `tabungan`;
CREATE TABLE `tabungan`  (
  `id_tabungan` bigint(20) NOT NULL,
  `nis` int(11) NOT NULL,
  `jumlah_tab` int(11) NOT NULL,
  `tgl_tab` date NOT NULL,
  `create_by` int(11) NOT NULL,
  `create_at` datetime NOT NULL,
  `edit_by` int(11) NULL DEFAULT NULL,
  `edit_at` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id_tabungan`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tabungan
-- ----------------------------
INSERT INTO `tabungan` VALUES (202001040002, 201910002, 0, '2020-01-01', 1577953383, '2020-01-04 02:15:42', 1577953383, '2020-01-05 01:49:21');
INSERT INTO `tabungan` VALUES (202001050001, 201810007, 300000, '2020-01-05', 1577953383, '2020-01-05 01:48:53', 1577953383, '2020-01-05 01:50:33');
INSERT INTO `tabungan` VALUES (202001050002, 201810009, 200000, '2020-01-05', 1577953383, '2020-01-05 01:49:59', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202001050003, 201910013, 200000, '2020-01-05', 1577953383, '2020-01-05 01:50:20', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202001050004, 201910014, 200000, '2020-01-05', 1577953383, '2020-01-05 01:50:59', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202001050005, 201810002, 300000, '2020-01-05', 1577953383, '2020-01-05 01:52:22', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202001050006, 201910010, 300000, '2020-01-05', 1577953383, '2020-01-05 01:54:28', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202001050007, 201810008, 350000, '2020-01-05', 1577953383, '2020-01-05 01:58:25', 1578490768, '2020-01-09 02:54:36');
INSERT INTO `tabungan` VALUES (202001050008, 201710001, 250000, '2020-01-05', 1577953383, '2020-01-05 02:05:47', 1578490768, '2020-01-09 02:46:37');
INSERT INTO `tabungan` VALUES (202001050009, 201810012, 245000, '2020-01-05', 1577953383, '2020-01-05 02:07:22', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202001050010, 201910008, 255000, '2020-01-05', 1577953383, '2020-01-05 02:08:15', 1577953383, '2020-01-05 02:08:28');
INSERT INTO `tabungan` VALUES (202001050011, 201910001, 200000, '2020-01-05', 1577953383, '2020-01-05 02:08:50', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202001050012, 201910011, 400000, '2020-01-05', 1577953383, '2020-01-05 02:09:18', 1577953383, '2020-01-17 07:51:57');
INSERT INTO `tabungan` VALUES (202001050013, 201910004, 200000, '2020-01-05', 1577953383, '2020-01-05 02:09:51', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202001050014, 201910009, 250000, '2020-01-05', 1577953383, '2020-01-05 02:10:20', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202001050015, 201610001, 200000, '2020-01-05', 1577953383, '2020-01-05 02:26:17', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202001080002, 201910006, 285000, '2020-01-08', 1577953383, '2020-01-08 02:29:47', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202001080003, 201910015, 675000, '2020-01-08', 1577953383, '2020-01-08 02:31:14', 1577953383, '2020-01-08 02:31:25');
INSERT INTO `tabungan` VALUES (202001080004, 201910005, 75000, '2020-01-08', 1577953383, '2020-01-08 02:31:51', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202001080005, 201910012, 205000, '2020-01-08', 1577953383, '2020-01-08 02:32:34', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202001090001, 201810003, 150000, '2020-01-09', 1578490768, '2020-01-09 02:38:02', 1578490768, '2020-01-09 02:38:37');
INSERT INTO `tabungan` VALUES (202001090002, 201810001, 200000, '2020-01-09', 1578490768, '2020-01-09 02:41:47', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202001090003, 201810005, 100000, '2020-01-09', 1578490768, '2020-01-09 02:42:47', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202001090004, 201810004, 280000, '2020-01-09', 1578490768, '2020-01-09 02:43:36', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202001090005, 201810016, 75000, '2020-01-09', 1578490768, '2020-01-09 05:57:48', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202001170001, 201910003, 100000, '2020-01-13', 1577953383, '2020-01-17 07:59:21', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202001170002, 201810011, 200000, '2020-01-13', 1577953383, '2020-01-17 08:07:17', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202001170003, 201510003, 200000, '2020-01-13', 1577953383, '2020-01-17 08:22:13', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202001170004, 201810013, 195000, '2020-01-13', 1577953383, '2020-01-17 08:24:13', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202001170005, 201910016, 375000, '2020-01-13', 1577953383, '2020-01-17 08:28:11', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202001170006, 201910017, 600000, '2020-01-13', 1577953383, '2020-01-17 08:31:01', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202001200001, 201810011, 150000, '2020-01-15', 1578490768, '2020-01-20 11:18:21', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202001200002, 201810016, 200000, '2020-01-20', 1578490768, '2020-01-20 11:20:52', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202001230001, 201910005, 200000, '2020-01-23', 1578490768, '2020-01-23 04:03:18', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202001230002, 201810007, 100000, '2020-01-23', 1578490768, '2020-01-23 04:25:35', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202001240001, 201910003, 100000, '2020-01-24', 1577953383, '2020-01-24 11:43:40', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202001250001, 201810012, 100000, '2020-01-25', 1578490768, '2020-01-25 08:54:44', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202001310001, 201910002, 150000, '2020-01-31', 1578490768, '2020-01-31 01:12:50', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202002030001, 201910001, 100000, '2020-02-03', 1578490768, '2020-02-03 11:32:15', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202002030002, 201810004, 50000, '2020-02-03', 1578490768, '2020-02-03 11:35:08', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202002030003, 201910004, 200000, '2020-02-03', 1578490768, '2020-02-03 11:41:21', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202002030004, 201910007, 100000, '2020-02-03', 1578490768, '2020-02-03 11:53:32', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202002060001, 201910014, 200000, '2020-02-06', 1577953383, '2020-02-06 03:25:59', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202002060002, 201910010, 200000, '2020-02-06', 1577953383, '2020-02-06 03:34:26', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202002060003, 201910009, 250000, '2020-02-06', 1577953383, '2020-02-06 03:34:52', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202002060004, 201710002, 200000, '2020-02-06', 1577953383, '2020-02-06 03:35:16', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202002060005, 201610001, 200000, '2020-02-06', 1577953383, '2020-02-06 03:35:35', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202002080001, 201910008, 250000, '2020-02-08', 1578490768, '2020-02-08 06:05:44', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202002080002, 201810009, 250000, '2020-02-08', 1578490768, '2020-02-08 06:06:05', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202002130001, 201910016, 400000, '2020-02-13', 1578490768, '2020-02-13 04:36:47', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202002130002, 201910006, 100000, '2020-02-13', 1578490768, '2020-02-13 04:39:29', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202002130003, 201910003, 100000, '2020-02-13', 1578490768, '2020-02-13 04:40:48', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202002130004, 201810007, 100000, '2020-02-13', 1578490768, '2020-02-13 04:45:07', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202002170001, 201810005, 100000, '2020-02-17', 1578490768, '2020-02-17 12:14:36', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202002170002, 201810007, 50000, '2020-02-17', 1578490768, '2020-02-17 01:44:41', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202002170003, 201810003, 150000, '2020-02-17', 1578490768, '2020-02-17 01:48:42', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202002170004, 201510003, 200000, '2020-02-17', 1578490768, '2020-02-17 01:50:10', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202002170005, 201710001, 200000, '2020-02-17', 1578490768, '2020-02-17 01:53:52', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202002200001, 201910012, 100000, '2020-02-02', 1578490768, '2020-02-20 07:25:20', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202002240001, 201810016, 200000, '2020-02-24', 1578490768, '2020-02-24 11:21:17', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202002270001, 201910015, 500000, '2020-02-27', 1578490768, '2020-02-27 04:06:16', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202002270002, 201910014, 200000, '2020-02-27', 1578490768, '2020-02-27 04:15:15', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202002270003, 201910011, 200000, '2020-02-27', 1578490768, '2020-02-27 04:15:33', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202002270004, 201810002, 200000, '2020-02-27', 1578490768, '2020-02-27 04:19:23', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202002280001, 201910002, 150000, '2020-02-28', 1578490768, '2020-02-28 07:16:00', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202003010001, 201810012, 400000, '2020-03-01', 1578490768, '2020-03-01 11:07:13', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202003010002, 201810011, 100000, '2020-03-01', 1578490768, '2020-03-01 11:07:32', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202003010003, 201910011, 500000, '2020-03-01', 1578490768, '2020-03-01 11:07:49', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202003020001, 201810007, 125000, '2020-03-02', 1578490768, '2020-03-02 11:27:55', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202003020002, 201810004, 200000, '2020-03-02', 1578490768, '2020-03-02 11:33:45', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202003020003, 201910005, 100000, '2020-03-02', 1578490768, '2020-03-02 05:51:11', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202003020004, 201810008, 150000, '2020-03-02', 1578490768, '2020-03-02 05:52:02', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202003090001, 201910006, 200000, '2020-03-09', 1578490768, '2020-03-09 12:05:32', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202003090002, 201810007, 100000, '2020-03-09', 1578490768, '2020-03-09 12:08:16', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202003090003, 201710002, 200000, '2020-03-09', 1578490768, '2020-03-09 12:15:21', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202003090004, 201610001, 200000, '2020-03-09', 1578490768, '2020-03-09 12:15:38', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202003090005, 201910009, 200000, '2020-03-09', 1578490768, '2020-03-09 12:18:38', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202003160001, 201810011, 200000, '2020-03-16', 1578490768, '2020-03-16 05:00:31', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202003160002, 201910003, 50000, '2020-03-16', 1578490768, '2020-03-16 05:00:49', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202003160003, 201810009, 245000, '2020-03-16', 1578490768, '2020-03-16 11:21:57', 1577953383, '2020-03-19 05:05:12');
INSERT INTO `tabungan` VALUES (202003160004, 201910008, 245000, '2020-03-16', 1578490768, '2020-03-16 11:28:01', 1577953383, '2020-03-19 05:05:01');
INSERT INTO `tabungan` VALUES (202003190001, 201510003, 100000, '2020-03-19', 1577953383, '2020-03-19 05:04:33', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202003190002, 201810010, 100000, '2020-03-19', 1577953383, '2020-03-19 05:04:49', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202003220001, 201810004, 100000, '2020-03-22', 1578490768, '2020-03-22 05:01:02', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202003230001, 201710001, 200000, '2020-03-23', 1577953383, '2020-03-23 08:53:11', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202003230002, 201910010, 200000, '2020-03-23', 1577953383, '2020-03-23 05:23:11', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202007110001, 201810009, 250000, '2020-07-11', 1577953383, '2020-07-11 07:46:53', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202007110002, 201910008, 250000, '2020-07-11', 1577953383, '2020-07-11 07:47:13', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202007110003, 202010001, 200000, '2020-07-06', 1577953383, '2020-07-11 08:01:31', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202007110004, 202010002, 300000, '2020-07-06', 1577953383, '2020-07-11 08:01:56', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202007110005, 202010003, 200000, '2020-07-06', 1577953383, '2020-07-11 08:02:35', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202007110006, 202010004, 200000, '2020-07-06', 1577953383, '2020-07-11 08:02:59', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202007110007, 202010005, 200000, '2020-07-06', 1577953383, '2020-07-11 08:03:18', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202007110008, 201910010, 200000, '2020-07-06', 1577953383, '2020-07-11 08:03:33', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202007110009, 202010006, 350000, '2020-07-06', 1577953383, '2020-07-11 08:03:50', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202007110010, 201910015, 200000, '2020-07-06', 1577953383, '2020-07-11 08:04:11', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202007110011, 202010007, 150000, '2020-07-06', 1577953383, '2020-07-11 08:04:32', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202007110012, 202010008, 200000, '2020-07-06', 1577953383, '2020-07-11 08:04:51', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202007110013, 202010009, 750000, '2020-07-06', 1577953383, '2020-07-11 08:05:08', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202007110014, 201910003, 150000, '2020-07-06', 1577953383, '2020-07-11 08:05:33', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202007110015, 201910005, 200000, '2020-07-06', 1577953383, '2020-07-11 08:05:58', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202007110016, 202010010, 100000, '2020-07-06', 1577953383, '2020-07-11 08:06:15', 1577953383, '2020-07-11 08:08:22');
INSERT INTO `tabungan` VALUES (202007110017, 202010011, 200000, '2020-07-06', 1577953383, '2020-07-11 08:06:35', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202007110018, 201910006, 200000, '2020-07-06', 1577953383, '2020-07-11 08:06:53', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202007110019, 202010012, 500000, '2020-07-06', 1577953383, '2020-07-11 08:07:10', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202007110020, 202010013, 200000, '2020-07-07', 1577953383, '2020-07-11 08:07:38', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202007110021, 201910017, 300000, '2020-07-07', 1577953383, '2020-07-11 08:07:59', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202007110022, 202010014, 300000, '2020-07-06', 1577953383, '2020-07-11 08:08:46', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202007110023, 201910001, 200000, '2020-07-06', 1577953383, '2020-07-11 08:09:00', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202007110024, 201810011, 200000, '2020-07-10', 1577953383, '2020-07-11 08:10:51', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202007120001, 201810004, 300000, '2020-07-12', 1577953383, '2020-07-12 06:11:18', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202007120002, 201910014, 200000, '2020-07-12', 1577953383, '2020-07-12 06:14:21', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202007120003, 201810019, 200000, '2020-07-10', 1577953383, '2020-07-12 06:15:41', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202007120004, 201910002, 150000, '2020-07-09', 1577953383, '2020-07-12 06:16:26', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202007140002, 201810010, 150000, '2020-07-14', 1577953383, '2020-07-14 01:16:57', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202007160001, 202010016, 200000, '2020-07-09', 1577953383, '2020-07-16 02:57:01', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202007160002, 201810008, 230000, '2020-07-09', 1577953383, '2020-07-16 03:09:24', 1577953383, '2020-07-16 03:10:11');
INSERT INTO `tabungan` VALUES (202007160003, 201610001, 250000, '2020-07-09', 1577953383, '2020-07-16 03:12:14', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202007160004, 201810012, 150000, '2020-07-16', 1577953383, '2020-07-16 03:17:29', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202007160005, 202010015, 250000, '2020-07-09', 1577953383, '2020-07-16 05:17:31', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202007160006, 202010017, 200000, '2020-07-16', 1577953383, '2020-07-16 05:27:42', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202007170001, 202010018, 500000, '2020-07-17', 1577953383, '2020-07-17 09:05:13', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202007170002, 202010019, 200000, '2020-07-17', 1577953383, '2020-07-17 05:47:49', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202007220001, 202010003, 50000, '2020-07-22', 1577953383, '2020-07-22 07:46:57', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202007230001, 201910009, 250000, '2020-07-23', 1595507881, '2020-07-23 04:20:35', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202007270001, 202010017, 200000, '2020-07-27', 1595507881, '2020-07-27 09:19:31', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202008010001, 202010019, 275000, '2020-08-01', 1595507881, '2020-08-01 08:09:14', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202008010002, 201910003, 100000, '2020-08-01', 1595507881, '2020-08-01 10:18:36', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202008010003, 202010010, 150000, '2020-08-01', 1595507881, '2020-08-01 10:18:55', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202008010004, 202010011, 200000, '2020-08-01', 1595507881, '2020-08-01 10:19:11', 1595507881, '2020-08-01 10:19:43');
INSERT INTO `tabungan` VALUES (202008020001, 202010004, 150000, '2020-08-02', 1595507881, '2020-08-02 07:43:28', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202008040001, 201910002, 300000, '2020-08-01', 1595507881, '2020-08-04 06:21:01', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202008040002, 201910006, 250000, '2020-08-04', 1595507881, '2020-08-04 06:24:25', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202008070001, 202010007, 450000, '2020-08-07', 1595507881, '2020-08-07 06:14:18', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202008080001, 202010008, 125000, '2020-08-08', 1595507881, '2020-08-08 08:40:19', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202008080002, 201910008, 200000, '2020-08-08', 1595507881, '2020-08-08 09:19:51', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202008080003, 201810009, 200000, '2020-08-08', 1595507881, '2020-08-08 09:20:04', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202008090001, 201810013, 150000, '2020-08-09', 1595507881, '2020-08-09 06:47:12', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202008090002, 201910011, 200000, '2020-08-09', 1595507881, '2020-08-09 07:41:47', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202008090003, 202010016, 200000, '2020-08-09', 1595507881, '2020-08-09 08:11:45', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202008090004, 201810012, 200000, '2020-08-09', 1595507881, '2020-08-09 09:52:28', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202008090005, 201810007, 300000, '2020-08-09', 1595507881, '2020-08-09 09:52:40', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202008090006, 202010003, 205000, '2020-08-09', 1595507881, '2020-08-09 12:46:35', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202008090007, 202010001, 200000, '2020-08-09', 1595507881, '2020-08-09 12:52:35', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202008090008, 201810004, 200000, '2020-08-09', 1595507881, '2020-08-09 02:33:22', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202008090009, 201610001, 200000, '2020-08-09', 1595507881, '2020-08-09 02:34:15', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202008090010, 201810008, 200000, '2020-08-09', 1595507881, '2020-08-09 02:37:56', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202008090011, 201810010, 150000, '2020-08-09', 1595507881, '2020-08-09 02:47:33', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202008100001, 201910005, 175000, '2020-08-10', 1595507881, '2020-08-10 07:44:56', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202008110001, 202010005, 200000, '2020-08-11', 1595507881, '2020-08-11 07:42:21', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202008110002, 201910010, 200000, '2020-08-11', 1595507881, '2020-08-11 05:01:22', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202008140001, 201910017, 120000, '2020-08-14', 1595507881, '2020-08-14 11:37:17', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202008140002, 201810011, 300000, '2020-08-24', 1595507881, '2020-08-14 02:46:29', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202008140003, 201910009, 200000, '2020-08-14', 1595507881, '2020-08-14 02:50:39', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202008140004, 202010015, 200000, '2020-08-14', 1595507881, '2020-08-14 02:56:56', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202008190001, 202010014, 100000, '2020-08-19', 1595507881, '2020-08-19 05:43:49', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202008190002, 202010010, 50000, '2020-08-19', 1595507881, '2020-08-19 05:44:39', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202008190003, 201910017, 75000, '2020-08-19', 1595507881, '2020-08-19 06:02:41', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202008190004, 202010006, 200000, '2020-08-19', 1595507881, '2020-08-19 06:24:45', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202008230001, 201810010, 100000, '2020-08-23', 1595507881, '2020-08-23 12:59:17', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202008230002, 202010019, 500000, '2020-08-23', 1595507881, '2020-08-23 01:30:14', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202008270001, 201810016, 100000, '2020-08-28', 1595507881, '2020-08-27 07:14:59', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202008280001, 201810008, 200000, '2020-09-01', 1595507881, '2020-08-28 02:31:49', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202008280002, 202010002, 75000, '2020-08-28', 1595507881, '2020-08-28 02:33:05', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202008280003, 201910015, 200000, '2020-08-28', 1595507881, '2020-08-28 02:36:10', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202008280004, 202010012, 356000, '2020-08-28', 1595507881, '2020-08-28 05:34:56', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202008280005, 202010019, 70000, '2020-08-28', 1595507881, '2020-08-28 05:35:53', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202008280006, 201810002, 20000, '2020-08-28', 1595507881, '2020-08-28 05:36:38', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202008280007, 201910014, 20000, '2020-08-28', 1595507881, '2020-08-28 05:36:56', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202008280008, 201910012, 415000, '2020-08-28', 1595507881, '2020-08-28 05:37:13', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202008280009, 202010020, 180000, '2020-08-28', 1595507881, '2020-08-28 05:38:10', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202008290001, 202010021, 330000, '2020-08-29', 1595507881, '2020-08-29 10:45:35', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202008310001, 201810003, 100000, '2020-08-31', 1595507881, '2020-08-31 12:51:35', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202008310002, 202010021, 50000, '2020-08-31', 1595507881, '2020-08-31 06:29:52', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202009010001, 202010003, 60000, '2020-09-01', 1595507881, '2020-09-01 05:16:41', 1595507881, '2020-09-01 05:17:12');
INSERT INTO `tabungan` VALUES (202009010002, 202010016, 150000, '2020-09-01', 1595507881, '2020-09-01 05:43:14', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202009010003, 201910002, 150000, '2020-09-01', 1595507881, '2020-09-01 05:46:46', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202009020001, 202010010, 200000, '2020-09-02', 1595507881, '2020-09-02 04:33:21', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202009020002, 201910006, 200000, '2020-09-02', 1595507881, '2020-09-02 04:39:10', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202009030001, 201910012, 200000, '2020-09-03', 1595507881, '2020-09-03 06:47:19', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202009030002, 202010011, 200000, '2020-09-03', 1595507881, '2020-09-03 10:29:49', 1595507881, '2020-09-03 10:30:50');
INSERT INTO `tabungan` VALUES (202009030003, 201810016, 100000, '2020-09-03', 1595507881, '2020-09-03 10:30:11', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202009030004, 201610001, 200000, '2020-09-03', 1595507881, '2020-09-03 11:45:31', 1595507881, '2020-09-03 11:46:13');
INSERT INTO `tabungan` VALUES (202009030005, 201810002, 200000, '2020-09-03', 1595507881, '2020-09-03 12:15:38', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202009030006, 201810005, 150000, '2020-09-03', 1595507881, '2020-09-03 04:09:49', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202009040001, 202010002, 150000, '2020-09-04', 1595507881, '2020-09-04 08:24:29', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202009050001, 202010013, 100000, '2020-09-05', 1595507881, '2020-09-05 12:31:14', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202009050002, 201610001, 250000, '2020-09-05', 1595507881, '2020-09-05 12:31:31', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202009050003, 201810011, 300000, '2020-09-05', 1595507881, '2020-09-05 12:31:49', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202009050004, 201810008, 300000, '2020-09-05', 1595507881, '2020-09-05 12:32:02', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202009050005, 201810006, 100000, '2020-09-05', 1595507881, '2020-09-05 12:32:20', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202009050006, 202010005, 175000, '2020-09-05', 1595507881, '2020-09-05 02:15:06', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202009060001, 201810007, 175000, '2020-09-06', 1595507881, '2020-09-06 06:49:36', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202009060002, 201810004, 200000, '2020-09-06', 1595507881, '2020-09-06 08:03:49', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202009060003, 201810012, 220000, '2020-09-06', 1595507881, '2020-09-06 05:59:48', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202009060004, 201810013, 150000, '2020-09-06', 1595507881, '2020-09-06 06:02:09', 0, '0000-00-00 00:00:00');
INSERT INTO `tabungan` VALUES (202106100001, 201410001, 30000, '2021-06-10', 1623296882, '2021-06-10 05:48:39', 0, '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for tagihan
-- ----------------------------
DROP TABLE IF EXISTS `tagihan`;
CREATE TABLE `tagihan`  (
  `id_tagihan` bigint(20) NOT NULL AUTO_INCREMENT,
  `waktu_tagihan` char(7) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nis` int(11) NOT NULL,
  `syahriah` bigint(20) NOT NULL,
  `kos_makan` bigint(20) NOT NULL,
  `sekolah` bigint(20) NOT NULL,
  `created_by` char(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `ket` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  PRIMARY KEY (`id_tagihan`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 798 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tagihan
-- ----------------------------
INSERT INTO `tagihan` VALUES (473, '2021-07', 201410001, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (474, '2021-08', 201410001, 200000, 0, 0, '1623296882', '2021-06-11 04:27:54', '');
INSERT INTO `tagihan` VALUES (670, '2021-06', 202210001, 0, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (671, '2021-06', 202010021, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (672, '2021-06', 202010020, 0, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (673, '2021-06', 202010019, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (674, '2021-06', 202010018, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (675, '2021-06', 202010017, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (676, '2021-06', 202010016, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (677, '2021-06', 202010015, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (678, '2021-06', 202010014, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (679, '2021-06', 202010013, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (680, '2021-06', 202010012, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (681, '2021-06', 202010011, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (682, '2021-06', 202010010, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (683, '2021-06', 202010009, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (684, '2021-06', 202010008, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (685, '2021-06', 202010007, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (686, '2021-06', 202010006, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (687, '2021-06', 202010005, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (688, '2021-06', 202010004, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (689, '2021-06', 202010003, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (690, '2021-06', 202010002, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (691, '2021-06', 202010001, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (692, '2021-06', 201910017, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (693, '2021-06', 201910016, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (694, '2021-06', 201910015, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (695, '2021-06', 201910014, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (696, '2021-06', 201910013, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (697, '2021-06', 201910012, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (698, '2021-06', 201910011, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (699, '2021-06', 201910010, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (700, '2021-06', 201910009, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (701, '2021-06', 201910008, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (702, '2021-06', 201910007, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (703, '2021-06', 201910006, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (704, '2021-06', 201910005, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (705, '2021-06', 201910004, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (706, '2021-06', 201910003, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (707, '2021-06', 201910002, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (708, '2021-06', 201910001, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (709, '2021-06', 201810019, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (710, '2021-06', 201810018, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (711, '2021-06', 201810017, 0, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (712, '2021-06', 201810016, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (713, '2021-06', 201810013, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (714, '2021-06', 201810012, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (715, '2021-06', 201810011, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (716, '2021-06', 201810010, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (717, '2021-06', 201810009, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (718, '2021-06', 201810008, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (719, '2021-06', 201810007, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (720, '2021-06', 201810006, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (721, '2021-06', 201810005, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (722, '2021-06', 201810004, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (723, '2021-06', 201810003, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (724, '2021-06', 201810002, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (725, '2021-06', 201710005, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (726, '2021-06', 201710004, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (727, '2021-06', 201710003, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (728, '2021-06', 201710002, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (729, '2021-06', 201710001, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (730, '2021-06', 201610001, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (731, '2021-06', 201510003, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (732, '2021-06', 201510002, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (733, '2021-06', 201510001, 1200000, 0, 0, '1623288261', '2021-06-11 04:44:43', '');
INSERT INTO `tagihan` VALUES (734, '2021-07', 202210001, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (735, '2021-07', 202010021, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (736, '2021-07', 202010020, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (737, '2021-07', 202010019, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (738, '2021-07', 202010018, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (739, '2021-07', 202010017, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (740, '2021-07', 202010016, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (741, '2021-07', 202010015, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (742, '2021-07', 202010014, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (743, '2021-07', 202010013, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (744, '2021-07', 202010012, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (745, '2021-07', 202010011, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (746, '2021-07', 202010010, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (747, '2021-07', 202010009, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (748, '2021-07', 202010008, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (749, '2021-07', 202010007, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (750, '2021-07', 202010006, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (751, '2021-07', 202010005, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (752, '2021-07', 202010004, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (753, '2021-07', 202010003, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (754, '2021-07', 202010002, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (755, '2021-07', 202010001, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (756, '2021-07', 201910017, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (757, '2021-07', 201910016, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (758, '2021-07', 201910015, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (759, '2021-07', 201910014, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (760, '2021-07', 201910013, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (761, '2021-07', 201910012, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (762, '2021-07', 201910011, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (763, '2021-07', 201910010, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (764, '2021-07', 201910009, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (765, '2021-07', 201910008, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (766, '2021-07', 201910007, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (767, '2021-07', 201910006, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (768, '2021-07', 201910005, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (769, '2021-07', 201910004, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (770, '2021-07', 201910003, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (771, '2021-07', 201910002, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (772, '2021-07', 201910001, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (773, '2021-07', 201810019, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (774, '2021-07', 201810018, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (775, '2021-07', 201810017, 0, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (776, '2021-07', 201810016, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (777, '2021-07', 201810013, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (778, '2021-07', 201810012, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (779, '2021-07', 201810011, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (780, '2021-07', 201810010, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (781, '2021-07', 201810009, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (782, '2021-07', 201810008, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (783, '2021-07', 201810007, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (784, '2021-07', 201810006, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (785, '2021-07', 201810005, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (786, '2021-07', 201810004, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (787, '2021-07', 201810003, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (788, '2021-07', 201810002, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (789, '2021-07', 201710005, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (790, '2021-07', 201710004, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (791, '2021-07', 201710003, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (792, '2021-07', 201710002, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (793, '2021-07', 201710001, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (794, '2021-07', 201610001, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (795, '2021-07', 201510003, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (796, '2021-07', 201510002, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');
INSERT INTO `tagihan` VALUES (797, '2021-07', 201510001, 30000, 0, 0, '1577953383', '2021-06-11 08:00:35', '');

-- ----------------------------
-- Table structure for trans
-- ----------------------------
DROP TABLE IF EXISTS `trans`;
CREATE TABLE `trans`  (
  `id_trans` bigint(20) NOT NULL,
  `nis` int(11) NOT NULL,
  `jumlah_trans` int(11) NOT NULL,
  `tgl_trans` date NOT NULL,
  `id_jtrans` int(11) NOT NULL,
  `ket_trans` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `create_by` int(11) NOT NULL,
  `create_at` datetime NOT NULL,
  `edit_by` int(11) NOT NULL,
  `edit_at` datetime NOT NULL,
  PRIMARY KEY (`id_trans`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of trans
-- ----------------------------
INSERT INTO `trans` VALUES (202001090001, 201910005, 25000, '2020-01-09', 2, '', 1578490768, '2020-01-09 05:26:20', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001090002, 201810012, 25000, '2020-01-09', 2, '', 1578490768, '2020-01-09 05:29:15', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001090003, 201810012, 25000, '2020-01-09', 2, '', 1578490768, '2020-01-09 05:30:31', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001090004, 201910009, 25000, '2020-01-09', 2, '', 1578490768, '2020-01-09 05:32:02', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001090005, 201810004, 25000, '2020-01-09', 2, '', 1578490768, '2020-01-09 05:33:09', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001090006, 201810008, 25000, '2020-01-09', 2, '', 1578490768, '2020-01-09 05:34:15', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001090007, 201910013, 25000, '2020-01-09', 2, '', 1578490768, '2020-01-09 05:35:25', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001090008, 201910014, 25000, '2020-01-09', 2, '', 1578490768, '2020-01-09 05:36:52', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001090009, 201910008, 25000, '2020-01-09', 2, '', 1578490768, '2020-01-09 05:38:03', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001090010, 201610001, 25000, '2020-01-09', 2, '', 1578490768, '2020-01-09 05:38:57', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001090011, 201910006, 25000, '2020-01-09', 2, '', 1578490768, '2020-01-09 05:40:27', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001090012, 201910010, 25000, '2020-01-09', 2, '', 1578490768, '2020-01-09 05:41:57', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001090014, 201810009, 25000, '2020-01-09', 2, '', 1578490768, '2020-01-09 05:43:46', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001090015, 201810005, 25000, '2020-01-09', 2, '', 1578490768, '2020-01-09 05:44:29', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001090016, 201810007, 25000, '2020-01-09', 2, '', 1578490768, '2020-01-09 05:45:54', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001090017, 201910011, 25000, '2020-01-09', 2, '', 1578490768, '2020-01-09 05:46:52', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001090018, 201810003, 25000, '2020-01-09', 2, '', 1578490768, '2020-01-09 05:47:40', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001090019, 201910012, 25000, '2020-01-09', 2, '', 1578490768, '2020-01-09 05:48:56', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001090020, 201910015, 25000, '2020-01-09', 2, '', 1578490768, '2020-01-09 05:49:44', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001090021, 201710001, 25000, '2020-01-09', 2, 'ojo kokehen njajan !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', 1578490768, '2020-01-09 05:53:25', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001090022, 201910004, 25000, '2020-01-09', 4, 'nyabuunn !!!!!!!!!!', 1578490768, '2020-01-09 05:55:32', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001090023, 201810016, 25000, '2020-01-09', 2, '', 1578490768, '2020-01-09 05:58:28', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001170001, 201910015, 50000, '2020-01-13', 2, 'tgl 13 dan a6 januari 2020', 1577953383, '2020-01-17 07:46:50', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001170002, 201910014, 50000, '2020-01-13', 2, 'tgl 13 & 16 januari', 1577953383, '2020-01-17 07:47:58', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001170003, 201910013, 50000, '2020-01-13', 2, '13 & 16 januari ', 1577953383, '2020-01-17 07:49:01', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001170004, 201910012, 50000, '2020-01-13', 2, '13 & 16 januari', 1577953383, '2020-01-17 07:49:50', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001170005, 201910011, 50000, '2020-01-13', 2, '13 & 16 januari 2020', 1577953383, '2020-01-17 07:51:03', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001170006, 201910010, 25000, '2020-01-16', 2, '', 1577953383, '2020-01-17 07:52:56', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001170007, 201910009, 50000, '2020-01-13', 2, '13 & 16 januari 2020', 1577953383, '2020-01-17 07:53:27', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001170008, 201910008, 25000, '2020-01-13', 2, '', 1577953383, '2020-01-17 07:56:04', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001170009, 201910006, 50000, '2020-01-13', 2, '13 & 16 januari 2020', 1577953383, '2020-01-17 07:57:09', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001170010, 201910005, 50000, '2020-01-13', 2, '13 & 16 januari 2020', 1577953383, '2020-01-17 07:57:54', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001170011, 201910004, 50000, '2020-01-13', 2, '13 & 16 januari 2020', 1577953383, '2020-01-17 07:58:44', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001170012, 201910003, 25000, '2020-01-16', 2, '', 1577953383, '2020-01-17 07:59:41', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001170013, 201910001, 25000, '2020-01-10', 2, '', 1577953383, '2020-01-17 08:01:02', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001170014, 201910001, 50000, '2020-01-13', 2, '13 & 16 januari 2020', 1577953383, '2020-01-17 08:01:28', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001170015, 201810016, 50000, '2020-01-13', 2, '13 & 16 januari 2020', 1577953383, '2020-01-17 08:05:25', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001170016, 201810011, 50000, '2020-01-13', 2, '13 & 16 januari 2020', 1577953383, '2020-01-17 08:07:44', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001170017, 201810012, 25000, '2020-01-16', 2, '', 1577953383, '2020-01-17 08:08:59', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001170018, 201810009, 25000, '2020-01-16', 2, '', 1577953383, '2020-01-17 08:09:54', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001170019, 201810008, 50000, '2020-01-13', 2, '13 & 16 januari 2020', 1577953383, '2020-01-17 08:11:00', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001170020, 201810007, 50000, '2020-01-13', 2, '13 & 16 januari 2020', 1577953383, '2020-01-17 08:11:31', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001170021, 201810005, 50000, '2020-01-13', 2, '13 & 16 januari 2020', 1577953383, '2020-01-17 08:12:21', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001170022, 201810004, 25000, '2020-01-13', 2, '', 1577953383, '2020-01-17 08:13:11', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001170023, 201810003, 50000, '2020-01-13', 2, '13 & 16 januari 2020', 1577953383, '2020-01-17 08:13:53', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001170024, 201810002, 50000, '2020-01-13', 2, '13 & 16 januari 2020', 1577953383, '2020-01-17 08:14:30', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001170025, 201710001, 50000, '2020-01-13', 2, '13 & 16 januari 2020', 1577953383, '2020-01-17 08:15:29', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001170026, 201610001, 50000, '2020-01-13', 2, '13 & 16 januari 2020', 1577953383, '2020-01-17 08:17:50', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001170027, 201510003, 45000, '2020-01-13', 2, '13 & 16 januari 2020', 1577953383, '2020-01-17 08:22:34', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001170028, 201910016, 50000, '2020-01-13', 2, '13 & 16 januari 2020', 1577953383, '2020-01-17 08:28:29', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001170029, 201910017, 25000, '2020-01-16', 2, '', 1577953383, '2020-01-17 08:31:30', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001200001, 201810003, 25000, '2020-01-20', 2, '', 1578490768, '2020-01-20 11:14:28', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001200002, 201810008, 25000, '2020-01-20', 2, '', 1578490768, '2020-01-20 11:16:30', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001200003, 201810009, 25000, '2020-01-20', 2, '', 1578490768, '2020-01-20 11:17:38', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001200004, 201810011, 25000, '2020-01-20', 2, '', 1578490768, '2020-01-20 11:18:54', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001200005, 201810012, 25000, '2020-01-20', 2, '', 1578490768, '2020-01-20 11:19:51', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001200006, 201810016, 25000, '2020-01-20', 2, '', 1578490768, '2020-01-20 11:21:11', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001200007, 201910003, 25000, '2020-01-20', 2, 'nyabuun !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', 1578490768, '2020-01-20 11:22:06', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001200008, 201810002, 50000, '2020-01-20', 2, 'beli peci', 1578490768, '2020-01-20 11:23:49', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001200009, 201910001, 25000, '2020-01-20', 2, '', 1578490768, '2020-01-20 11:24:34', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001200010, 201910013, 25000, '2020-01-20', 2, '', 1578490768, '2020-01-20 11:25:21', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001200011, 201910009, 25000, '2020-01-20', 2, 'pekalongan uyyyeeeeeeeeeeeeeeee', 1578490768, '2020-01-20 11:26:26', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001200012, 201910011, 25000, '2020-01-20', 2, 'tukang nyabuuuun !!!!!!!!!!!!!!!!!!!!!!!!', 1578490768, '2020-01-20 11:27:27', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001200013, 201810007, 25000, '2020-01-20', 2, '', 1578490768, '2020-01-20 11:28:05', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001200014, 201910014, 25000, '2020-01-20', 2, 'nyabuunn waeeeeeeeeeee !!!!!!!!!!!!!!', 1578490768, '2020-01-20 11:29:11', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001200015, 201610001, 25000, '2020-01-20', 2, '', 1578490768, '2020-01-20 11:30:13', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001200016, 201910017, 25000, '2020-01-20', 2, 'nggo sangu mingkem !!!!', 1578490768, '2020-01-20 11:30:57', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001200017, 201910015, 25000, '2020-01-20', 2, '', 1578490768, '2020-01-20 11:31:51', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001200018, 201810004, 25000, '2020-01-20', 2, '', 1578490768, '2020-01-20 11:32:46', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001200019, 201910006, 25000, '2020-01-20', 2, '', 1578490768, '2020-01-20 11:33:50', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001200020, 201910008, 25000, '2020-01-20', 2, '', 1578490768, '2020-01-20 11:34:56', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001200021, 201510003, 25000, '2020-01-20', 2, 'nggo sangu nyabuun !!!!!!!!!!', 1578490768, '2020-01-20 11:36:12', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001200022, 201910012, 25000, '2020-01-20', 2, 'nggo oprasi wajah', 1578490768, '2020-01-20 11:37:39', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001200023, 201910016, 25000, '2020-01-20', 2, '', 1578490768, '2020-01-20 11:38:28', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001200024, 201810013, 25000, '2020-01-20', 2, '', 1578490768, '2020-01-20 11:39:31', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001200025, 201910004, 25000, '2020-01-20', 2, 'buat sulam hidung ???', 1578490768, '2020-01-20 11:40:43', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001200026, 201710001, 25000, '2020-01-20', 2, 'tukang wik wik wik', 1578490768, '2020-01-20 11:41:51', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001230001, 201910012, 25000, '2020-01-23', 2, '', 1578490768, '2020-01-23 03:58:22', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001230002, 201810005, 25000, '2020-01-23', 2, '', 1578490768, '2020-01-23 03:59:39', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001230003, 201910001, 25000, '2020-01-23', 2, '', 1578490768, '2020-01-23 04:00:55', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001230004, 201910009, 25000, '2020-01-23', 2, '', 1578490768, '2020-01-23 04:02:13', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001230005, 201910005, 25000, '2020-01-23', 2, '', 1578490768, '2020-01-23 04:03:49', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001230006, 201910016, 25000, '2020-01-23', 2, '', 1578490768, '2020-01-23 04:04:43', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001230007, 201910003, 25000, '2020-01-23', 2, '', 1578490768, '2020-01-23 04:05:40', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001230008, 201810002, 25000, '2020-01-13', 2, '', 1578490768, '2020-01-23 04:06:33', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001230009, 201810011, 25000, '2020-01-23', 2, '', 1578490768, '2020-01-23 04:07:39', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001230010, 201810004, 25000, '2020-01-23', 2, '', 1578490768, '2020-01-23 04:08:46', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001230011, 201610001, 25000, '2020-01-23', 2, '', 1578490768, '2020-01-23 04:09:44', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001230012, 201910006, 25000, '2020-01-23', 2, '', 1578490768, '2020-01-23 04:10:38', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001230013, 201910004, 25000, '2020-01-23', 2, 'oprasi irung ben mancuung !!!!!!!!!', 1578490768, '2020-01-23 04:12:18', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001230014, 201910010, 25000, '2020-01-23', 2, '', 1578490768, '2020-01-23 04:13:29', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001230015, 201910017, 25000, '2020-01-23', 2, '', 1578490768, '2020-01-23 04:14:36', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001230016, 201910008, 25000, '2020-01-23', 2, '', 1578490768, '2020-01-23 04:16:38', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001230017, 201810008, 25000, '2020-01-23', 2, '', 1578490768, '2020-01-23 04:18:29', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001230018, 201910013, 25000, '2020-01-23', 2, '', 1578490768, '2020-01-23 04:20:28', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001230019, 201810009, 25000, '2020-01-23', 2, '', 1578490768, '2020-01-23 04:21:23', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001230020, 201810012, 25000, '2020-01-23', 2, '', 1578490768, '2020-01-23 04:22:26', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001230021, 201910011, 25000, '2020-01-23', 2, '', 1578490768, '2020-01-23 04:23:24', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001230022, 201810003, 25000, '2020-01-23', 2, '', 1578490768, '2020-01-23 04:24:08', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001230023, 201810007, 25000, '2020-01-23', 2, '', 1578490768, '2020-01-23 04:24:50', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001230024, 201510003, 25000, '2020-01-23', 2, '', 1578490768, '2020-01-23 04:26:07', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001230025, 201710001, 25000, '2020-01-23', 2, '', 1578490768, '2020-01-23 04:27:01', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001230026, 201810016, 25000, '2020-01-23', 2, '', 1578490768, '2020-01-23 04:28:01', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001230027, 201910014, 25000, '2020-01-23', 2, '', 1578490768, '2020-01-23 04:29:55', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001230028, 201810013, 20000, '2020-01-23', 2, '', 1578490768, '2020-01-23 04:31:17', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001230029, 201910015, 50000, '2020-01-23', 2, 'beli kaca mata harga 670000', 1578490768, '2020-01-23 04:43:46', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001230030, 201910015, 50000, '2020-01-23', 2, 'beli kaca mata harga 670000', 1578490768, '2020-01-23 04:44:00', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001230031, 201910015, 50000, '2020-01-23', 2, 'beli kaca mata harga 670000', 1578490768, '2020-01-23 04:44:11', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001230032, 201910015, 50000, '2020-01-23', 2, 'beli kaca mata harga 670000', 1578490768, '2020-01-23 04:44:25', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001230033, 201910015, 50000, '2020-01-23', 2, 'beli kaca mata harga 670000', 1578490768, '2020-01-23 04:44:32', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001230034, 201910015, 50000, '2020-01-23', 2, 'beli kaca mata harga 670000', 1578490768, '2020-01-23 04:44:45', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001270001, 201910005, 25000, '2020-01-27', 2, '', 1578490768, '2020-01-27 11:26:24', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001270002, 201910016, 25000, '2020-01-27', 2, '', 1578490768, '2020-01-27 11:27:41', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001270003, 201710001, 25000, '2020-01-27', 2, '', 1578490768, '2020-01-27 11:28:34', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001270004, 201810008, 25000, '2020-01-27', 2, '', 1578490768, '2020-01-27 11:29:33', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001270005, 201810011, 25000, '2020-01-27', 2, '', 1578490768, '2020-01-27 11:30:37', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001270006, 201610001, 25000, '2020-01-27', 2, '', 1578490768, '2020-01-27 11:31:34', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001270007, 201910004, 25000, '2020-01-27', 2, '', 1578490768, '2020-01-27 11:32:38', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001270008, 201910011, 50000, '2020-01-27', 2, 'beli salep gudhixx', 1578490768, '2020-01-27 11:33:53', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001270009, 201810004, 25000, '2020-01-27', 2, '', 1578490768, '2020-01-27 11:34:49', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001270010, 201910013, 25000, '2020-01-27', 2, 'nyelengi nggo lamarann', 1578490768, '2020-01-27 11:36:00', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001270011, 201910009, 25000, '2020-01-27', 2, '', 1578490768, '2020-01-27 11:37:12', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001270012, 201810009, 25000, '2020-01-27', 2, 'tuku sempakkk', 1578490768, '2020-01-27 11:38:04', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001270013, 201910008, 25000, '2020-01-27', 2, '', 1578490768, '2020-01-27 11:39:10', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001270014, 201810003, 25000, '2020-01-27', 2, '', 1578490768, '2020-01-27 11:40:09', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001270015, 201910017, 25000, '2020-01-27', 2, '', 1578490768, '2020-01-27 11:41:18', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001270016, 201510003, 25000, '2020-01-27', 2, 'tuku enak enak', 1578490768, '2020-01-27 11:42:43', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001270017, 201910014, 25000, '2020-01-27', 2, 'nggo tuku pemutih raiii', 1578490768, '2020-01-27 11:44:00', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001270018, 201910006, 25000, '2020-01-27', 2, '', 1578490768, '2020-01-27 11:46:01', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001270019, 201910003, 25000, '2020-01-27', 2, '', 1578490768, '2020-01-27 11:46:42', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001270020, 201910001, 25000, '2020-01-27', 2, '', 1578490768, '2020-01-27 11:48:11', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001270021, 201810016, 25000, '2020-01-27', 2, '', 1578490768, '2020-01-27 11:49:07', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001270022, 201910010, 25000, '2020-01-27', 2, '', 1578490768, '2020-01-27 11:50:08', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001270023, 201810007, 25000, '2020-01-27', 2, '', 1578490768, '2020-01-27 11:51:13', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001270024, 201910015, 25000, '2020-01-27', 2, '', 1578490768, '2020-01-27 11:52:06', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001270025, 201910012, 25000, '2020-01-27', 2, '', 1578490768, '2020-01-27 11:52:32', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001300001, 201810011, 25000, '2020-01-30', 2, '', 1578490768, '2020-01-30 04:13:38', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001300002, 201810009, 25000, '2020-01-30', 2, '', 1578490768, '2020-01-30 04:15:25', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001300003, 201910016, 25000, '2020-01-30', 2, '', 1578490768, '2020-01-30 04:16:33', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001300004, 201810008, 25000, '2020-01-30', 2, '', 1578490768, '2020-01-30 04:16:59', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001300005, 201810004, 25000, '2020-01-30', 2, '', 1578490768, '2020-01-30 04:17:48', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001300006, 201910009, 25000, '2020-01-30', 2, '', 1578490768, '2020-01-30 04:18:36', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001300007, 201910001, 25000, '2020-01-30', 2, '', 1578490768, '2020-01-30 04:19:30', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001300008, 201910014, 25000, '2020-01-30', 2, '', 1578490768, '2020-01-30 04:20:12', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001300009, 201810016, 25000, '2020-01-30', 2, '', 1578490768, '2020-01-30 04:21:03', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001300010, 201910004, 25000, '2020-01-30', 2, '', 1578490768, '2020-01-30 04:21:58', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001300011, 201910006, 30000, '2020-01-30', 2, '', 1578490768, '2020-01-30 04:22:55', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001300012, 201910006, 30000, '2020-01-30', 2, 'beli sabun nggo ntabunn', 1578490768, '2020-01-30 04:23:20', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001300013, 201610001, 25000, '2020-01-30', 2, '', 1578490768, '2020-01-30 04:24:25', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001300014, 201910005, 25000, '2020-01-30', 2, '', 1578490768, '2020-01-30 04:26:00', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001300015, 201910003, 25000, '2020-01-30', 2, '', 1578490768, '2020-01-30 04:27:34', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001300016, 201810007, 25000, '2020-01-30', 2, '', 1578490768, '2020-01-30 04:29:03', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001300017, 201910008, 25000, '2020-01-30', 2, '', 1578490768, '2020-01-30 04:29:55', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001300018, 201910010, 25000, '2020-01-30', 2, '', 1578490768, '2020-01-30 04:30:59', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001300019, 201910013, 25000, '2020-01-30', 2, '', 1578490768, '2020-01-30 04:32:03', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001300020, 201810012, 25000, '2020-01-30', 2, '', 1578490768, '2020-01-30 04:33:08', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001300021, 201710001, 25000, '2020-01-30', 2, '', 1578490768, '2020-01-30 04:34:38', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001300022, 201910017, 25000, '2020-01-30', 2, '', 1578490768, '2020-01-30 04:36:09', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001300023, 201510003, 25000, '2020-01-30', 2, '', 1578490768, '2020-01-30 04:37:12', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001300024, 201910011, 25000, '2020-01-30', 2, '', 1578490768, '2020-01-30 04:38:34', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001300025, 201910015, 25000, '2020-01-30', 2, '', 1578490768, '2020-01-30 04:39:36', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202001300026, 201910012, 25000, '2020-01-30', 2, '', 1578490768, '2020-01-30 04:40:33', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002030001, 201910014, 25000, '2020-02-03', 2, '', 1578490768, '2020-02-03 11:26:51', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002030002, 201910016, 25000, '2020-02-03', 2, '', 1578490768, '2020-02-03 11:28:12', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002030003, 201910003, 25000, '2020-02-03', 2, '', 1578490768, '2020-02-03 11:29:09', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002030004, 201710001, 25000, '2020-02-03', 2, '', 1578490768, '2020-02-03 11:29:44', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002030005, 201810007, 25000, '2020-02-03', 2, '', 1578490768, '2020-02-03 11:30:52', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002030006, 201910001, 25000, '2020-02-03', 2, '', 1578490768, '2020-02-03 11:32:35', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002030007, 201810011, 25000, '2020-02-03', 2, '', 1578490768, '2020-02-03 11:33:20', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002030008, 201910002, 25000, '2020-02-03', 2, '', 1578490768, '2020-02-03 11:34:12', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002030009, 201810004, 25000, '2020-02-03', 2, '', 1578490768, '2020-02-03 11:35:23', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002030010, 201910005, 25000, '2020-02-03', 2, '', 1578490768, '2020-02-03 11:36:28', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002030011, 201910009, 25000, '2020-02-03', 2, '', 1578490768, '2020-02-03 11:37:11', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002030012, 201910006, 25000, '2020-02-03', 2, '', 1578490768, '2020-02-03 11:38:06', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002030013, 201910012, 30000, '2020-02-03', 2, '', 1578490768, '2020-02-03 11:39:13', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002030014, 201810016, 25000, '2020-02-03', 2, '', 1578490768, '2020-02-03 11:39:59', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002030015, 201910004, 25000, '2020-02-03', 2, '', 1578490768, '2020-02-03 11:40:40', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002030016, 201610001, 25000, '2020-02-03', 2, '', 1578490768, '2020-02-03 11:42:27', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002030017, 201810008, 25000, '2020-02-03', 2, '', 1578490768, '2020-02-03 11:43:52', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002030018, 201910010, 25000, '2020-02-03', 2, '', 1578490768, '2020-02-03 11:45:12', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002030019, 201810009, 25000, '2020-02-03', 2, '', 1578490768, '2020-02-03 11:45:54', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002030020, 201810012, 25000, '2020-02-03', 2, '', 1578490768, '2020-02-03 11:46:21', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002030021, 201810002, 25000, '2020-02-03', 2, '', 1578490768, '2020-02-03 11:48:16', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002030022, 201510003, 25000, '2020-02-03', 2, '', 1578490768, '2020-02-03 11:49:21', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002030023, 201910013, 25000, '2020-02-03', 2, '', 1578490768, '2020-02-03 11:49:59', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002030024, 201910015, 25000, '2020-02-03', 2, '', 1578490768, '2020-02-03 11:50:45', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002030025, 201910017, 25000, '2020-02-03', 2, '', 1578490768, '2020-02-03 11:51:16', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002030026, 201910011, 50000, '2020-02-03', 2, '', 1578490768, '2020-02-03 11:52:36', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002030027, 201910007, 25000, '2020-02-03', 2, '', 1578490768, '2020-02-03 11:53:58', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002030028, 201910008, 25000, '2020-02-03', 2, '', 1578490768, '2020-02-03 11:54:30', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002060001, 201510003, 30000, '2020-02-06', 2, 'habis', 1577953383, '2020-02-06 03:14:46', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002060002, 201910001, 30000, '2020-02-06', 2, 'obat gatel', 1577953383, '2020-02-06 03:15:48', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002060003, 201810011, 25000, '2020-02-06', 2, '', 1577953383, '2020-02-06 03:16:10', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002060004, 201910009, 25000, '2020-02-06', 2, '', 1577953383, '2020-02-06 03:17:02', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002060005, 201810016, 25000, '2020-02-06', 2, '', 1577953383, '2020-02-06 03:17:14', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002060006, 201810009, 25000, '2020-02-06', 2, '', 1577953383, '2020-02-06 03:17:45', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002060007, 201910016, 25000, '2020-02-06', 2, '', 1577953383, '2020-02-06 03:18:35', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002060008, 201810012, 25000, '2020-02-06', 2, '', 1577953383, '2020-02-06 03:18:48', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002060009, 201910002, 25000, '2020-02-06', 2, '', 1577953383, '2020-02-06 03:19:19', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002060010, 201910005, 25000, '2020-02-06', 2, '', 1577953383, '2020-02-06 03:20:14', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002060011, 201910010, 25000, '2020-02-06', 2, '', 1577953383, '2020-02-06 03:20:29', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002060012, 201910003, 25000, '2020-02-06', 2, '', 1577953383, '2020-02-06 03:20:41', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002060013, 201810002, 25000, '2020-02-06', 2, '', 1577953383, '2020-02-06 03:21:35', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002060014, 201810007, 25000, '2020-02-06', 2, '', 1577953383, '2020-02-06 03:22:15', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002060015, 201910004, 25000, '2020-02-06', 2, '', 1577953383, '2020-02-06 03:22:50', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002060016, 201910008, 40000, '2020-02-06', 2, 'peci', 1577953383, '2020-02-06 03:23:27', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002060017, 201910017, 25000, '2020-02-06', 2, '', 1577953383, '2020-02-06 03:24:17', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002060018, 201810013, 25000, '2020-02-06', 2, '', 1577953383, '2020-02-06 03:24:26', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002060019, 201910014, 25000, '2020-02-06', 2, 'a', 1577953383, '2020-02-06 03:26:56', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002060020, 201810004, 10000, '2020-02-06', 2, 'cukur', 1577953383, '2020-02-06 03:27:24', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002060021, 201910007, 20000, '2020-02-06', 2, '', 1577953383, '2020-02-06 03:28:17', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002060022, 201910006, 15000, '2020-02-06', 2, '', 1577953383, '2020-02-06 03:28:48', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002060023, 201810008, 25000, '2020-02-06', 2, '', 1577953383, '2020-02-06 03:29:15', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002060024, 201910015, 25000, '2020-02-06', 2, '', 1577953383, '2020-02-06 03:29:51', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002060025, 201710001, 20000, '2020-02-06', 2, '', 1577953383, '2020-02-06 03:30:44', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002060026, 201910011, 25000, '2020-02-06', 2, '', 1577953383, '2020-02-06 03:31:40', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002060027, 201610001, 25000, '2020-02-06', 2, '', 1577953383, '2020-02-06 03:36:20', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002060028, 201710002, 25000, '2020-02-06', 2, '', 1577953383, '2020-02-06 03:37:00', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002100001, 201810007, 25000, '2020-02-10', 2, '', 1577953383, '2020-02-10 11:50:31', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002100002, 201810011, 25000, '2020-02-10', 2, '', 1577953383, '2020-02-10 11:50:41', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002100003, 201910016, 25000, '2020-02-10', 2, '', 1577953383, '2020-02-10 11:51:36', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002100004, 201810009, 25000, '2020-02-10', 2, '', 1577953383, '2020-02-10 11:51:47', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002100005, 201610001, 25000, '2020-02-10', 2, '', 1577953383, '2020-02-10 11:52:21', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002100006, 201910003, 25000, '2020-02-10', 2, '', 1577953383, '2020-02-10 11:52:33', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002100007, 201910014, 25000, '2020-02-10', 2, '', 1577953383, '2020-02-10 11:52:58', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002100008, 201910001, 25000, '2020-02-10', 2, '', 1577953383, '2020-02-10 11:53:07', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002100009, 201910009, 50000, '2020-02-10', 2, 'kaos kai, sabun dll\r\n', 1577953383, '2020-02-10 11:54:02', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002100010, 201810016, 25000, '2020-02-10', 2, 'kaos kai, sabun dll\r\n', 1577953383, '2020-02-10 11:54:54', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002100011, 201910006, 25000, '2020-02-10', 2, 'kaos kai, sabun dll\r\n', 1577953383, '2020-02-10 11:55:11', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002100012, 201810002, 25000, '2020-02-10', 2, '\r\n', 1577953383, '2020-02-10 11:55:45', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002100013, 201910011, 25000, '2020-02-10', 2, '\r\n', 1577953383, '2020-02-10 11:55:53', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002100014, 201910002, 25000, '2020-02-10', 2, '\r\n', 1577953383, '2020-02-10 11:56:20', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002100015, 201910008, 25000, '2020-02-10', 2, '\r\n', 1577953383, '2020-02-10 11:56:28', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002100016, 201910017, 25000, '2020-02-10', 2, '', 1577953383, '2020-02-10 11:57:04', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002100017, 201910007, 25000, '2020-02-10', 2, '', 1577953383, '2020-02-10 11:57:15', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002100018, 201810008, 25000, '2020-02-10', 2, '', 1577953383, '2020-02-10 11:57:36', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002100019, 201710001, 25000, '2020-02-10', 2, '', 1577953383, '2020-02-10 11:57:45', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002100020, 201910010, 25000, '2020-02-10', 2, '', 1577953383, '2020-02-10 11:58:11', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002100021, 201810013, 25000, '2020-02-10', 2, '', 1577953383, '2020-02-10 11:58:19', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002100022, 201710002, 25000, '2020-02-10', 2, '', 1577953383, '2020-02-10 11:58:40', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002100023, 201910015, 25000, '2020-02-10', 2, '', 1577953383, '2020-02-10 11:58:51', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002100024, 201810012, 25000, '2020-02-10', 2, '', 1577953383, '2020-02-10 12:00:44', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002130001, 201810011, 25000, '2020-02-13', 2, '', 1578490768, '2020-02-13 04:35:55', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002130002, 201910016, 25000, '2020-02-13', 2, '', 1578490768, '2020-02-13 04:37:12', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002130003, 201910011, 25000, '2020-02-13', 2, '', 1578490768, '2020-02-13 04:37:41', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002130004, 201610001, 25000, '2020-02-13', 2, '', 1578490768, '2020-02-13 04:38:10', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002130005, 201910001, 25000, '2020-02-13', 2, '', 1578490768, '2020-02-13 04:38:52', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002130006, 201910006, 25000, '2020-02-13', 2, '', 1578490768, '2020-02-13 04:39:48', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002130007, 201910003, 25000, '2020-02-13', 2, '', 1578490768, '2020-02-13 04:41:09', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002130008, 201810002, 25000, '2020-02-13', 2, '', 1578490768, '2020-02-13 04:41:36', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002130009, 201910009, 25000, '2020-02-13', 2, '', 1578490768, '2020-02-13 04:42:19', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002130010, 201810008, 25000, '2020-02-13', 2, '', 1578490768, '2020-02-13 04:42:45', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002130011, 201910007, 25000, '2020-02-13', 2, '', 1578490768, '2020-02-13 04:43:25', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002130012, 201910005, 25000, '2020-02-13', 2, '', 1578490768, '2020-02-13 04:43:58', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002130013, 201910017, 25000, '2020-02-13', 2, '', 1578490768, '2020-02-13 04:44:31', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002130014, 201810007, 25000, '2020-02-13', 2, '', 1578490768, '2020-02-13 04:45:25', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002130015, 201910014, 25000, '2020-02-13', 2, '', 1578490768, '2020-02-13 04:46:27', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002130016, 201910010, 25000, '2020-02-13', 2, '', 1578490768, '2020-02-13 04:46:57', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002130017, 201810004, 25000, '2020-02-13', 2, '', 1578490768, '2020-02-13 04:47:28', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002130018, 201910015, 25000, '2020-02-13', 2, '', 1578490768, '2020-02-13 04:47:59', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002130019, 201810012, 25000, '2020-02-13', 2, '', 1578490768, '2020-02-13 04:48:38', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002130020, 201910008, 25000, '2020-02-13', 2, '', 1578490768, '2020-02-13 04:48:59', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002130021, 201810009, 50000, '2020-02-13', 2, '', 1578490768, '2020-02-13 04:49:42', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002130022, 201810009, 50000, '2020-02-13', 2, 'celana osis\r\n', 1578490768, '2020-02-13 04:49:59', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002130023, 201710002, 25000, '2020-02-13', 2, '', 1578490768, '2020-02-13 04:51:56', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002130024, 201810013, 25000, '2020-02-13', 2, '', 1578490768, '2020-02-13 04:52:18', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002130025, 201710001, 5000, '2020-02-13', 2, '', 1578490768, '2020-02-13 04:54:12', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002170001, 201910001, 20000, '2020-02-17', 2, '', 1578490768, '2020-02-17 01:38:15', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002170002, 201910009, 30000, '2020-02-17', 2, 'beli odol', 1578490768, '2020-02-17 01:39:15', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002170003, 201810012, 25000, '2020-02-17', 2, '', 1578490768, '2020-02-17 01:39:46', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002170004, 201910010, 25000, '2020-02-17', 2, '', 1578490768, '2020-02-17 01:40:15', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002170005, 201910005, 25000, '2020-02-17', 2, '', 1578490768, '2020-02-17 01:40:53', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002170006, 201910003, 25000, '2020-02-17', 2, '', 1578490768, '2020-02-17 01:41:12', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002170007, 201810011, 25000, '2020-02-17', 2, '', 1578490768, '2020-02-17 01:41:45', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002170008, 201810009, 25000, '2020-02-17', 2, '', 1578490768, '2020-02-17 01:42:10', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002170009, 201910016, 25000, '2020-02-17', 2, '', 1578490768, '2020-02-17 01:42:38', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002170010, 201910002, 25000, '2020-02-17', 2, '', 1578490768, '2020-02-17 01:42:57', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002170011, 201810016, 25000, '2020-02-17', 2, '', 1578490768, '2020-02-17 01:43:17', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002170012, 201810007, 25000, '2020-02-17', 2, '', 1578490768, '2020-02-17 01:43:37', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002170013, 201710002, 25000, '2020-02-17', 2, '', 1578490768, '2020-02-17 01:45:42', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002170014, 201810008, 30000, '2020-02-17', 2, 'tali sepatu', 1578490768, '2020-02-17 01:46:22', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002170015, 201910006, 20000, '2020-02-17', 2, '', 1578490768, '2020-02-17 01:47:04', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002170016, 201810002, 25000, '2020-02-17', 2, '', 1578490768, '2020-02-17 01:47:32', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002170017, 201910011, 25000, '2020-02-17', 2, '', 1578490768, '2020-02-17 01:47:49', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002170018, 201910014, 25000, '2020-02-17', 2, '', 1578490768, '2020-02-17 01:48:09', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002170019, 201810003, 25000, '2020-02-17', 2, '', 1578490768, '2020-02-17 01:48:58', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002170020, 201910017, 25000, '2020-02-17', 2, '', 1578490768, '2020-02-17 01:49:26', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002170021, 201510003, 25000, '2020-02-17', 2, '', 1578490768, '2020-02-17 01:50:28', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002170022, 201610001, 25000, '2020-02-17', 2, '', 1578490768, '2020-02-17 01:50:50', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002170023, 201810004, 25000, '2020-02-17', 2, '', 1578490768, '2020-02-17 01:51:15', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002170024, 201910015, 25000, '2020-02-17', 2, '', 1578490768, '2020-02-17 01:51:41', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002170025, 201810013, 25000, '2020-02-17', 2, '', 1578490768, '2020-02-17 01:52:09', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002170026, 201710001, 25000, '2020-02-17', 2, '', 1578490768, '2020-02-17 01:54:07', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002170027, 201910008, 50000, '2020-02-17', 2, 'beli celana pramuka', 1578490768, '2020-02-17 01:54:46', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002200001, 201910010, 25000, '2020-02-20', 2, '', 1578490768, '2020-02-20 03:22:16', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002200002, 201810005, 25000, '2020-02-20', 2, '', 1578490768, '2020-02-20 03:22:46', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002200003, 201610001, 25000, '2020-02-20', 2, '', 1578490768, '2020-02-20 03:23:41', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002200004, 201910008, 25000, '2020-02-20', 2, '', 1578490768, '2020-02-20 03:24:00', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002200005, 201910006, 25000, '2020-02-20', 2, '', 1578490768, '2020-02-20 03:24:28', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002200006, 201910016, 25000, '2020-02-20', 2, '', 1578490768, '2020-02-20 03:25:06', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002200007, 201910003, 25000, '2020-02-20', 2, '', 1578490768, '2020-02-20 03:25:47', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002200008, 201810007, 25000, '2020-02-20', 2, '', 1578490768, '2020-02-20 03:26:03', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002200009, 201810011, 25000, '2020-02-20', 2, '', 1578490768, '2020-02-20 03:26:47', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002200010, 201810009, 25000, '2020-02-20', 2, '', 1578490768, '2020-02-20 03:27:11', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002200011, 201910005, 25000, '2020-02-20', 2, '', 1578490768, '2020-02-20 03:27:54', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002200012, 201910002, 25000, '2020-02-20', 2, '', 1578490768, '2020-02-20 03:28:31', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002200013, 201810002, 25000, '2020-02-20', 2, '', 1578490768, '2020-02-20 03:29:03', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002200014, 201810004, 25000, '2020-02-20', 2, '', 1578490768, '2020-02-20 03:29:22', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002200015, 201910015, 25000, '2020-02-20', 2, '', 1578490768, '2020-02-20 03:29:38', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002200016, 201910009, 25000, '2020-02-20', 2, '', 1578490768, '2020-02-20 03:30:04', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002200017, 201910017, 25000, '2020-02-20', 2, '', 1578490768, '2020-02-20 03:30:37', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002200018, 201910014, 25000, '2020-02-20', 2, '', 1578490768, '2020-02-20 03:31:15', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002200019, 201910012, 25000, '2020-02-20', 2, '', 1578490768, '2020-02-20 03:31:58', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002200020, 201510003, 25000, '2020-02-20', 2, '', 1578490768, '2020-02-20 03:32:25', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002200021, 201910011, 25000, '2020-02-20', 2, '', 1578490768, '2020-02-20 03:32:45', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002200022, 201810012, 25000, '2020-02-20', 2, '', 1578490768, '2020-02-20 03:33:09', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002240001, 201810011, 25000, '2020-02-24', 2, '', 1578490768, '2020-02-24 11:16:08', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002240002, 201810008, 25000, '2020-02-24', 2, '', 1578490768, '2020-02-24 11:16:41', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002240003, 201910003, 25000, '2020-02-24', 2, '', 1578490768, '2020-02-24 11:17:28', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002240004, 201910009, 25000, '2020-02-24', 2, '', 1578490768, '2020-02-24 11:17:55', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002240005, 201910002, 25000, '2020-02-24', 2, '', 1578490768, '2020-02-24 11:18:27', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002240006, 201910017, 25000, '2020-02-24', 2, '', 1578490768, '2020-02-24 11:18:45', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002240007, 201810003, 25000, '2020-02-24', 2, '', 1578490768, '2020-02-24 11:19:31', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002240008, 201910006, 25000, '2020-02-24', 2, '', 1578490768, '2020-02-24 11:19:49', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002240009, 201710002, 25000, '2020-02-24', 2, '', 1578490768, '2020-02-24 11:20:16', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002240010, 201910011, 25000, '2020-02-24', 2, '', 1578490768, '2020-02-24 11:20:41', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002240011, 201810016, 25000, '2020-02-24', 2, '', 1578490768, '2020-02-24 11:21:34', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002240012, 201710001, 25000, '2020-02-24', 2, '', 1578490768, '2020-02-24 11:22:05', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002240013, 201810002, 25000, '2020-02-24', 2, '', 1578490768, '2020-02-24 11:22:22', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002240014, 201510003, 25000, '2020-02-24', 2, '', 1578490768, '2020-02-24 11:22:54', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002240015, 201810009, 25000, '2020-02-24', 2, '', 1578490768, '2020-02-24 11:23:15', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002240016, 201910015, 25000, '2020-02-24', 2, '', 1578490768, '2020-02-24 11:23:52', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002240017, 201810007, 25000, '2020-02-24', 2, '', 1578490768, '2020-02-24 11:24:30', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002240018, 201810005, 25000, '2020-02-24', 2, '', 1578490768, '2020-02-24 11:24:56', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002240019, 201910012, 25000, '2020-02-24', 2, '', 1578490768, '2020-02-24 11:25:42', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002240020, 201910010, 50000, '2020-02-24', 2, '', 1578490768, '2020-02-24 11:26:41', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002240021, 201910008, 25000, '2020-02-24', 2, '', 1578490768, '2020-02-24 11:27:14', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002240022, 201810004, 40000, '2020-02-24', 2, 'sangu kemah', 1578490768, '2020-02-24 11:28:03', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002240023, 201610001, 25000, '2020-02-24', 2, '', 1578490768, '2020-02-24 11:28:30', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002240024, 201610001, 15000, '2020-02-24', 4, '', 1578490768, '2020-02-24 11:28:56', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002240025, 201910014, 25000, '2020-02-24', 2, '', 1578490768, '2020-02-24 11:30:14', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002240026, 201910016, 25000, '2020-02-24', 2, '', 1578490768, '2020-02-24 11:30:34', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002270001, 201810007, 25000, '2020-02-27', 2, '', 1578490768, '2020-02-27 04:07:43', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002270002, 201810011, 25000, '2020-02-27', 2, '', 1578490768, '2020-02-27 04:08:05', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002270003, 201810008, 20000, '2020-02-27', 2, '', 1578490768, '2020-02-27 04:08:38', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002270004, 201610001, 25000, '2020-02-27', 2, '', 1578490768, '2020-02-27 04:09:18', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002270005, 201810016, 25000, '2020-02-27', 2, '', 1578490768, '2020-02-27 04:09:45', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002270006, 201810003, 25000, '2020-02-27', 2, '', 1578490768, '2020-02-27 04:11:42', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002270007, 201910008, 25000, '2020-02-27', 2, '', 1578490768, '2020-02-27 04:12:13', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002270008, 201710001, 25000, '2020-02-27', 2, '', 1578490768, '2020-02-27 04:12:44', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002270009, 201810009, 25000, '2020-02-27', 2, '', 1578490768, '2020-02-27 04:13:09', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002270010, 201910017, 25000, '2020-02-27', 2, '', 1578490768, '2020-02-27 04:13:54', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002270011, 201910014, 25000, '2020-02-27', 2, '', 1578490768, '2020-02-27 04:14:34', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002270012, 201910012, 25000, '2020-02-27', 2, '', 1578490768, '2020-02-27 04:16:02', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002270013, 201810005, 25000, '2020-02-27', 2, '', 1578490768, '2020-02-27 04:16:21', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002270014, 201910010, 25000, '2020-02-27', 2, '', 1578490768, '2020-02-27 04:16:53', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002270015, 201810013, 25000, '2020-02-27', 2, '', 1578490768, '2020-02-27 04:17:12', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002270016, 201910009, 25000, '2020-02-27', 2, 'beli obat', 1578490768, '2020-02-27 04:17:52', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002270017, 201810002, 25000, '2020-02-27', 2, '', 1578490768, '2020-02-27 04:19:42', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002270018, 201510003, 25000, '2020-02-27', 2, '', 1578490768, '2020-02-27 04:20:04', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002270019, 201710002, 30000, '2020-02-27', 2, '', 1578490768, '2020-02-27 04:20:35', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002270020, 201910015, 50000, '2020-02-27', 2, 'beli peci', 1578490768, '2020-02-27 04:21:26', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002270021, 201910015, 50000, '2020-02-27', 2, 'beli peci', 1578490768, '2020-02-27 04:21:48', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002270022, 201910011, 25000, '2020-02-27', 2, '', 1578490768, '2020-02-27 04:22:18', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002270023, 201910006, 15000, '2020-02-27', 2, '', 1578490768, '2020-02-27 04:25:34', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002270024, 201910009, 30000, '2020-02-27', 2, '', 1578490768, '2020-02-27 04:27:37', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002270025, 201810012, 25000, '2020-02-27', 2, '', 1578490768, '2020-02-27 04:27:57', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002270026, 201810004, 25000, '2020-02-27', 2, '', 1578490768, '2020-02-27 04:28:18', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202002270027, 201910016, 25000, '2020-02-27', 2, '', 1578490768, '2020-02-27 04:28:47', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003020001, 201810016, 25000, '2020-03-02', 2, '', 1578490768, '2020-03-02 11:26:51', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003020002, 201810007, 25000, '2020-03-02', 2, '', 1578490768, '2020-03-02 11:27:13', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003020003, 201810009, 25000, '2020-03-02', 2, '', 1578490768, '2020-03-02 11:28:37', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003020004, 201910009, 25000, '2020-03-02', 2, '', 1578490768, '2020-03-02 11:28:58', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003020005, 201910002, 25000, '2020-03-02', 2, '', 1578490768, '2020-03-02 11:29:50', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003020006, 201910010, 25000, '2020-03-02', 2, '', 1578490768, '2020-03-02 11:30:06', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003020007, 201810011, 25000, '2020-03-02', 2, '', 1578490768, '2020-03-02 11:30:30', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003020008, 201910015, 25000, '2020-03-02', 2, '', 1578490768, '2020-03-02 11:30:51', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003020009, 201910011, 25000, '2020-03-02', 2, '', 1578490768, '2020-03-02 11:31:23', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003020010, 201710002, 25000, '2020-03-02', 2, '', 1578490768, '2020-03-02 11:31:43', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003020011, 201910017, 25000, '2020-03-02', 2, '', 1578490768, '2020-03-02 11:32:39', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003020012, 201810005, 25000, '2020-03-02', 2, '', 1578490768, '2020-03-02 11:32:56', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003020013, 201810003, 25000, '2020-03-02', 2, '', 1578490768, '2020-03-02 11:33:24', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003020014, 201710001, 25000, '2020-03-02', 2, '', 1578490768, '2020-03-02 11:34:02', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003020015, 201910014, 25000, '2020-03-02', 2, '', 1578490768, '2020-03-02 11:34:37', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003020016, 201910012, 25000, '2020-03-02', 2, '', 1578490768, '2020-03-02 11:35:19', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003020017, 201810002, 25000, '2020-03-02', 2, '', 1578490768, '2020-03-02 11:36:28', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003020018, 201510003, 25000, '2020-03-02', 2, '', 1578490768, '2020-03-02 11:37:14', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003050001, 201910009, 25000, '2020-03-05', 2, '', 1578490768, '2020-03-05 04:26:55', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003050002, 201810011, 25000, '2020-03-05', 2, '', 1578490768, '2020-03-05 04:27:28', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003050003, 201910010, 25000, '2020-03-05', 2, '', 1578490768, '2020-03-05 04:27:48', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003050004, 201910005, 25000, '2020-03-05', 2, '', 1578490768, '2020-03-05 04:28:08', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003050005, 201910005, 25000, '2020-03-05', 2, '', 1578490768, '2020-03-05 04:28:16', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003050006, 201910002, 25000, '2020-03-05', 2, '', 1578490768, '2020-03-05 04:28:51', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003050007, 201810016, 25000, '2020-03-05', 2, '', 1578490768, '2020-03-05 04:29:36', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003050008, 201910008, 50000, '2020-03-05', 2, 'plus shidiq', 1578490768, '2020-03-05 04:30:13', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003050009, 201910011, 25000, '2020-03-05', 2, '', 1578490768, '2020-03-05 04:30:54', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003050010, 201810003, 25000, '2020-03-05', 2, '', 1578490768, '2020-03-05 04:31:16', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003050011, 201910015, 25000, '2020-03-05', 2, '', 1578490768, '2020-03-05 04:31:38', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003050012, 201910017, 25000, '2020-03-05', 2, '', 1578490768, '2020-03-05 04:32:02', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003050013, 201810002, 25000, '2020-03-05', 2, '', 1578490768, '2020-03-05 04:32:33', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003060001, 201710001, 25000, '2020-03-05', 2, '', 1578490768, '2020-03-06 07:14:13', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003060002, 201710002, 20000, '2020-03-05', 2, '', 1578490768, '2020-03-06 07:15:02', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003060003, 201510003, 25000, '2020-03-05', 2, '', 1578490768, '2020-03-06 07:15:34', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003060004, 201810012, 25000, '2020-03-05', 2, '', 1578490768, '2020-03-06 07:16:06', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003060005, 201810007, 25000, '2020-03-05', 2, '', 1578490768, '2020-03-06 07:17:02', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003060006, 201910016, 25000, '2020-03-05', 2, '', 1578490768, '2020-03-06 07:17:26', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003060007, 201810004, 25000, '2020-03-05', 2, '', 1578490768, '2020-03-06 07:18:40', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003060008, 201810004, 25000, '2020-03-05', 2, '', 1578490768, '2020-03-06 07:19:11', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003070001, 201810008, 25000, '2020-03-07', 2, '', 1578490768, '2020-03-07 12:10:28', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003090001, 201810008, 25000, '2020-03-09', 2, '', 1578490768, '2020-03-09 11:58:44', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003090002, 201910011, 25000, '2020-03-09', 2, '', 1578490768, '2020-03-09 12:00:06', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003090003, 201810016, 25000, '2020-03-09', 2, '', 1578490768, '2020-03-09 12:00:42', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003090004, 201910016, 25000, '2020-03-09', 2, '', 1578490768, '2020-03-09 12:01:12', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003090005, 201810011, 25000, '2020-03-09', 2, '', 1578490768, '2020-03-09 12:01:38', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003090006, 201710001, 25000, '2020-03-09', 2, '', 1578490768, '2020-03-09 12:02:10', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003090007, 201910002, 25000, '2020-03-09', 2, '', 1578490768, '2020-03-09 12:02:40', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003090008, 201910005, 25000, '2020-03-09', 2, '', 1578490768, '2020-03-09 12:03:33', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003090009, 201910006, 25000, '2020-03-09', 2, '', 1578490768, '2020-03-09 12:05:53', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003090010, 201910017, 25000, '2020-03-09', 2, '', 1578490768, '2020-03-09 12:06:20', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003090011, 201910010, 25000, '2020-03-09', 2, '', 1578490768, '2020-03-09 12:06:48', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003090012, 201910015, 25000, '2020-03-09', 2, '', 1578490768, '2020-03-09 12:07:18', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003090013, 201810007, 25000, '2020-03-09', 2, '', 1578490768, '2020-03-09 12:07:51', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003090014, 201810002, 25000, '2020-03-09', 2, '', 1578490768, '2020-03-09 12:09:02', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003090015, 201910008, 25000, '2020-03-09', 2, '', 1578490768, '2020-03-09 12:09:31', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003090016, 201910014, 25000, '2020-03-09', 2, '', 1578490768, '2020-03-09 12:10:11', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003090017, 201810003, 25000, '2020-03-09', 2, '', 1578490768, '2020-03-09 12:10:36', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003090018, 201810004, 25000, '2020-03-09', 2, '', 1578490768, '2020-03-09 12:11:01', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003090019, 201510003, 25000, '2020-03-09', 2, '', 1578490768, '2020-03-09 12:11:37', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003090020, 201810012, 25000, '2020-03-09', 2, '', 1578490768, '2020-03-09 12:13:07', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003090021, 201610001, 25000, '2020-03-09', 2, '', 1578490768, '2020-03-09 12:15:57', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003090022, 201610001, 25000, '2020-03-09', 2, '', 1578490768, '2020-03-09 12:16:16', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003090023, 201710002, 25000, '2020-03-09', 2, '', 1578490768, '2020-03-09 12:16:42', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003090024, 201810013, 25000, '2020-03-09', 2, '', 1578490768, '2020-03-09 12:17:27', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003090025, 201910009, 25000, '2020-03-09', 2, '', 1578490768, '2020-03-09 12:18:59', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003120001, 201610001, 25000, '2020-03-12', 2, 'sangu', 1577953383, '2020-03-12 04:29:38', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003120002, 201910010, 25000, '2020-03-12', 2, 'sangu', 1577953383, '2020-03-12 04:31:30', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003120003, 201910002, 25000, '2020-03-12', 2, 'sangu', 1577953383, '2020-03-12 04:32:00', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003120004, 201810008, 25000, '2020-03-12', 2, 'sangu', 1577953383, '2020-03-12 04:32:30', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003120005, 201510003, 25000, '2020-03-12', 2, 'sangu', 1577953383, '2020-03-12 04:32:54', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003120006, 201910009, 25000, '2020-03-12', 2, 'sangu', 1577953383, '2020-03-12 04:33:20', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003120007, 201910011, 30000, '2020-03-12', 2, 'sangu', 1577953383, '2020-03-12 04:34:04', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003120008, 201810007, 35000, '2020-03-12', 2, 'sangu + sandal', 1577953383, '2020-03-12 04:34:46', 1577953383, '2020-03-12 04:35:34');
INSERT INTO `trans` VALUES (202003120009, 201910016, 30000, '2020-03-12', 2, 'sangu', 1577953383, '2020-03-12 04:36:40', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003120010, 201810004, 30000, '2020-03-12', 2, 'sangu', 1577953383, '2020-03-12 04:37:23', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003120011, 201910008, 30000, '2020-03-12', 2, 'sangu', 1577953383, '2020-03-12 04:37:51', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003120012, 201810011, 25000, '2020-03-12', 2, 'sangu', 1577953383, '2020-03-12 04:40:06', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003120013, 201810012, 30000, '2020-03-12', 2, 'sangu', 1577953383, '2020-03-12 04:40:32', 1577953383, '2020-03-12 04:41:35');
INSERT INTO `trans` VALUES (202003120014, 201810016, 30000, '2020-03-12', 2, 'sangu', 1577953383, '2020-03-12 04:42:31', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003120015, 201910006, 30000, '2020-03-12', 2, 'sangu', 1577953383, '2020-03-12 04:42:57', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003120016, 201910015, 25000, '2020-03-12', 2, 'sangu', 1577953383, '2020-03-12 04:43:38', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003120017, 201710001, 25000, '2020-03-12', 2, 'sangu', 1577953383, '2020-03-12 04:44:00', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003120018, 201910017, 30000, '2020-03-12', 2, 'sangu', 1577953383, '2020-03-12 04:44:29', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003120019, 201810002, 25000, '2020-03-12', 2, 'sangu', 1577953383, '2020-03-12 04:45:28', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003120020, 201910014, 25000, '2020-03-12', 2, 'sangu', 1577953383, '2020-03-12 04:45:53', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003120021, 201710002, 30000, '2020-03-12', 2, 'sangu', 1577953383, '2020-03-12 04:46:30', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003160001, 201910004, 50000, '2020-03-16', 2, 'keluar', 1578490768, '2020-03-16 04:59:14', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003160002, 201910004, 50000, '2020-03-16', 2, 'keluar', 1578490768, '2020-03-16 04:59:25', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003160003, 201910004, 50000, '2020-03-16', 2, 'keluar', 1578490768, '2020-03-16 04:59:32', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003160004, 201910004, 25000, '2020-03-16', 2, 'keluar', 1578490768, '2020-03-16 04:59:44', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003160005, 201810009, 25000, '2020-03-16', 2, '', 1578490768, '2020-03-16 11:22:23', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003160006, 201910014, 25000, '2020-03-16', 2, '', 1578490768, '2020-03-16 11:22:54', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003160007, 201910006, 25000, '2020-03-16', 2, '', 1578490768, '2020-03-16 11:23:25', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003160008, 201810016, 25000, '2020-03-16', 2, '', 1578490768, '2020-03-16 11:23:42', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003160009, 201910003, 25000, '2020-03-16', 2, '', 1578490768, '2020-03-16 11:24:16', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003160010, 201710001, 25000, '2020-03-16', 2, '', 1578490768, '2020-03-16 11:24:40', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003160011, 201910009, 25000, '2020-03-16', 2, '', 1578490768, '2020-03-16 11:25:22', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003160012, 201810008, 25000, '2020-03-16', 2, '', 1578490768, '2020-03-16 11:25:45', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003160013, 201910005, 25000, '2020-03-16', 2, '', 1578490768, '2020-03-16 11:26:11', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003160014, 201710002, 25000, '2020-03-16', 2, '', 1578490768, '2020-03-16 11:26:38', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003160015, 201910002, 25000, '2020-03-16', 2, '', 1578490768, '2020-03-16 11:27:07', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003160016, 201810012, 25000, '2020-03-16', 2, '', 1578490768, '2020-03-16 11:27:32', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003160017, 201910008, 25000, '2020-03-16', 2, '', 1578490768, '2020-03-16 11:28:19', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003160018, 201610001, 25000, '2020-03-16', 2, '', 1578490768, '2020-03-16 11:28:43', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003160019, 201910017, 25000, '2020-03-16', 2, '', 1578490768, '2020-03-16 11:29:06', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003160020, 201810007, 25000, '2020-03-16', 2, '', 1578490768, '2020-03-16 11:29:46', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003160021, 201810004, 25000, '2020-03-16', 2, '', 1578490768, '2020-03-16 11:30:08', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003160022, 201910011, 25000, '2020-03-16', 2, '', 1578490768, '2020-03-16 11:30:34', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003160023, 201910015, 25000, '2020-03-16', 2, '', 1578490768, '2020-03-16 11:31:00', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003160024, 201910016, 30000, '2020-03-16', 2, '', 1578490768, '2020-03-16 11:32:00', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003190001, 201610001, 25000, '2020-03-19', 2, '', 1577953383, '2020-03-19 04:55:49', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003190002, 201910002, 25000, '2020-03-19', 2, '', 1577953383, '2020-03-19 04:56:08', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003190003, 201810012, 25000, '2020-03-19', 2, '', 1577953383, '2020-03-19 04:56:35', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003190004, 201810009, 25000, '2020-03-19', 2, '', 1577953383, '2020-03-19 04:56:49', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003190005, 201810008, 25000, '2020-03-19', 2, '', 1577953383, '2020-03-19 04:57:08', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003190006, 201910010, 25000, '2020-03-19', 2, '', 1577953383, '2020-03-19 04:57:46', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003190007, 201910003, 25000, '2020-03-19', 2, '', 1577953383, '2020-03-19 04:58:14', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003190008, 201810004, 25000, '2020-03-19', 2, '', 1577953383, '2020-03-19 04:58:23', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003190009, 201810011, 25000, '2020-03-19', 2, '', 1577953383, '2020-03-19 04:58:49', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003190010, 201910014, 25000, '2020-03-19', 2, '', 1577953383, '2020-03-19 04:59:04', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003190011, 201910006, 25000, '2020-03-19', 2, '', 1577953383, '2020-03-19 04:59:22', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003190012, 201710002, 25000, '2020-03-19', 2, '', 1577953383, '2020-03-19 04:59:46', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003190013, 201910011, 25000, '2020-03-19', 2, '', 1577953383, '2020-03-19 05:00:08', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003190014, 201910009, 25000, '2020-03-19', 2, '', 1577953383, '2020-03-19 05:00:19', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003190015, 201910017, 25000, '2020-03-19', 2, '', 1577953383, '2020-03-19 05:01:32', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003190016, 201810007, 25000, '2020-03-19', 2, '', 1577953383, '2020-03-19 05:01:47', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003190017, 201910015, 25000, '2020-03-19', 2, '', 1577953383, '2020-03-19 05:02:30', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003190018, 201910008, 25000, '2020-03-19', 2, '', 1577953383, '2020-03-19 05:02:44', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003190019, 201910016, 25000, '2020-03-19', 2, '', 1577953383, '2020-03-19 05:03:06', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003190020, 201510003, 25000, '2020-03-19', 2, '', 1577953383, '2020-03-19 05:05:34', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003190021, 201810010, 25000, '2020-03-19', 2, '', 1577953383, '2020-03-19 05:06:00', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003190022, 201810002, 25000, '2020-03-19', 2, '', 1577953383, '2020-03-19 05:06:56', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003230001, 201810009, 25000, '2020-03-23', 2, '', 1577953383, '2020-03-23 07:17:38', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003230002, 201910014, 25000, '2020-03-23', 2, '', 1577953383, '2020-03-23 07:17:59', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003230003, 201810011, 25000, '2020-03-23', 2, '', 1577953383, '2020-03-23 07:18:34', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003230004, 201710002, 25000, '2020-03-23', 2, '', 1577953383, '2020-03-23 07:18:49', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003230005, 201810012, 25000, '2020-03-23', 2, '', 1577953383, '2020-03-23 07:19:17', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003230006, 201910008, 25000, '2020-03-23', 2, '', 1577953383, '2020-03-23 07:19:46', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003230007, 201910006, 25000, '2020-03-23', 2, '', 1577953383, '2020-03-23 07:20:12', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003230008, 201610001, 25000, '2020-03-23', 2, '', 1577953383, '2020-03-23 07:20:43', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003230009, 201810002, 25000, '2020-03-23', 2, '', 1577953383, '2020-03-23 07:21:07', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003230010, 201810007, 25000, '2020-03-23', 2, '', 1577953383, '2020-03-23 07:21:18', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003230011, 201810016, 20000, '2020-03-23', 2, '', 1577953383, '2020-03-23 07:21:55', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003230012, 201910017, 25000, '2020-03-23', 2, '', 1577953383, '2020-03-23 07:22:40', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003230013, 201810008, 25000, '2020-03-23', 2, '', 1577953383, '2020-03-23 07:23:27', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003230014, 201910009, 25000, '2020-03-23', 2, '', 1577953383, '2020-03-23 07:23:39', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003230015, 201910011, 25000, '2020-03-23', 2, '', 1577953383, '2020-03-23 07:23:58', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003230016, 201910010, 25000, '2020-03-23', 2, '', 1577953383, '2020-03-23 07:24:07', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003230017, 201810004, 10000, '2020-03-23', 2, '', 1577953383, '2020-03-23 07:24:41', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003230018, 201910015, 25000, '2020-03-23', 2, '', 1577953383, '2020-03-23 07:25:16', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003230019, 201510003, 25000, '2020-03-23', 2, '', 1577953383, '2020-03-23 07:29:40', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003230020, 201810010, 25000, '2020-03-23', 2, '', 1577953383, '2020-03-23 07:29:52', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003230021, 201710001, 25000, '2020-03-23', 2, '', 1577953383, '2020-03-23 08:53:32', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290001, 201710002, 25000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:15:54', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290002, 201710002, 25000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:16:03', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290003, 201710002, 20000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:16:11', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290004, 201510003, 25000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:16:31', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290005, 201510003, 25000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:16:38', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290006, 201510003, 25000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:16:44', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290007, 201510003, 25000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:16:50', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290008, 201510003, 25000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:16:58', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290009, 201510003, 25000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:17:05', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290010, 201810009, 25000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:17:23', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290011, 201810009, 25000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:17:36', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290012, 201810009, 25000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:17:41', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290013, 201810009, 25000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:17:47', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290014, 201810009, 25000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:17:52', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290015, 201810009, 25000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:17:58', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290016, 201810009, 20000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:18:07', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290017, 201910017, 25000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:18:51', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290018, 201910017, 25000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:18:58', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290019, 201910017, 25000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:19:04', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290020, 201910017, 20000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:19:15', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290021, 201810002, 20000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:19:34', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290022, 201810002, 5000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:19:47', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290023, 201910010, 25000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:20:07', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290024, 201910010, 25000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:20:34', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290025, 201910010, 25000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:20:41', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290026, 201910010, 25000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:20:48', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290027, 201810011, 25000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:21:03', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290028, 201810011, 25000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:21:09', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290029, 201910008, 25000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:21:29', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290030, 201910008, 25000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:21:35', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290031, 201910014, 25000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:22:18', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290032, 201910014, 25000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:22:25', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290033, 201910014, 25000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:22:32', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290034, 201910006, 25000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:22:45', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290035, 201910006, 25000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:22:53', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290036, 201910006, 20000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:23:03', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290037, 201910009, 25000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:23:19', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290038, 201910009, 25000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:23:30', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290039, 201710001, 25000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:24:19', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290040, 201710001, 25000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:24:25', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290041, 201710001, 25000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:24:32', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290042, 201710001, 25000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:24:39', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290043, 201710001, 25000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:24:44', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290044, 201710001, 25000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:24:50', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290045, 201710001, 25000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:24:55', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290046, 201810004, 25000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:25:10', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290047, 201810004, 25000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:25:18', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290048, 201910011, 25000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:25:32', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290049, 201910011, 25000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:25:39', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290050, 201910011, 25000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:25:46', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290051, 201910011, 25000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:25:52', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290052, 201610001, 25000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:26:08', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290053, 201610001, 25000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:26:14', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290054, 201610001, 10000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:26:23', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290055, 201910015, 25000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:26:42', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290056, 201910015, 25000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:26:50', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290057, 201910015, 25000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:26:56', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290058, 201810007, 25000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:27:32', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202003290059, 201810007, 25000, '2020-03-26', 2, '', 1577953383, '2020-03-29 10:27:39', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007110001, 202010003, 30000, '2020-07-10', 2, '', 1577953383, '2020-07-11 08:12:27', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007110002, 202010010, 50000, '2020-07-10', 2, '', 1577953383, '2020-07-11 08:13:01', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007110003, 201910015, 50000, '2020-07-10', 2, '', 1577953383, '2020-07-11 08:13:33', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007110004, 201910005, 50000, '2020-07-10', 2, '', 1577953383, '2020-07-11 08:13:59', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007110005, 201910006, 40000, '2020-07-10', 2, '', 1577953383, '2020-07-11 08:14:25', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007110006, 202010008, 50000, '2020-07-10', 2, '', 1577953383, '2020-07-11 08:15:14', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007110007, 202010009, 25000, '2020-07-10', 2, '', 1577953383, '2020-07-11 08:15:37', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007110008, 201810011, 50000, '2020-07-10', 2, '', 1577953383, '2020-07-11 08:16:06', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007110009, 202010001, 30000, '2020-07-10', 2, '', 1577953383, '2020-07-11 08:16:56', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007110010, 202010011, 50000, '2020-07-10', 2, '', 1577953383, '2020-07-11 08:17:24', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007110011, 201910011, 50000, '2020-07-10', 2, '', 1577953383, '2020-07-11 08:18:11', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007160001, 202010016, 25000, '2020-07-16', 2, '', 1577953383, '2020-07-16 02:57:24', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007160002, 201910002, 50000, '2020-07-16', 2, '', 1577953383, '2020-07-16 02:58:54', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007160003, 202010001, 50000, '2020-07-16', 2, '', 1577953383, '2020-07-16 03:01:50', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007160004, 202010003, 50000, '2020-07-16', 2, '', 1577953383, '2020-07-16 03:02:36', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007160005, 202010005, 30000, '2020-07-16', 2, '', 1577953383, '2020-07-16 03:03:30', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007160006, 202010005, 30000, '2020-07-16', 2, '', 1577953383, '2020-07-16 03:04:15', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007160007, 202010014, 50000, '2020-07-16', 2, '', 1577953383, '2020-07-16 03:05:34', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007160008, 201810010, 50000, '2020-07-16', 2, '', 1577953383, '2020-07-16 03:07:20', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007160009, 202010011, 50000, '2020-07-16', 2, '', 1577953383, '2020-07-16 03:08:03', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007160010, 201810008, 50000, '2020-07-16', 2, '', 1577953383, '2020-07-16 03:10:27', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007160011, 201910010, 50000, '2020-07-16', 2, '', 1577953383, '2020-07-16 03:11:05', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007160012, 201610001, 50000, '2020-07-09', 2, '', 1577953383, '2020-07-16 03:12:32', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007160013, 201610001, 50000, '2020-07-16', 2, '', 1577953383, '2020-07-16 03:12:58', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007160014, 201910003, 50000, '2020-07-16', 2, '', 1577953383, '2020-07-16 03:13:46', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007160015, 202010006, 50000, '2020-07-16', 2, '', 1577953383, '2020-07-16 03:14:30', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007160016, 201910017, 50000, '2020-07-16', 2, '', 1577953383, '2020-07-16 03:15:11', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007160017, 201810004, 50000, '2020-07-16', 2, '', 1577953383, '2020-07-16 03:16:07', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007160018, 201810012, 45000, '2020-07-16', 2, '', 1577953383, '2020-07-16 03:18:59', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007160019, 202010009, 50000, '2020-07-16', 2, '', 1577953383, '2020-07-16 03:19:52', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007160020, 201810009, 50000, '2020-07-16', 2, '', 1577953383, '2020-07-16 03:20:34', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007160021, 201810007, 50000, '2020-07-16', 2, '', 1577953383, '2020-07-16 03:21:12', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007160022, 201910005, 50000, '2020-07-16', 2, '', 1577953383, '2020-07-16 03:21:52', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007160023, 201910006, 40000, '2020-07-16', 2, '', 1577953383, '2020-07-16 03:22:30', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007160024, 202010013, 50000, '2020-07-16', 2, '', 1577953383, '2020-07-16 05:08:31', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007160025, 201910011, 50000, '2020-07-16', 2, '', 1577953383, '2020-07-16 05:10:27', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007160026, 201810011, 50000, '2020-07-16', 2, '', 1577953383, '2020-07-16 05:12:29', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007160027, 201910001, 50000, '2020-07-16', 2, '', 1577953383, '2020-07-16 05:14:12', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007160028, 201910009, 40000, '2020-07-16', 2, '', 1577953383, '2020-07-16 05:16:06', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007160029, 202010015, 30000, '2020-07-16', 2, '', 1577953383, '2020-07-16 05:17:49', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007160030, 202010007, 50000, '2020-07-16', 2, '', 1577953383, '2020-07-16 05:18:52', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007160031, 202010008, 50000, '2020-07-16', 2, '', 1577953383, '2020-07-16 05:19:58', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007160032, 201910015, 50000, '2020-07-16', 2, '', 1577953383, '2020-07-16 05:21:16', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007160033, 201910015, 20000, '2020-07-16', 2, 'MAJMUK', 1577953383, '2020-07-16 05:22:00', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007160034, 202010017, 25000, '2020-07-16', 2, '', 1577953383, '2020-07-16 05:28:10', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007230001, 201810010, 50000, '2020-07-23', 2, '', 1595507881, '2020-07-23 02:45:39', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007230002, 202010014, 50000, '2020-07-23', 2, '', 1595507881, '2020-07-23 02:46:39', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007230003, 202010018, 50000, '2020-07-23', 2, '', 1595507881, '2020-07-23 02:46:59', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007230004, 201910003, 50000, '2020-07-23', 2, '', 1595507881, '2020-07-23 02:47:41', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007230005, 201910002, 50000, '2020-07-23', 2, '', 1595507881, '2020-07-23 02:48:00', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007230006, 202010009, 50000, '2020-07-23', 2, '', 1595507881, '2020-07-23 02:48:12', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007230007, 202010015, 50000, '2020-07-23', 2, '', 1595507881, '2020-07-23 02:48:26', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007230008, 201810009, 50000, '2020-07-23', 2, '', 1595507881, '2020-07-23 02:48:46', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007230009, 201910015, 50000, '2020-07-23', 2, '', 1595507881, '2020-07-23 02:49:02', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007230010, 202010016, 25000, '2020-07-23', 2, '', 1595507881, '2020-07-23 02:49:21', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007230011, 202010001, 35000, '2020-07-23', 2, '', 1595507881, '2020-07-23 02:49:43', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007230012, 202010011, 50000, '2020-07-23', 2, '', 1595507881, '2020-07-23 02:50:01', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007230013, 201910010, 50000, '2020-07-23', 2, '', 1595507881, '2020-07-23 02:50:18', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007230014, 202010005, 50000, '2020-07-23', 2, '', 1595507881, '2020-07-23 02:50:32', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007230015, 201910005, 50000, '2020-07-23', 2, '', 1595507881, '2020-07-23 02:50:47', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007230016, 201810011, 50000, '2020-07-23', 2, '', 1595507881, '2020-07-23 02:51:10', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007230017, 201910006, 50000, '2020-07-23', 2, '', 1595507881, '2020-07-23 02:51:30', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007230018, 201810008, 50000, '2020-07-23', 2, '', 1595507881, '2020-07-23 02:51:45', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007230019, 201810012, 40000, '2020-07-23', 2, '', 1595507881, '2020-07-23 02:52:02', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007230020, 201810007, 50000, '2020-07-23', 2, '', 1595507881, '2020-07-23 02:52:41', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007230021, 201810004, 50000, '2020-07-23', 2, '', 1595507881, '2020-07-23 02:54:03', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007230022, 202010017, 50000, '2020-07-23', 2, '', 1595507881, '2020-07-23 02:55:32', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007230023, 201910017, 50000, '2020-07-23', 2, '', 1595507881, '2020-07-23 02:55:45', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007230024, 202010013, 50000, '2020-07-23', 2, '', 1595507881, '2020-07-23 02:56:02', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007230025, 202010006, 50000, '2020-07-23', 2, '', 1595507881, '2020-07-23 02:56:44', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007230026, 202010019, 50000, '2020-07-23', 2, '', 1595507881, '2020-07-23 03:03:13', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007230027, 201910001, 50000, '2020-07-23', 2, '', 1595507881, '2020-07-23 03:03:28', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007230028, 201610001, 50000, '2020-07-23', 2, '', 1595507881, '2020-07-23 03:03:40', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007230029, 201910011, 50000, '2020-07-23', 2, '', 1595507881, '2020-07-23 03:03:56', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007230030, 202010002, 50000, '2020-07-23', 2, '', 1595507881, '2020-07-23 03:04:07', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007230031, 202010003, 50000, '2020-07-23', 2, '', 1595507881, '2020-07-23 03:04:20', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007230032, 202010010, 50000, '2020-07-23', 2, '', 1595507881, '2020-07-23 03:04:40', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007230033, 201910009, 50000, '2020-07-23', 2, '', 1595507881, '2020-07-23 04:21:25', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007230034, 201910008, 50000, '2020-07-23', 2, '', 1595507881, '2020-07-23 04:21:51', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007270001, 202010004, 20000, '2020-07-27', 2, '', 1595507881, '2020-07-27 01:21:09', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007270002, 201810012, 20000, '2020-07-27', 2, '', 1595507881, '2020-07-27 01:21:23', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007270003, 202010006, 50000, '2020-07-27', 2, '', 1595507881, '2020-07-27 01:21:44', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007270004, 201810007, 15000, '2020-07-27', 2, '', 1595507881, '2020-07-27 01:22:16', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007270005, 201910008, 30000, '2020-07-27', 2, '', 1595507881, '2020-07-27 01:22:46', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007270006, 201910001, 35000, '2020-07-27', 2, '', 1595507881, '2020-07-27 01:23:02', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007270007, 201910003, 50000, '2020-07-27', 2, '', 1595507881, '2020-07-27 01:23:21', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007270008, 201810008, 30000, '2020-07-27', 2, '', 1595507881, '2020-07-27 01:23:48', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007270009, 201810010, 50000, '2020-07-27', 2, '', 1595507881, '2020-07-27 01:24:06', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007270010, 202010011, 20000, '2020-07-27', 2, '', 1595507881, '2020-07-27 01:24:26', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007270011, 201910010, 20000, '2020-07-27', 2, '', 1595507881, '2020-07-27 01:24:40', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007270012, 201910005, 50000, '2020-07-27', 2, '', 1595507881, '2020-07-27 01:25:00', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007270013, 202010001, 15000, '2020-07-27', 2, '', 1595507881, '2020-07-27 01:25:15', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007270014, 202010008, 15000, '2020-07-27', 2, '', 1595507881, '2020-07-27 01:25:32', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007270015, 202010009, 25000, '2020-07-27', 2, '', 1595507881, '2020-07-27 01:25:49', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007270016, 202010005, 20000, '2020-07-27', 2, '', 1595507881, '2020-07-27 01:26:09', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007270017, 202010018, 25000, '2020-07-27', 2, '', 1595507881, '2020-07-27 01:26:27', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007270018, 202010003, 15000, '2020-07-27', 2, '', 1595507881, '2020-07-27 01:26:41', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007270019, 201910006, 30000, '2020-07-27', 2, '', 1595507881, '2020-07-27 01:26:58', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007270020, 201910011, 35000, '2020-07-27', 2, '', 1595507881, '2020-07-27 02:24:27', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007270021, 202010019, 50000, '2020-07-27', 2, '', 1595507881, '2020-07-27 02:24:45', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007270022, 201810009, 20000, '2020-07-27', 2, '', 1595507881, '2020-07-27 02:25:00', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007270023, 202010002, 30000, '2020-07-27', 2, '', 1595507881, '2020-07-27 02:25:16', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007270024, 201910002, 20000, '2020-07-27', 2, '', 1595507881, '2020-07-27 02:25:34', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007270025, 201810004, 20000, '2020-07-27', 2, '', 1595507881, '2020-07-27 02:25:47', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007300001, 201910014, 50000, '2020-07-30', 2, '', 1595507881, '2020-07-30 01:32:13', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007300002, 202010007, 50000, '2020-07-30', 2, '', 1595507881, '2020-07-30 01:33:13', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007300003, 202010019, 50000, '2020-07-30', 2, '', 1595507881, '2020-07-30 01:33:55', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007300004, 201810007, 50000, '2020-07-30', 2, '', 1595507881, '2020-07-30 01:34:20', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007300005, 201910017, 50000, '2020-07-30', 2, '', 1595507881, '2020-07-30 01:34:56', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007300006, 202010002, 30000, '2020-07-30', 2, '', 1595507881, '2020-07-30 01:35:23', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007300007, 202010012, 50000, '2020-07-30', 2, '', 1595507881, '2020-07-30 01:35:46', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007300008, 201810004, 50000, '2020-07-30', 2, '', 1595507881, '2020-07-30 01:36:20', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007300009, 202010017, 50000, '2020-07-30', 2, '', 1595507881, '2020-07-30 01:36:35', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007300010, 202010005, 50000, '2020-07-30', 2, '', 1595507881, '2020-07-30 01:36:51', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007300011, 202010003, 25000, '2020-07-30', 2, '', 1595507881, '2020-07-30 01:37:28', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007300012, 202010015, 50000, '2020-07-30', 2, '', 1595507881, '2020-07-30 01:37:48', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007300013, 201810009, 50000, '2020-07-30', 2, '', 1595507881, '2020-07-30 01:38:13', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007300014, 201910015, 50000, '2020-07-30', 2, '', 1595507881, '2020-07-30 01:38:35', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007300015, 201810012, 35000, '2020-07-30', 2, '', 1595507881, '2020-07-30 01:38:54', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007300016, 201810008, 50000, '2020-07-30', 2, '', 1595507881, '2020-07-30 01:39:08', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007300017, 201910008, 50000, '2020-07-30', 2, '', 1595507881, '2020-07-30 01:39:37', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007300018, 201910002, 30000, '2020-07-30', 2, '', 1595507881, '2020-07-30 01:39:56', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007300019, 201910011, 50000, '2020-07-30', 2, '', 1595507881, '2020-07-30 01:40:12', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007300020, 201910010, 50000, '2020-07-30', 2, '', 1595507881, '2020-07-30 01:40:40', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007300021, 202010013, 25000, '2020-07-30', 2, '', 1595507881, '2020-07-30 01:40:55', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007300022, 201910009, 50000, '2020-07-30', 2, '', 1595507881, '2020-07-30 01:41:11', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007300023, 202010001, 50000, '2020-07-30', 2, '', 1595507881, '2020-07-30 01:41:30', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007300024, 202010016, 50000, '2020-07-30', 2, '', 1595507881, '2020-07-30 01:42:09', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007300025, 201610001, 50000, '2020-07-30', 2, '', 1595507881, '2020-07-30 01:42:34', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007300026, 202010008, 25000, '2020-07-30', 2, '', 1595507881, '2020-07-30 01:42:53', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007300027, 202010004, 30000, '2020-07-30', 2, '', 1595507881, '2020-07-30 01:43:12', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007300028, 201810011, 50000, '2020-07-30', 2, '', 1595507881, '2020-07-30 01:43:28', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007300029, 202010011, 30000, '2020-07-30', 2, '', 1595507881, '2020-07-30 01:43:51', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007300030, 202010009, 50000, '2020-07-30', 2, '', 1595507881, '2020-07-30 01:44:35', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007300031, 202010018, 50000, '2020-07-30', 2, '', 1595507881, '2020-07-30 01:44:51', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007300032, 201910001, 50000, '2020-07-30', 2, '', 1595507881, '2020-07-30 01:45:07', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007300033, 201910006, 40000, '2020-07-30', 2, '', 1595507881, '2020-07-30 01:45:22', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007300034, 202010014, 20000, '2020-07-30', 2, '', 1595507881, '2020-07-30 01:48:10', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007300035, 202010006, 50000, '2020-07-30', 2, '', 1595507881, '2020-07-30 01:48:29', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202007300036, 201810019, 50000, '2020-07-30', 2, '', 1595507881, '2020-07-30 01:50:07', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008040001, 202010010, 30000, '2020-08-04', 2, '', 1595507881, '2020-08-04 06:02:27', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008040002, 202010011, 50000, '2020-08-04', 2, '', 1595507881, '2020-08-04 06:02:49', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008040003, 202010017, 50000, '2020-08-04', 2, '', 1595507881, '2020-08-04 06:03:07', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008040004, 201810009, 40000, '2020-08-04', 2, '', 1595507881, '2020-08-04 06:03:23', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008040005, 201810008, 50000, '2020-08-04', 2, '', 1595507881, '2020-08-04 06:03:51', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008040006, 201910011, 50000, '2020-08-04', 2, '', 1595507881, '2020-08-04 06:04:04', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008040007, 201810004, 50000, '2020-08-04', 2, '', 1595507881, '2020-08-04 06:04:40', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008040008, 201910017, 50000, '2020-08-04', 2, '', 1595507881, '2020-08-04 06:04:52', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008040009, 201810010, 50000, '2020-08-04', 2, '', 1595507881, '2020-08-04 06:05:04', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008040010, 202010012, 50000, '2020-08-04', 2, '', 1595507881, '2020-08-04 06:05:16', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008040011, 202010015, 20000, '2020-08-04', 2, '', 1595507881, '2020-08-04 06:05:32', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008040012, 202010002, 30000, '2020-08-04', 2, '', 1595507881, '2020-08-04 06:05:48', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008040013, 202010003, 30000, '2020-08-04', 2, '', 1595507881, '2020-08-04 06:06:33', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008040014, 202010006, 50000, '2020-08-04', 2, '', 1595507881, '2020-08-04 06:06:50', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008040015, 201910009, 50000, '2020-08-04', 2, '', 1595507881, '2020-08-04 06:07:01', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008040016, 201810012, 40000, '2020-08-04', 2, '', 1595507881, '2020-08-04 06:07:15', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008040017, 202010016, 50000, '2020-08-04', 2, '', 1595507881, '2020-08-04 06:07:39', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008040018, 201910015, 50000, '2020-08-04', 2, '', 1595507881, '2020-08-04 06:07:52', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008040019, 202010014, 30000, '2020-08-04', 2, '', 1595507881, '2020-08-04 06:08:13', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008040020, 201910014, 50000, '2020-08-04', 2, '', 1595507881, '2020-08-04 06:08:27', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008040021, 202010007, 20000, '2020-08-04', 2, '', 1595507881, '2020-08-04 06:08:53', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008040022, 201810011, 50000, '2020-08-04', 2, '', 1595507881, '2020-08-04 06:09:05', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008040023, 202010009, 50000, '2020-08-04', 2, '', 1595507881, '2020-08-04 06:09:21', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008040024, 202010013, 50000, '2020-08-04', 2, '', 1595507881, '2020-08-04 06:10:45', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008040025, 201910003, 50000, '2020-08-04', 2, '', 1595507881, '2020-08-04 06:11:03', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008040026, 202010018, 50000, '2020-08-04', 2, '', 1595507881, '2020-08-04 06:11:17', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008040027, 201610001, 50000, '2020-08-04', 2, '', 1595507881, '2020-08-04 06:11:28', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008040028, 202010019, 50000, '2020-08-04', 2, '', 1595507881, '2020-08-04 06:12:43', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008040029, 201910002, 50000, '2020-08-04', 2, '', 1595507881, '2020-08-04 06:21:19', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008040030, 201910001, 15000, '2020-08-04', 2, '', 1595507881, '2020-08-04 06:23:36', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008040031, 201910006, 50000, '2020-08-04', 2, '', 1595507881, '2020-08-04 06:25:20', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008040032, 201910008, 50000, '2020-08-04', 2, 'kertas hilang', 1595507881, '2020-08-04 06:28:04', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008040033, 201910008, 30000, '2020-08-04', 2, 'peci ', 1595507881, '2020-08-04 06:28:23', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008040034, 201910010, 30000, '2020-08-04', 2, '', 1595507881, '2020-08-04 06:53:20', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008090001, 202010013, 25000, '2020-08-09', 2, '', 1595507881, '2020-08-09 01:29:54', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008090002, 202010012, 50000, '2020-08-09', 2, '', 1595507881, '2020-08-09 01:41:40', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008090003, 202010010, 50000, '2020-08-09', 2, '', 1595507881, '2020-08-09 01:51:05', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008090004, 202010011, 50000, '2020-08-09', 2, '', 1595507881, '2020-08-09 01:54:46', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008090005, 202010005, 20000, '2020-08-09', 2, '', 1595507881, '2020-08-09 02:06:42', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008090006, 202010002, 30000, '2020-08-09', 2, '', 1595507881, '2020-08-09 02:08:54', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008090007, 201910008, 50000, '2020-08-09', 2, '', 1595507881, '2020-08-09 02:12:24', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008090008, 201910002, 50000, '2020-08-09', 2, '', 1595507881, '2020-08-09 02:13:23', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008090009, 201910017, 50000, '2020-08-09', 2, '', 1595507881, '2020-08-09 02:13:54', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008090010, 202010006, 50000, '2020-08-09', 2, '', 1595507881, '2020-08-09 02:15:10', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008090011, 202010007, 30000, '2020-08-09', 2, '', 1595507881, '2020-08-09 02:15:30', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008090012, 202010016, 50000, '2020-08-09', 2, '', 1595507881, '2020-08-09 02:17:08', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008090013, 201910015, 30000, '2020-08-09', 2, '', 1595507881, '2020-08-09 02:17:52', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008090014, 201910010, 50000, '2020-08-09', 2, '', 1595507881, '2020-08-09 02:18:31', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008090015, 202010018, 25000, '2020-08-09', 2, '', 1595507881, '2020-08-09 02:19:04', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008090016, 202010009, 50000, '2020-08-09', 2, '', 1595507881, '2020-08-09 02:20:40', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008090017, 201910006, 50000, '2020-08-09', 2, '', 1595507881, '2020-08-09 02:21:03', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008090018, 202010003, 20000, '2020-08-09', 2, '', 1595507881, '2020-08-09 02:21:24', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008090019, 201810004, 50000, '2020-08-09', 2, '', 1595507881, '2020-08-09 02:22:17', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008090020, 201910011, 50000, '2020-08-09', 2, '', 1595507881, '2020-08-09 02:23:05', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008090021, 202010001, 20000, '2020-08-09', 2, '', 1595507881, '2020-08-09 02:23:20', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008090022, 201910009, 50000, '2020-08-09', 2, '', 1595507881, '2020-08-09 02:24:02', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008090023, 202010015, 50000, '2020-08-09', 2, '', 1595507881, '2020-08-09 02:24:31', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008090024, 201910014, 50000, '2020-08-09', 2, '', 1595507881, '2020-08-09 02:25:13', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008090025, 201810009, 40000, '2020-08-09', 2, '', 1595507881, '2020-08-09 02:25:42', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008090026, 201810011, 50000, '2020-08-09', 2, '', 1595507881, '2020-08-09 02:26:58', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008090027, 202010019, 50000, '2020-08-09', 2, '', 1595507881, '2020-08-09 02:27:49', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008090028, 202010008, 20000, '2020-08-09', 2, '', 1595507881, '2020-08-09 02:28:16', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008090029, 201810019, 50000, '2020-08-09', 2, '', 1595507881, '2020-08-09 02:28:40', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008090030, 201810019, 50000, '2020-08-09', 2, '', 1595507881, '2020-08-09 02:30:17', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008090031, 201810019, 50000, '2020-08-09', 2, '', 1595507881, '2020-08-09 02:30:23', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008090032, 201610001, 30000, '2020-08-09', 2, '', 1595507881, '2020-08-09 02:34:35', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008090033, 201810008, 50000, '2020-08-09', 2, '', 1595507881, '2020-08-09 02:38:09', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008100001, 202010003, 0, '2020-08-10', 2, '', 1595507881, '2020-08-10 07:40:29', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008100002, 202010001, 0, '2020-08-10', 2, '', 1595507881, '2020-08-10 07:41:39', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008100003, 201910005, 20000, '2020-08-10', 2, '', 1595507881, '2020-08-10 07:45:10', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008140001, 202010019, 50000, '2020-08-14', 2, '', 1595507881, '2020-08-14 02:25:38', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008140002, 201910017, 50000, '2020-08-14', 2, '', 1595507881, '2020-08-14 02:26:03', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008140003, 202010005, 50000, '2020-08-14', 2, '', 1595507881, '2020-08-14 02:26:19', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008140004, 201810012, 40000, '2020-08-14', 2, '', 1595507881, '2020-08-14 02:26:40', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008140005, 202010017, 50000, '2020-08-14', 2, '', 1595507881, '2020-08-14 02:30:06', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008140006, 201810004, 50000, '2020-08-14', 2, '', 1595507881, '2020-08-14 02:30:56', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008140007, 202010003, 35000, '2020-08-14', 2, '', 1595507881, '2020-08-14 02:31:15', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008140008, 202010015, 50000, '2020-08-14', 2, '', 1595507881, '2020-08-14 02:32:14', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008140009, 202010008, 30000, '2020-08-14', 2, '', 1595507881, '2020-08-14 02:32:33', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008140010, 202010009, 50000, '2020-08-14', 2, '', 1595507881, '2020-08-14 02:33:25', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008140011, 202010010, 30000, '2020-08-14', 2, '', 1595507881, '2020-08-14 02:33:41', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008140012, 202010011, 50000, '2020-08-14', 2, '', 1595507881, '2020-08-14 02:34:02', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008140013, 201810008, 50000, '2020-08-14', 2, '', 1595507881, '2020-08-14 02:34:22', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008140014, 202010007, 50000, '2020-08-14', 2, '', 1595507881, '2020-08-14 02:34:45', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008140015, 201910002, 50000, '2020-08-14', 2, '', 1595507881, '2020-08-14 02:35:02', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008140016, 201910014, 50000, '2020-08-14', 2, '', 1595507881, '2020-08-14 02:35:43', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008140017, 201910009, 50000, '2020-08-14', 2, '', 1595507881, '2020-08-14 02:36:23', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008140018, 201910006, 50000, '2020-08-14', 2, '', 1595507881, '2020-08-14 02:36:45', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008140019, 202010018, 50000, '2020-08-14', 2, '', 1595507881, '2020-08-14 02:37:16', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008140020, 201910015, 50000, '2020-08-14', 2, '', 1595507881, '2020-08-14 02:37:41', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008140021, 202010014, 50000, '2020-08-14', 2, '', 1595507881, '2020-08-14 02:38:08', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008140022, 202010004, 30000, '2020-08-14', 2, '', 1595507881, '2020-08-14 02:38:37', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008140023, 201810009, 50000, '2020-08-14', 2, '', 1595507881, '2020-08-14 02:40:07', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008140024, 202010002, 40000, '2020-08-14', 2, '', 1595507881, '2020-08-14 02:40:45', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008140025, 202010006, 50000, '2020-08-14', 2, '', 1595507881, '2020-08-14 02:41:13', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008140026, 201910011, 50000, '2020-08-14', 2, '', 1595507881, '2020-08-14 02:41:46', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008140027, 201910005, 50000, '2020-08-14', 2, '', 1595507881, '2020-08-14 02:42:27', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008140028, 201610001, 50000, '2020-08-14', 2, '', 1595507881, '2020-08-14 02:43:37', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008140029, 201910008, 50000, '2020-08-14', 2, '', 1595507881, '2020-08-14 02:44:01', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008140030, 201910010, 50000, '2020-08-14', 2, '', 1595507881, '2020-08-14 02:45:07', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008140031, 201810011, 50000, '2020-08-14', 2, '', 1595507881, '2020-08-14 02:47:01', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008140032, 202010016, 20000, '2020-08-14', 2, '', 1595507881, '2020-08-14 02:53:30', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008140033, 201910009, 0, '2020-08-14', 2, '', 1595507881, '2020-08-14 02:54:01', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008140034, 201810010, 50000, '2020-08-14', 2, '', 1595507881, '2020-08-14 02:54:24', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008140035, 202010015, 0, '2020-08-14', 2, 'nabung', 1595507881, '2020-08-14 02:57:19', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008140036, 201810007, 50000, '2020-08-14', 2, 'nabung', 1595507881, '2020-08-14 02:58:19', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008190001, 201810004, 50000, '2020-08-19', 2, '', 1595507881, '2020-08-19 05:34:35', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008190002, 202010019, 25000, '2020-08-19', 2, '', 1595507881, '2020-08-19 05:34:54', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008190003, 201810009, 50000, '2020-08-19', 2, '', 1595507881, '2020-08-19 05:35:19', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008190004, 202010010, 20000, '2020-08-19', 2, '', 1595507881, '2020-08-19 05:35:36', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008190005, 202010009, 50000, '2020-08-19', 2, '', 1595507881, '2020-08-19 05:36:15', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008190006, 201810011, 50000, '2020-08-19', 2, '', 1595507881, '2020-08-19 05:36:27', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008190007, 202010017, 50000, '2020-08-19', 2, '', 1595507881, '2020-08-19 05:36:40', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008190008, 202010018, 50000, '2020-08-19', 2, '', 1595507881, '2020-08-19 05:36:52', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008190009, 202010015, 50000, '2020-08-19', 2, '', 1595507881, '2020-08-19 05:37:03', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008190010, 201910009, 50000, '2020-08-19', 2, '', 1595507881, '2020-08-19 05:37:17', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008190011, 202010003, 50000, '2020-08-19', 2, '', 1595507881, '2020-08-19 05:37:30', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008190012, 202010001, 50000, '2020-08-19', 2, '', 1595507881, '2020-08-19 05:37:43', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008190013, 201810010, 50000, '2020-08-19', 2, '', 1595507881, '2020-08-19 05:37:57', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008190014, 201910017, 50000, '2020-08-19', 2, '', 1595507881, '2020-08-19 05:38:08', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008190015, 202010016, 50000, '2020-08-19', 2, '', 1595507881, '2020-08-19 05:38:53', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008190016, 202010008, 30000, '2020-08-19', 2, '', 1595507881, '2020-08-19 05:39:08', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008190017, 201810007, 50000, '2020-08-19', 2, '', 1595507881, '2020-08-19 05:39:28', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008190018, 201810012, 40000, '2020-08-19', 2, '', 1595507881, '2020-08-19 05:39:46', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008190019, 201910010, 50000, '2020-08-19', 2, '', 1595507881, '2020-08-19 05:40:48', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008190020, 202010005, 50000, '2020-08-19', 2, '', 1595507881, '2020-08-19 05:41:05', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008190021, 201910008, 50000, '2020-08-19', 2, '', 1595507881, '2020-08-19 05:41:18', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008190022, 202010004, 30000, '2020-08-19', 2, '', 1595507881, '2020-08-19 05:41:36', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008190023, 202010011, 50000, '2020-08-19', 2, '', 1595507881, '2020-08-19 05:42:24', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008190024, 201910002, 50000, '2020-08-19', 2, '', 1595507881, '2020-08-19 05:42:42', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008190025, 201610001, 50000, '2020-08-19', 2, '', 1595507881, '2020-08-19 05:42:57', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008190026, 201810008, 50000, '2020-08-19', 2, '', 1595507881, '2020-08-19 05:43:09', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008190027, 202010010, 0, '2020-08-19', 2, '', 1595507881, '2020-08-19 05:45:00', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008190028, 201910011, 50000, '2020-08-19', 2, '', 1595507881, '2020-08-19 05:45:55', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008190029, 202010007, 50000, '2020-08-19', 2, '', 1595507881, '2020-08-19 05:46:14', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008190030, 202010002, 30000, '2020-08-19', 2, '', 1595507881, '2020-08-19 05:57:11', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008190031, 201910015, 50000, '2020-08-19', 2, '', 1595507881, '2020-08-19 05:59:34', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008190032, 202010006, 30000, '2020-08-19', 2, '', 1595507881, '2020-08-19 06:25:55', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008240001, 201810010, 50000, '2020-08-24', 2, '', 1595507881, '2020-08-24 02:34:32', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008240002, 202010006, 50000, '2020-08-24', 2, '', 1595507881, '2020-08-24 02:35:39', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008240003, 201810004, 20000, '2020-08-24', 2, '', 1595507881, '2020-08-24 02:36:07', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008240004, 202010017, 50000, '2020-08-24', 2, '', 1595507881, '2020-08-24 02:36:21', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008240005, 202010009, 50000, '2020-08-24', 2, '', 1595507881, '2020-08-24 02:38:07', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008240006, 202010002, 30000, '2020-08-24', 2, '', 1595507881, '2020-08-24 02:39:02', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008240007, 202010005, 50000, '2020-08-24', 2, '', 1595507881, '2020-08-24 02:39:32', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008240008, 201910006, 50000, '2020-08-24', 2, '', 1595507881, '2020-08-24 02:39:52', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008240009, 201910010, 50000, '2020-08-24', 2, '', 1595507881, '2020-08-24 02:40:09', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008240010, 202010003, 50000, '2020-08-24', 2, '', 1595507881, '2020-08-24 02:40:35', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008240011, 201810011, 50000, '2020-08-24', 2, '', 1595507881, '2020-08-24 02:40:55', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008240012, 201910009, 50000, '2020-08-24', 2, '', 1595507881, '2020-08-24 02:41:16', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008240013, 201910011, 50000, '2020-08-24', 2, '', 1595507881, '2020-08-24 02:41:34', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008240014, 202010007, 50000, '2020-08-24', 2, '', 1595507881, '2020-08-24 02:41:54', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008240015, 201910008, 50000, '2020-08-24', 2, '', 1595507881, '2020-08-24 02:42:22', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008240016, 201910002, 50000, '2020-08-24', 2, '', 1595507881, '2020-08-24 02:42:55', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008240017, 201910017, 50000, '2020-08-24', 2, '', 1595507881, '2020-08-24 02:43:10', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008240018, 202010010, 35000, '2020-08-24', 2, '', 1595507881, '2020-08-24 02:43:32', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008240019, 202010001, 50000, '2020-08-24', 2, '', 1595507881, '2020-08-24 02:44:07', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008240020, 202010008, 35000, '2020-08-24', 2, '', 1595507881, '2020-08-24 02:44:30', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008240021, 202010014, 50000, '2020-08-24', 2, '', 1595507881, '2020-08-24 02:45:03', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008240022, 202010004, 40000, '2020-08-24', 2, '', 1595507881, '2020-08-24 02:45:30', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008240023, 202010018, 50000, '2020-08-24', 2, '', 1595507881, '2020-08-24 02:45:54', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008240024, 201610001, 50000, '2020-08-24', 2, '', 1595507881, '2020-08-24 02:46:14', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008240025, 201810007, 50000, '2020-08-24', 2, '', 1595507881, '2020-08-24 02:47:35', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008240026, 201810012, 40000, '2020-08-24', 2, '', 1595507881, '2020-08-24 02:48:12', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008240027, 201910005, 25000, '2020-08-24', 2, '', 1595507881, '2020-08-24 02:48:39', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008240028, 202010015, 50000, '2020-08-24', 2, '', 1595507881, '2020-08-24 02:49:02', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008240029, 201810009, 50000, '2020-08-24', 2, '', 1595507881, '2020-08-24 02:49:31', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008240030, 202010016, 50000, '2020-08-24', 2, '', 1595507881, '2020-08-24 02:49:52', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008240031, 202010019, 50000, '2020-08-24', 2, '', 1595507881, '2020-08-24 02:50:29', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008240032, 201810008, 50000, '2020-08-24', 2, '', 1595507881, '2020-08-24 02:50:55', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008280001, 201910010, 50000, '2020-08-28', 2, '', 1595507881, '2020-08-28 02:08:47', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008280002, 201910009, 50000, '2020-08-28', 2, '', 1595507881, '2020-08-28 02:09:43', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008280003, 201810007, 50000, '2020-08-28', 2, '', 1595507881, '2020-08-28 02:10:05', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008280004, 201910008, 50000, '2020-08-28', 2, '', 1595507881, '2020-08-28 02:10:25', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008280005, 202010009, 50000, '2020-08-28', 2, '', 1595507881, '2020-08-28 02:10:44', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008280006, 201810004, 30000, '2020-08-28', 2, '', 1595507881, '2020-08-28 02:11:24', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008280007, 201810009, 50000, '2020-08-28', 2, '', 1595507881, '2020-08-28 02:11:48', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008280008, 201910005, 50000, '2020-08-28', 2, '', 1595507881, '2020-08-28 02:12:16', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008280009, 201810012, 40000, '2020-08-28', 2, '', 1595507881, '2020-08-28 02:12:42', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008280010, 202010019, 50000, '2020-08-28', 2, '', 1595507881, '2020-08-28 02:13:08', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008280011, 202010001, 30000, '2020-08-28', 2, '', 1595507881, '2020-08-28 02:13:37', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008280012, 202010015, 50000, '2020-08-28', 2, '', 1595507881, '2020-08-28 02:14:01', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008280013, 201810011, 50000, '2020-08-28', 2, '', 1595507881, '2020-08-28 02:14:20', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008280014, 202010017, 25000, '2020-08-28', 2, '', 1595507881, '2020-08-28 02:14:51', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008280015, 202010018, 50000, '2020-08-28', 2, '', 1595507881, '2020-08-28 02:15:13', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008280016, 201810010, 50000, '2020-08-28', 2, '', 1595507881, '2020-08-28 02:15:30', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008280017, 202010006, 50000, '2020-08-28', 2, '', 1595507881, '2020-08-28 02:15:56', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008280018, 202010010, 35000, '2020-08-28', 2, '', 1595507881, '2020-08-28 02:16:54', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008280019, 202010008, 25000, '2020-08-28', 2, '', 1595507881, '2020-08-28 02:17:46', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008280020, 201910017, 50000, '2020-08-28', 2, '', 1595507881, '2020-08-28 02:18:10', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008280021, 202010005, 50000, '2020-08-28', 2, '', 1595507881, '2020-08-28 02:18:33', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008280022, 201910002, 50000, '2020-08-28', 2, '', 1595507881, '2020-08-28 02:18:51', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008280023, 201910006, 50000, '2020-08-28', 2, '', 1595507881, '2020-08-28 02:19:15', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008280024, 201910011, 50000, '2020-08-28', 2, '', 1595507881, '2020-08-28 02:19:29', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008280025, 202010007, 40000, '2020-08-28', 2, '', 1595507881, '2020-08-28 02:20:07', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008280026, 202010004, 20000, '2020-08-28', 2, '', 1595507881, '2020-08-28 02:20:29', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008280027, 202010016, 40000, '2020-08-28', 2, '', 1595507881, '2020-08-28 02:20:52', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008280028, 202010002, 30000, '2020-08-28', 2, '', 1595507881, '2020-08-28 02:21:17', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008280029, 202010003, 30000, '2020-08-28', 2, '', 1595507881, '2020-08-28 02:21:34', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008280030, 201610001, 20000, '2020-08-28', 2, '', 1595507881, '2020-08-28 02:21:54', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008280031, 201810016, 50000, '2020-08-28', 2, '', 1595507881, '2020-08-28 02:22:27', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008280032, 201810008, 50000, '2020-08-28', 2, '', 1595507881, '2020-08-28 02:32:06', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008280033, 202010002, 0, '2020-08-28', 2, '', 1595507881, '2020-08-28 02:34:02', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202008280034, 201910015, 50000, '2020-08-28', 2, '', 1595507881, '2020-08-28 02:36:24', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202009030001, 201910011, 50000, '2020-09-03', 2, '', 1595507881, '2020-09-03 11:25:15', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202009030002, 201810004, 50000, '2020-09-03', 2, '', 1595507881, '2020-09-03 11:25:57', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202009030003, 202010017, 50000, '2020-09-03', 2, '', 1595507881, '2020-09-03 11:26:19', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202009030004, 201810011, 50000, '2020-09-03', 2, '', 1595507881, '2020-09-03 11:26:40', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202009030005, 202010009, 50000, '2020-09-03', 2, '', 1595507881, '2020-09-03 11:26:55', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202009030006, 201910015, 50000, '2020-09-03', 2, '', 1595507881, '2020-09-03 11:28:31', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202009030007, 201810007, 50000, '2020-09-03', 2, '', 1595507881, '2020-09-03 11:28:49', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202009030008, 201810008, 50000, '2020-09-03', 2, '', 1595507881, '2020-09-03 11:29:02', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202009030009, 202010015, 50000, '2020-09-03', 2, '', 1595507881, '2020-09-03 11:29:46', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202009030010, 201910009, 50000, '2020-09-03', 2, '', 1595507881, '2020-09-03 11:30:01', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202009030011, 201910010, 50000, '2020-09-03', 2, '', 1595507881, '2020-09-03 11:30:21', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202009030012, 201910006, 50000, '2020-09-03', 2, '', 1595507881, '2020-09-03 11:30:36', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202009030013, 202010006, 50000, '2020-09-03', 2, '', 1595507881, '2020-09-03 11:30:59', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202009030014, 202010016, 50000, '2020-09-03', 2, '', 1595507881, '2020-09-03 11:31:14', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202009030015, 202010019, 50000, '2020-09-03', 2, '', 1595507881, '2020-09-03 11:31:36', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202009030016, 202010012, 50000, '2020-09-03', 2, '', 1595507881, '2020-09-03 11:32:02', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202009030017, 201810012, 40000, '2020-09-03', 2, '', 1595507881, '2020-09-03 11:32:21', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202009030018, 202010001, 35000, '2020-09-03', 2, '', 1595507881, '2020-09-03 11:32:44', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202009030019, 202010002, 30000, '2020-09-03', 2, '', 1595507881, '2020-09-03 11:33:06', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202009030020, 202010018, 40000, '2020-09-03', 2, '', 1595507881, '2020-09-03 11:33:25', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202009030021, 202010010, 30000, '2020-09-03', 2, '', 1595507881, '2020-09-03 11:33:45', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202009030022, 202010008, 25000, '2020-09-03', 2, '', 1595507881, '2020-09-03 11:34:16', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202009030023, 202010004, 40000, '2020-09-03', 2, '', 1595507881, '2020-09-03 11:34:45', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202009030024, 201910005, 30000, '2020-09-03', 2, '', 1595507881, '2020-09-03 11:35:06', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202009030025, 201810016, 50000, '2020-09-03', 2, '', 1595507881, '2020-09-03 11:35:49', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202009030026, 201910002, 25000, '2020-09-03', 2, '', 1595507881, '2020-09-03 11:36:21', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202009030027, 201910017, 45000, '2020-09-03', 2, '', 1595507881, '2020-09-03 11:36:53', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202009030028, 202010014, 40000, '2020-09-03', 2, '', 1595507881, '2020-09-03 11:37:24', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202009030029, 202010003, 30000, '2020-09-03', 2, '', 1595507881, '2020-09-03 11:37:43', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202009030030, 201610001, 50000, '2020-09-03', 2, '', 1595507881, '2020-09-03 11:45:54', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202009030031, 202010020, 50000, '2020-09-03', 2, '', 1595507881, '2020-09-03 12:13:44', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202009030032, 201810002, 0, '2020-09-03', 2, '', 1595507881, '2020-09-03 12:15:53', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202009030033, 201810013, 50000, '2020-09-03', 2, '', 1595507881, '2020-09-03 04:10:06', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202009030034, 201910012, 30000, '2020-09-03', 2, '', 1595507881, '2020-09-03 04:10:39', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202009030035, 201810003, 50000, '2020-09-03', 2, '', 1595507881, '2020-09-03 04:10:59', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202009030036, 201810010, 50000, '2020-09-03', 2, '', 1595507881, '2020-09-03 04:11:31', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202106100001, 201410001, 10000, '2021-06-10', 4, '', 1623296882, '2021-06-10 05:54:19', 0, '0000-00-00 00:00:00');
INSERT INTO `trans` VALUES (202106100002, 201410001, 5000, '2021-06-10', 2, '', 1623296882, '2021-06-10 05:56:43', 0, '0000-00-00 00:00:00');

SET FOREIGN_KEY_CHECKS = 1;
