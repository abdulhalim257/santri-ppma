<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>PSB Santri PPMA </title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.form.io/formiojs/formio.full.min.css">
    <script src="https://cdn.form.io/formiojs/formio.full.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="plugins/iCheck/square/blue.css">
    
  <script src="base64.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

</head>
<style>
    .login-box,
    .register-box {
        width: 70%;
        margin: 7% auto;
    }
</style>
<?php require('sadmin/konek.php');
$hari = date('Y-m-d');
// echo $hari;
if (isset($_GET['s'])) {
    $form = mysqli_query($kon, "SELECT * FROM survey_set s left join questions q on s.id_survey_set=q.id_survey_set where s.kategori_survey_set='PSB' and s.slug='$_GET[s]' and s.status_survey_set='Y' and (s.start_date <= '$hari' and s.end_date >='$hari') order by s.id_survey_set DESC limit 1");
    $rowform = mysqli_fetch_array($form);
    $jform = mysqli_num_rows($form);
} else {
    $form = mysqli_query($kon, "SELECT * FROM survey_set s left join questions q on s.id_survey_set=q.id_survey_set where s.kategori_survey_set='PSB' and s.status_survey_set='Y' and (s.start_date <= '$hari' and s.end_date >='$hari') order by s.id_survey_set DESC limit 1");
    $rowform = mysqli_fetch_array($form);
    $jform = mysqli_num_rows($form);
}
if ($jform < 1) {
    // echo "<script type='text/javascript'>alert(\"Link salah!\");window.location=\"index.php\";</script>";
} else {

    $valquestion = $rowform['question'];
    $valanswer = "{\"data\": {}, \"metadata\": {}}";
}
?>

<body class="hold-transition login-page">
    <div class="login-box">
        <div class="login-logo">
            <a href="index.php"><b><?php echo ucwords("$rowform[title]"); ?> </b></a>
        </div>
        <!-- /.login-logo -->
        <div class="login-box-body">
            <p class="login-box-msg"><?php echo "$rowform[description]"; ?></p>
            <p><i>Tanggal : <b><?php echo tgl_indo($rowform['start_date']); ?></b> s/d <b><?php echo tgl_indo($rowform['end_date']); ?></b></i></p>

            <form id="idForm" method="POST" enctype="multipart/form-data">
                <div class="form-group has-feedback">
                    <input type="email" class="form-control" placeholder="Masukkan Email" name="email" id="email" required>
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <textarea name="components" id="components" cols="30" rows="10" hidden><?php echo $valquestion; ?></textarea>
                <textarea name="question" id="question" cols="30" rows="10" hidden><?php echo $valanswer; ?></textarea>
                <!-- <input type="hiddens" class="form-control" name="components" id="components" value=""> -->
                <input type="hidden" class="form-control" name="id_survey_set" id="id_survey_set" value="<?php echo $rowform['id_survey_set']; ?>">
                <div id='builder'></div>
            </form>
        </div>
        <!-- /.login-box-body -->
    </div>
    <!-- /.login-box -->

    <script>
        var jsonElement = document.getElementById('components');
        var jsonElementquestion = document.getElementById('question');

        /**
         * CARA 1
         */
        var form_fields = jsonElement.value;
        var qform_fields = jsonElementquestion.value;
        const obj = JSON.parse(form_fields);
        const qobj = JSON.parse(qform_fields);

        Formio.createForm(document.getElementById('builder'), obj)
            .then((form) => {
                form.submission = qobj;
                form.on('change', function(changed) {
                    jsonElementquestion.value = JSON.stringify(form.submission);
                });
                form.on('submit', function(submission) {
                    var question = jsonElementquestion.value;
                    var id_survey_set = $('#id_survey_set').val();
                    var email = $('#email').val();
                    $.ajax({
                        type: "POST",
                        url: "psb_add.php",
                        // dataType: 'json',
                        data: "question=" + question + "&id_survey_set=" + id_survey_set + "&email=" + email,
                        success: function(response) {
                            $("#question").val("");
                            $("#id_survey_set").val("");
                            $("#email").val("");
                            window.location = response;
                            // alert(response);
                        },
                    });
                    // return false;    
                    console.log('Berhasil!', submission);
                    // alert(question + id_survey_set + email);
                });
            });

        // Formio.builder(document.getElementById('builder'), obj, {})

        //     .then((form) => {
        //         form.on('change', function() {
        //             jsonElement.value = JSON.stringify(form.schema);
        //         })
        //     });

        /**
         * CARA 2
         */

        /* var builder = new Formio.FormBuilder(document.getElementById("builder"), {
            display: 'form',
            components: [],
        });

        var onBuild = function(build) {
            jsonElement.value = '';
            jsonElement.value = JSON.stringify(builder.instance.schema);
        };

        var onReady = function() {
            builder.instance.on('change', onBuild);
        };

        builder.instance.ready.then(onReady); */

        // $("#submit").click(function(e) {
        //     e.preventDefault();

        //     var form_fields = $("#components").val();

        //     console.log(form_fields);

        //     $.ajax({
        //         type: 'POST',
        //         url: '/',
        //         data: "form_field = " + form_fields,
        //         success: function() {
        //             alert('success')
        //         },
        //         error: function() {
        //             alert('fail');
        //         }
        //     });
        // });
    </script>
    <!-- jQuery 3 -->
    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- iCheck -->
    <script src="plugins/iCheck/icheck.min.js"></script>
    <!-- <script>
        $(function() {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' /* optional */
            });
        });
    </script> -->
</body>

</html>