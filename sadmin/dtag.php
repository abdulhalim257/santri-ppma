<?php require_once('atas.php');
if (isset($_GET["d"])) {
    if ($_GET['j'] != "all") {
        $deltag = "and santri.jkel='$_GET[j]'";
    } else {
        $deltag = "";
    }
    mysqli_query($kon, "delete tagihan from tagihan left join santri on tagihan.nis=santri.nis where tagihan.waktu_tagihan='$_GET[d]' $deltag");
    echo "<script type='text/javascript'>alert(\"Berhasil!\");window.location=\"dtag.php\";</script>";
}
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Dashboard
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Tagihan</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"><a href='ttag.php'><i class="fa fa-cc-mastercard"></i></a> Daftar Tagihan</h3>
                    <?php if ($_SESSION['level'] == 'sadmin') { ?>
                        <form style='margin-right:5px; margin-top:0px' class='pull-right' action='' method='GET'>
                            <select class='select2' name='j' style='padding:4px'>
                                <?php
                                echo "<option value='all'>- Semua -</option>";
                                echo "<option value='L' ";
                                if (isset($_GET['j']) and $_GET['j'] == "L") {
                                    echo "selected";
                                }
                                echo ">Laki-laki</option>";
                                echo "<option value='P'";
                                if (isset($_GET['j']) and $_GET['j'] == "P") {
                                    echo "selected";
                                }
                                echo ">Perempuan</option>";
                                ?>
                            </select>
                            <input type="submit" style='margin-top:0px' class='btn btn-success btn-sm' value='Lihat'>
                        </form>
                    <?php } ?>
                </div>
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Bulan</th>
                                <th>JKel</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            if ($_SESSION['level'] == 'sadmin') {
                                if ((isset($_GET['j']) and $_GET['j'] == "all") or empty($_GET['j'])) {
                                    $s = mysqli_query($kon, "select * from tagihan left join santri on tagihan.nis=santri.nis group by tagihan.waktu_tagihan desc");
                                    $jkel = "all";
                                } else {
                                    $s = mysqli_query($kon, "select * from tagihan left join santri on tagihan.nis=santri.nis where santri.jkel='$_GET[j]' group by tagihan.waktu_tagihan desc");
                                    $jkel = $_GET['j'];
                                }
                            } else {
                                $s = mysqli_query($kon, "select * from tagihan left join santri on tagihan.nis=santri.nis $_SESSION[where] group by tagihan.waktu_tagihan desc");
                                $jkel = $_SESSION['jeniskelamin'];
                            }
                            while ($f = mysqli_fetch_array($s)) {
                                $td = substr($f['waktu_tagihan'], 5, 2);
                                switch ($td) {

                                    case '01':
                                        $tdd = "Januari";
                                        break;

                                    case '02':
                                        $tdd = "Februari";
                                        break;

                                    case '03':
                                        $tdd = "Maret";
                                        break;

                                    case '04':
                                        $tdd = "April";
                                        break;

                                    case '05':
                                        $tdd = "Mei";
                                        break;

                                    case '06':
                                        $tdd = "Juni";
                                        break;

                                    case '07':
                                        $tdd = "Juli";
                                        break;

                                    case '08':
                                        $tdd = "Agustus";
                                        break;

                                    case '09':
                                        $tdd = "September";
                                        break;

                                    case '10':
                                        $tdd = "Oktober";
                                        break;

                                    case '11':
                                        $tdd = "November";
                                        break;

                                    case '12':
                                        $tdd = "Desember";
                                        break;

                                    default:
                                        # code...
                                        break;
                                }
                                echo "
                                    <tr>
                                        <td>$no</td>
                                        <td>$tdd " . substr($f['waktu_tagihan'], 0, 4) . "</td>
                                        <td>$jkel</td>
                                        <td>
                                        <a class='btn btn-info btn-xs' href='etag.php?e=$f[waktu_tagihan]&j=$jkel'>
                                        <i class='fa fa-edit'></i></a>
                                        <a class='btn btn-danger btn-xs' href='?d=$f[waktu_tagihan]&j=$jkel' onclick=\"return confirm('Yakin hapus?')\">
                                        <i class='fa fa-remove'></i></a>
                                        <a class='btn btn-primary btn-xs' href='ptag.php?e=$f[waktu_tagihan]&j=$jkel' target=_blank()>
                                        <i class='fa fa-print'></i></a>
                                        </td>
                                    </tr>
                                    ";
                                $no++;
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </section>
        <!-- /.Left col -->
    </div>
    <!-- /.row (main row) -->

</section>
<?php require_once('bawah.php'); ?>