<?php require('konek.php');
$so = mysqli_query($kon, "select * from trans left join jtrans on trans.id_jtrans=jtrans.id_jtrans left join admin on trans.create_by=admin.id_admin left join santri on trans.nis=santri.nis where trans.id_trans='$_GET[p]'");
$sso = mysqli_fetch_array($so);
// echo $sso['id_trans'];

$d = mysqli_query($kon, "select *, sum(jumlah_trans) as jts from trans where nis='$sso[nis]' and id_trans<'$sso[id_trans]'");
$dd = mysqli_fetch_array($d);

if ($sso['edit_at'] == "0000-00-00 00:00:00") {
    $s = mysqli_query($kon, "select * from tabungan where nis='$sso[nis]' and create_at <= '$sso[create_at]' order by create_at desc limit 1 ");
    $ss = mysqli_fetch_array($s);

    // $sa=mysqli_query($kon,"select *,sum(jumlah_tab) as jtab from tabungan where nis='$ss[nis]' and create_at <= '$ss[create_at]'");
    $sa = mysqli_query($kon, "select *,sum(jumlah_tab) as jtab from tabungan where nis='$ss[nis]' and create_at <= '$ss[create_at]'");
    $ssa = mysqli_fetch_array($sa);
} else {
    $s = mysqli_query($kon, "select * from tabungan left join admin on tabungan.create_by=admin.id_admin where nis='$sso[nis]' and (edit_at <= '$sso[edit_at]' or create_at <= '$sso[edit_at]')");
    $ss = mysqli_fetch_array($s);
    $sa = mysqli_query($kon, "select *,sum(jumlah_tab) as jtab from tabungan where nis='$ss[nis]' and (edit_at <= '$sso[edit_at]' or create_at <= '$sso[edit_at]') ");
    $ssa = mysqli_fetch_array($sa);
}

$jt = $ssa['jtab'] - $dd['jts'];
$saldo = $ssa['jtab'] - $dd['jts'] - $sso['jumlah_trans'];

// <body onload=window.print()>
echo "
    <body>
    <table>
        <tr>
            <td style='border-bottom:1px solid; text-align:center;' colspan=3>
                <h4 style='margin-block-start:0px;margin-block-end:0px;'>Pondok Pesantren Manba'ul Anwar</h4>
                <h5 style='margin-block-start:0px;margin-block-end:0px;'>Jl. Dieng Km. 05 Krasak Mojotengah</h5>
            </td>
        </tr>
        <tr>
            <td>ID Transaksi</td>
            <td>:</td>
            <td>$sso[id_trans]</td>
        </tr>
        <tr>
            <td>Nama</td>
            <td>:</td>
            <td>$sso[nama_lengkap]</td>
        </tr>
        <tr>
            <td style='border-bottom:1px solid'>Tanggal</td>
            <td style='border-bottom:1px solid'>:</td>
            <td style='border-bottom:1px solid'>" . tgl_indo($ss['tgl_tab']) . "</td>
        </tr>
        <tr>
            <td>Saldo Awal</td>
            <td> : </td>
            <td> Rp " . number_format($jt, 2, ',', '.') . "</td>
        </tr>
        <tr>
            <td>Jumlah Ambil</td>
            <td> : </td>
            <td> Rp " . number_format($sso['jumlah_trans'], 2, ',', '.') . "</td>
        </tr>
        <tr>
            <td style='border-bottom:1px solid'>Saldo Akhir</td>
            <td style='border-bottom:1px solid'> : </td>
            <td style='border-bottom:1px solid'> Rp " . number_format($saldo, 2, ',', '.') . "</td>
        </tr>
        <tr>
            <td>Jenis Keperluan</td>
            <td> : </td>
            <td> " . $sso['ket_jtrans'] . "</td>
        </tr>
        <tr>
            <td style='border-bottom:1px solid'>Keterangan</td>
            <td style='border-bottom:1px solid'> : </td>
            <td style='border-bottom:1px solid'> " . $sso['ket_trans'] . "</td>
        </tr>
        <tr>
            <td style='height:50px'></td>
            <td></td>
            <td align=center>Wonosobo, " . tgl_indo(date('Y-m-d')) . "<br>Admin</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td align=center>$sso[username]</td>
        </tr>
    </table>
    <h6>Nb. * Nota ini harap disimpan, dan jangan sampai hilang!.</h6>
    ";
