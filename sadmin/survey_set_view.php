<?php require_once('atas.php');

if (isset($_GET["det"])) {
    $qsurvey_set = mysqli_query($kon, "select *,a.date_created as tanggal_input from answers a right join survey_set s on s.id_survey_set=a.id_survey_set where a.id_answer='$_GET[det]'");
    $dqsurvey_set = mysqli_fetch_array($qsurvey_set);
}
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Dashboard
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Detail Answers</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <!-- Left col -->
        <section class="col-lg-3 connectedSortable">
            <!-- About Me Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Tentang Answers</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <strong><i class="fa fa-pencil margin-r-5"></i> Judul</strong>
                    <p class="text-muted">
                        <?php echo "$dqsurvey_set[title]"; ?>
                    </p>

                    <strong><i class="fa fa-book margin-r-5"></i> Deskripsi</strong>
                    <p class="text-muted">
                        <?php echo "$dqsurvey_set[description]"; ?>
                    </p>

                    <strong><i class="fa fa-calendar margin-r-5"></i> Waktu Survey</strong>
                    <p>
                        <?php echo "" . tgl_indo($dqsurvey_set['start_date']) . " sd " . tgl_indo($dqsurvey_set['end_date']) . ""; ?>
                    </p>

                    <strong><i class="fa fa-user margin-r-5"></i> Responden</strong>
                    <p>
                        <?php echo "$dqsurvey_set[respon]"; ?>
                    </p>

                    <strong><i class="fa fa-calendar-plus-o margin-r-5"></i> Waktu Input</strong>
                    <p>
                        <?php echo "" . tgl_indo(substr($dqsurvey_set['tanggal_input'], 0, 10)) . " " . substr($dqsurvey_set['tanggal_input'], 11, 20) . ""; ?></p>
                </div>
                <!-- /.box-body -->
            </div>
        </section>
        <section class="col-lg-9 connectedSortable">
            <!-- /.box -->
            <div class="box">
                <div class="box-body">
                    <?php
                    $piechart = "SELECT * FROM questions as an where an.id_survey_set='$dqsurvey_set[id_survey_set]'";
                    $querypiechart = mysqli_query($kon, $piechart);
                    $nrowpiechart =  mysqli_num_rows($querypiechart);
                    $rowpiechart =  mysqli_fetch_assoc($querypiechart);

                    $json = json_decode($rowpiechart['question'], true);

                    for ($i = 0; $i < 500; $i++) {
                        if (!empty($json["components"][$i]["type"]) && $json["components"][$i]["key"] != "submit") {
                            echo "<b>Pertanyaan : </b>" . $json["components"][$i]["label"] . "<br>";

                            $jsondanswers = json_decode($dqsurvey_set['answer'], true);
                            $key = $json["components"][$i]["key"];
                            if($json["components"][$i]["type"]=="radio"){
                                for ($z = 0; $z < 500; $z++) {
                                    if (!empty($json["components"][$i]["values"][$z]["value"])) {
                                        if ($json["components"][$i]["values"][$z]["value"]==$jsondanswers["data"][$key]) {
                                            echo "" . $json["components"][$i]["values"][$z]["label"] . "<br>";
                                        }
                                    }
                                }
                            }elseif ($json["components"][$i]["type"] != "file") {
                                echo "" . $jsondanswers["data"][$key] . "<br>";
                            } else {
                                // deteski posisi base64,
                                $deteksi = strpos($jsondanswers["data"][$key][0]["url"], "base64,");
                                $hasildeteksi = $deteksi + 7;
                                $img1 = substr($jsondanswers["data"][$key][0]["url"], 0, $hasildeteksi);
                                $img = str_replace($img1, '', $jsondanswers["data"][$key][0]["url"]);
                                $img = str_replace(' ', '+', $img);
                                $data = base64_decode($img);
                                $file = "images/".$jsondanswers["data"][$key][0]["name"];
                                $success = file_put_contents($file, $data);

                                $ext = pathinfo("images/".$jsondanswers["data"][$key][0]["name"], PATHINFO_EXTENSION);
                                if ($ext == 'gif' || $ext == 'png' || $ext == 'jpg'|| $ext == 'jpeg') {
                                    echo "<img src='images/".$jsondanswers["data"][$key][0]["name"]."' width='200px'>";
                                    // $_SERVER['SERVER_NAME']
                                }else{
                                    echo"<a href='images/".$jsondanswers["data"][$key][0]["name"]."'> Download disini </a>";
                                }
                            }
                        }
                    }
                    ?>

                </div>
                <!-- /.box-body -->
            </div>
        </section>
        <!-- /.Left col -->
    </div>
    <!-- /.row (main row) -->

</section>

<?php require_once('bawah.php'); ?>