<?php require_once('atas.php');
$s = mysqli_query($kon, "select * from admin where id_admin='$_SESSION[id_admin]'");
$f = mysqli_fetch_array($s);
if (isset($_POST["save"])) {
    $passwordl = md5($_POST['passwordl']);
    $passwordlm = $_POST['passwordlm'];
    if ($passwordlm == $passwordl) {
        echo "<script type='text/javascript'>window.location=\"tutup1.php\";</script>";
    } else {
        echo "<script type='text/javascript'>alert(\"Password tidak sama!\");history.back();</script>";
    }
}

?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Dashboard
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Tutup Buku</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <!-- Left col -->
        <section class="col-lg-7 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-user-plus"></i> Konfirmasi</h3>
                </div>
                <div class="box-body">
                    <p>Sisa saldo pada periode sebelumnya otomatis akan masuk kedalam data tabungan. Akun Super Admin, Admin, dan Santri tidak akan menerima perubahan apapun.</p>
                    <form action="" method="POST" enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="form-group">
                                <label>Password</label> *
                                <input type="hidden" name='passwordlm' class="form-control" placeholder="Masukkan Password" value="<?php echo $f['password']; ?>">
                                <input type="text" name='passwordl' class="form-control" placeholder="Masukkan Password" required>
                            </div>
                        </div>
                        <?php echo "
                    <div class='box-footer'>
                        <input type='submit' name='save' class='btn btn-primary' value='Save' onclick=\"return confirm('Yakin Tutup Buku?')\">
                        <input type='Reset' class='btn btn-danger' value='Reset'>
                    </div>
                    ";
                        ?>
                    </form>
                    <!-- /.box-body -->
                </div>
            </div>
        </section>
        <!-- /.Left col -->
    </div>
    <!-- /.row (main row) -->

</section>
<?php require_once('bawah.php'); ?>