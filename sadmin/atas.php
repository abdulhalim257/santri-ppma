<?php require('konek.php');
session_start();
if (empty($_SESSION['level'])) {
  echo "<script type='text/javascript'>alert(\"Login dulu!\");window.location=\"../index.php\";</script>";
}
?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin PPMA | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="../bower_components/select2/dist/css/select2.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="../bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="../plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="../bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="../plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="../bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="../bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="../bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <script src="../js/jquery.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>
  <script src="../base64.js"></script>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="../../https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="../../https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <link rel='stylesheet' href='https://cdn.form.io/formiojs/formio.full.min.css'>

  <link rel='stylesheet' href='https://cdn.form.io/formiojs/formio.full.min.css'>
  <script src='https://cdn.form.io/formiojs/formio.full.min.js'></script>
</head>

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <header class="main-header">
      <!-- Logo -->
      <a href="index.php" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <!-- <span class="logo-mini"><b>A</b>LT</span> -->
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Admin</b> PPMA</span>
      </a>
      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
          <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <!-- Notifications: style can be found in dropdown.less -->
            <li class="dropdown notifications-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-qrcode"></i>
              </a>
              <ul class="dropdown-menu">
                <li>
                  <!-- inner menu: contains the actual data -->
                  <ul class="menu">
                    <li>
                      <a href="c.php?id=tab">
                        <i class="fa fa-money text-aqua"></i> Tambah Tabungan
                      </a>
                    </li>
                    <li>
                      <a href="c.php?id=trans">
                        <i class="fa fa-credit-card text-yellow"></i> Tambah Transaksi
                      </a>
                    </li>
                    <!-- <li>
                    <a href="#">
                      <i class="fa fa-users text-red"></i> 5 new members joined
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                    </a>
                  </li> -->
                    <!-- <li>
                    <a href="#">
                      <i class="fa fa-user text-red"></i> You changed your username
                    </a>
                  </li> -->
                  </ul>
                </li>
                <!-- <li class="footer"><a href="#">View all</a></li> -->
              </ul>
            </li>
            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php
                                                                          echo "<img src='../files/$_SESSION[foto]' class='user-image' alt='User Image'>";
                                                                          ?>
                <span class="hidden-xs"><?php echo "$_SESSION[username]"; ?></span>
              </a>
              <ul class="dropdown-menu">
                <!-- User image -->
                <li class="user-header">
                  <?php
                  echo "<img src='../files/$_SESSION[foto]' class='img-circle' alt='User Image'>";
                  ?>

                  <p>
                    <?php echo "$_SESSION[username]"; ?>
                    <small><?php echo "$_SESSION[nickname]"; ?></small>
                  </p>
                </li>
                <!-- Menu Body -->
                <!-- <li class="user-body">
                <div class="row">
                  <div class="col-xs-4 text-center">
                    <a href="#">Followers</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Sales</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Friends</a>
                  </div>
                </div>
              </li> -->
                <!-- Menu Footer-->
                <li class="user-footer">
                  <div class="pull-left">
                    <?php
                    echo "<a href='eadmin.php?e=$_SESSION[id_admin]' class='btn btn-default btn-flat'>Profile</a>";
                    ?>
                  </div>
                  <div class="pull-right">
                    <a href="logout.php" class="btn btn-default btn-flat">Sign out</a>
                  </div>
                </li>
              </ul>
            </li>
            <!-- Control Sidebar Toggle Button -->
            <!-- <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li> -->
          </ul>
        </div>
      </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
          <div class="pull-left image">
            <?php
            echo "<img src='../files/$_SESSION[foto]' class='img-circle' alt='User Image' style='width:100%;max-width:45px;height:45px'>";
            ?>
          </div>
          <div class="pull-left info">
            <p><?php echo "$_SESSION[username]"; ?></p>
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
          </div>
        </div>
        <!-- search form -->
        <!-- <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form> -->
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
          <li class="header">MAIN NAVIGATION</li>
          <li class="">
            <a href="index.php">
              <i class="fa fa-dashboard"></i> <span>Dashboard</span>
              <span class="pull-right-container">
                <small class="label pull-right bg-green">new</small>
              </span>
            </a>
          </li>
          <li class="treeview">
            <a href="#">
              <i class="fa fa-money"></i>
              <span>Tabungan</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="ttab.php"><i class="fa fa-user-plus"></i> Tambah Tabungan</a></li>
              <li><a href="dtab.php"><i class="fa fa-table"></i> Daftar Tabungan</a></li>
            </ul>
          </li>
          <li class="treeview">
            <a href="#">
              <i class="fa fa-credit-card"></i>
              <span>Transaksi</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="ttrans.php"><i class="fa fa-user-plus"></i> Tambah Transaksi</a></li>
              <li><a href="dtrans.php"><i class="fa fa-table"></i> Daftar Transaksi</a></li>
            </ul>
          </li>
          <li class="treeview">
            <a href="#">
              <i class="fa fa-cc-mastercard"></i>
              <span>Tagihan</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="ttag.php"><i class="fa fa-user-plus"></i> Tambah Tagihan</a></li>
              <li><a href="dtag.php?j=all"><i class="fa fa-table"></i>Daftar Tagihan</a></li>
            </ul>
          </li>
          <li>
            <a href="rekap.php">
              <i class="fa fa-book"></i> <span>Rekap</span>
            </a>
          </li>
          <!-- <li class="treeview">
            <a href="#">
              <i class="fa fa-book"></i>
              <span>Laporan</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="drtab.php"><i class="fa fa-table"></i>Tabungan</a></li>
              cetak per santri dan semua
              <li><a href="drtrans.php"><i class="fa fa-table"></i>Transaksi</a></li>
              cetak per santri dan semua
              <li><a href="drsem.php"><i class="fa fa-table"></i>Semua</a></li>
            </ul>
          </li> -->
          <?php if ($_SESSION['level'] == 'sadmin') { ?>
            <li class="header">DATA MASTER</li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-user"></i>
                <span>Santri</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="tsantri.php"><i class="fa fa-user-plus"></i> Tambah Santri</a></li>
                <li><a href="dsantri.php"><i class="fa fa-table"></i> Daftar Santri </a></li>
              </ul>
            </li>
            <li>
              <a href="survey.php">
                <i class="fa fa-commenting"></i> <span>Survey</span>
              </a>
            </li>
            <li>
              <li class="treeview">
              <a href="#">
                <i class="fa fa-usd"></i>
                <span>Jenis Transaksi</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="tjt.php"><i class="fa fa-plus"></i> Tambah Jenis Transaksi</a></li>
                <li><a href="djt.php"><i class="fa fa-table"></i> Daftar Jenis Transaksi</a></li>
              </ul>
            </li>
            <li>
              <a href="tjenjang.php">
                <i class="fa fa-graduation-cap"></i> <span>Jenjang Pendidikan</span>
              </a>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-user-secret"></i>
                <span>Admin</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="tadmin.php"><i class="fa fa-user-plus"></i> Tambah Admin</a></li>
                <li><a href="dadmin.php"><i class="fa fa-table"></i> Daftar Admin</a></li>
              </ul>
            </li>
            <li>
              <a href="db.php">
                <i class="fa fa-database"></i> <span>Database</span>
              </a>
            </li>
            <li>
              <a href="kartu.php">
                <i class="fa fa-id-card"></i> <span>Kartu</span>
              </a>
            </li>
            <li>
              <a href="tutup.php">
                <i class="fa fa-address-book"></i> <span>Tutup Buku</span>
              </a>
            </li>
          <?php } ?>
          <li>
            <a href="logout.php">
              <i class="fa fa-unlock"></i> <span>Sign Out</span>
            </a>
          </li>
        </ul>
      </section>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">