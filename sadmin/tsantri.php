<?php require_once('atas.php');
if (isset($_POST["save"])) {
    $angkatan = $_POST['angkatan'];


    $nama_lengkap = $_POST['nama_lengkap'];
    $tempat_lhr = $_POST['tempat_lhr'];
    $tgl_lhr = $_POST['tgl_lhr'];
    $jke = $_POST['jkel'];
    if ($jke == 'L') {
        $jkel = 1;
        $ang = $angkatan . $jkel;
    } else {
        $jkel = 2;
        $ang = $angkatan . $jkel;
    }
    $alamat = $_POST['alamat'];
    $nm_ayah = $_POST['nm_ayah'];
    $nm_ibu = $_POST['nm_ibu'];
    $no_telp_wali = $_POST['no_telp_wali'];
    // $foto=$_POST['foto'];
    $create_by = $_SESSION['id_admin'];
    $create_waktu = date("Y-m-d h:i:s");
    $edit_by = '';
    $edit_waktu = '0000-00-00 00:00:00';
    $status_santri = 'Aktif';
    $id_jenjang = $_POST['id_jenjang'];;
    $ni = mysqli_query($kon, "select * from santri where nis LIKE '%$ang%' order by nis desc limit 1");
    $nit = mysqli_fetch_array($ni);
    $jni = mysqli_num_rows($ni);
    if ($jni > 0) {
        $nis = $nit['nis'] + 1;
    } else {
        $nis = $ang . '0001';
    }
    // echo "$ang<br>$nis";

    $file_name       = $_FILES['file']['name'];
    $explode         = explode('.', $file_name);
    $file_ext        = $explode[count($explode) - 1];
    $file_size       = $_FILES['file']['size'];
    $file_tmp        = $_FILES['file']['tmp_name'];

    $maxsize = 1024 * 1024; // maksimal 200 KB (1KB = 1024 Byte)
    $valid_ext = array('jpg', 'jpeg', 'png');
    if (in_array($file_ext, $valid_ext) === true) {
        if ($file_size < 10440700) {
            $t = time() . '_' . $file_name;
            $lokasi = '../files/' . $t;
            $password = md5($nis);
            $a = mysqli_query($kon, "insert into santri values(
                        '$nis',
                        '$angkatan',
                        '$nama_lengkap',
                        '$tempat_lhr',
                        '$tgl_lhr',
                        '$jke',
                        '$alamat',
                        '$nm_ayah',
                        '$nm_ibu',
                        '$no_telp_wali',
                        '$t',
                        '$create_by',
                        '$create_waktu',
                        '$edit_by',
                        '$edit_waktu',
                        '$status_santri',
                        '$id_jenjang',
                        '$password'
                        )");
            if ($a) {
                move_uploaded_file($file_tmp, $lokasi);
                echo "<script type='text/javascript'>alert(\"Berhasil!\");window.location=\"dsantri.php\";</script>";
            } else {
                echo "<script type='text/javascript'>alert(\"Data sudah ada!\");history.back();</script>";
            }
        } else {
            echo "<script type='text/javascript'>alert(\"Gagal upload foto!\");history.back();</script>";
        }
    } else {
        echo "<script type='text/javascript'>alert(\"Gagal upload foto!\");history.back();</script>";
    }
}
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Dashboard
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Santri</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <!-- Left col -->
        <section class="col-lg-7 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-user-plus"></i> Tambah Santri</h3>
                </div>
                <form action="" method="POST" enctype="multipart/form-data">
                    <div class="box-body">
                        <div class="form-group">
                            <label>Angkatan</label>
                            <?php
                            $d = date('Y');
                            $dd = $d + 1;
                            $da = $d - 10;
                            ?>
                            <select name='angkatan' class="form-control select2" style="width: 100%;" required>
                                <option selected="selected" value="">Pilihan</option>
                                <?php
                                for ($dd; $dd >= $da; $dd--) {
                                    echo "<option value='$dd'>$dd</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Nama Lengkap</label>
                            <input type="text" name='nama_lengkap' class="form-control" placeholder="Masukkan Nama Lengkap" required>
                        </div>
                        <div class="form-group">
                            <label>Tempat Lahir</label>
                            <input type="text" name='tempat_lhr' class="form-control" placeholder="Masukkan Tempat Lahir" required>
                        </div>
                        <div class="form-group">
                            <label>Tanggal Lahir</label>
                            <input type="date" name='tgl_lhr' class="form-control" placeholder="Masukkan Tanggal Lahir" required>
                        </div>
                        <div class="form-group">
                            <label>Jenis Kelamin</label><br>
                            <input type="radio" name="jkel" class="flat-red" value="L"> Laki-laki
                            <input type="radio" name="jkel" class="flat-red" value="P"> Perempuan
                        </div>
                        <div class="form-group">
                            <label>Alamat</label>
                            <input type="text" name='alamat' class="form-control" placeholder="Masukkan Alamat" required>
                        </div>
                        <div class="form-group">
                            <label>Nama Ayah</label>
                            <input type="text" name='nm_ayah' class="form-control" placeholder="Masukkan Nama Ayah" required>
                        </div>
                        <div class="form-group">
                            <label>Nama Ibu</label>
                            <input type="text" name='nm_ibu' class="form-control" placeholder="Masukkan Nama Ibu" required>
                        </div>
                        <div class="form-group">
                            <label>No Telp Wali</label>
                            <input type="text" name='no_telp_wali' class="form-control" placeholder="Masukkan No Telp Wali" required>
                        </div>
                        <div class="form-group">
                            <label>Jenjang Pendidikan</label>
                            <select name='id_jenjang' class="form-control select2" style="width: 100%;" required>
                                <option selected="selected" value="">Pilihan</option>
                                <?php
                                $s = mysqli_query($kon, "select * from jenjang order by jenjang asc");
                                while ($ss = mysqli_fetch_array($s)) {
                                    echo "<option value='$ss[id_jenjang]'>$ss[jenjang]</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Foto</label>
                            <input name='file' type="file" class="form-control" placeholder="Pilih Foto" accept=".jpg,.jpeg,.png" required>
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <p>*Password sesuai awal NIS</p>
                        </div>
                    </div>
                    <div class="box-footer">
                        <input type="submit" name='save' class="btn btn-primary" value="Submit" onclick=\"return confirm('Yakin?')\">
                        <input type="Reset" class="btn btn-danger" value="Reset">
                        <a href="dsantri.php"><button type="button" class="btn btn-info">Lihat</button></a>
                    </div>
                </form>
                <!-- /.box-body -->
            </div>
        </section>
        <!-- /.Left col -->
    </div>
    <!-- /.row (main row) -->

</section>
<?php require_once('bawah.php'); ?>