<?php require_once('atas.php');
if (isset($_POST["save"])) {

    // $jt = mysqli_query($kon, "select *, sum(jumlah_tab) as jtab from tabungan where nis='$nis'");
    // $jt1 = mysqli_fetch_array($jt);
    // $jtr = mysqli_query($kon, "select *, sum(jumlah_trans) as jtrans from trans where nis='$nis'");
    // $jtr1 = mysqli_fetch_array($jtr);
    // $saldo = $jt1['jtab'] - $jtr1['jtrans'];

    $nis = $_POST['nis'];
    $jumlah_trans = $_POST['jumlah_trans'];
    $tgl_lh = date('Y-m-d', strtotime($_POST['tgl_trans']));
    $tgl_trans = $tgl_lh;
    $id_jtrans = $_POST['id_jtrans'];
    $ket_trans = $_POST['ket_trans'];
    $create_by = $_SESSION['id_admin'];
    $create_at = date("Y-m-d h:i:s");
    $edit_by = '';
    $edit_at = '';
    $tab = date('Ymd');
    $ni = mysqli_query($kon, "select * from trans where id_trans LIKE '%$tab%' order by id_trans desc limit 1");
    $nit = mysqli_fetch_array($ni);
    $jni = mysqli_num_rows($ni);
    if ($jni > 0) {
        $id_trans = $nit['id_trans'] + 1;
    } else {
        $id_trans = $tab . '0001';
    }
    $a = mysqli_query($kon, "insert into trans values(
                '$id_trans',
                '$nis',
                '$jumlah_trans',
                '$tgl_trans',
                '$id_jtrans',
                '$ket_trans',
                '$create_by',
                '$create_at',
                '$edit_by',
                '$edit_at'
                )");
    if ($a) {
        //1 print 1 balek tambah transaksi
        echo "<script type='text/javascript'>alert(\"Berhasil!\");window.location=\"ptrans.php?p=$id_trans\";</script>";
    } else {
        echo "<script type='text/javascript'>alert(\"Gagal!\");history.back();</script>";
    }
}
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Dashboard
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Transaksi</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <!-- Left col -->
        <section class="col-lg-7 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-credit-card"></i> Tambah Transaksi</h3>
                </div>
                <form action="" method="POST" enctype="multipart/form-data">
                    <div class="box-body">
                        <div class="form-group">
                            <label>NIS</label>
                            <select id="cektab" name='nis' class="form-control select2" style="width: 100%;" required>
                                <option selected="selected" value="">Pilihan</option>
                                <?php
                                if ($_SESSION['level'] == 'sadmin') {
                                    $s = mysqli_query($kon, "select * from santri where status_santri='Aktif'");
                                } else {
                                    $s = mysqli_query($kon, "select * from santri $_SESSION[where] and status_santri='Aktif'");
                                }
                                while ($ss = mysqli_fetch_array($s)) {
                                    echo "<option value='$ss[nis]' ";
                                    if (isset($_POST['nis']) and $ss['nis'] == $_POST['nis']) {
                                        echo "selected";
                                    }
                                    echo ">$ss[nama_lengkap] ($ss[nis])</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Sisa Saldo</label>
                            <div id="display_info">
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Jumlah Transaksi</label>
                            <input type="number" min=1 name='jumlah_trans' class="form-control" placeholder="Masukkan Jumlah" required>
                        </div>
                        <div class="form-group">
                            <label>Tanggal</label>
                            <input type="text" name='tgl_trans' class="form-control pull-right" id="datepicker" placeholder="Masukkan Tanggal" required>
                        </div>
                        <div class="form-group">
                            <label>Jenis Transaksi</label>
                            <select name='id_jtrans' class="form-control select2" style="width: 100%;" required>
                                <option selected="selected" value="">Pilihan</option>
                                <?php
                                $s = mysqli_query($kon, "select * from jtrans where status_jtrans='Aktif'");
                                while ($ss = mysqli_fetch_array($s)) {
                                    echo "<option value='$ss[id_jtrans]'>$ss[ket_jtrans]</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Keterangan</label>
                            <textarea name="ket_trans" class="form-control" placeholder="Masukkan Keterangan"></textarea>
                        </div>
                    </div>
                    <div class="box-footer">
                        <input type="submit" name='save' class="btn btn-primary" value="Submit">
                        <input type="Reset" class="btn btn-danger" value="Reset">
                        <a href="dtrans.php"><button type="button" class="btn btn-info">Lihat</button></a>
                    </div>
                </form>
                <!-- /.box-body -->
            </div>
        </section>
        <!-- /.Left col -->
    </div>
    <!-- /.row (main row) -->

</section>
<?php require_once('bawah.php'); ?>
<script type="text/javascript">
    $(function() {
        $("#cektab").change(function() {
            var selectedText = $(this).find("option:selected").text();
            var selectedValue = $(this).val();
            if (selectedValue) {
                $.ajax({
                    type: 'post',
                    url: 'ceksaldo.php',
                    data: {
                        nis: selectedValue,
                    },
                    success: function(response) {
                        // We get the element having id of display_info and put the response inside it
                        $('#display_info').html(response);
                    }
                });
            } else {
                $('#display_info').html("Pilih santri dulu.");
            }
            // alert("Selected Text: " + selectedText + " Value: " + selectedValue);
        });
    });
</script>