<?php require_once('atas.php');
$sa = mysqli_query($kon, "select * from tabungan where id_tabungan='$_GET[e]'");
$ssa = mysqli_fetch_array($sa);
if (isset($_POST["save"])) {
    $nis = $_POST['nis'];
    $jumlah_tab = $_POST['jumlah_tab'];
    $tgl_lh = date('Y-m-d', strtotime($_POST['tgl_tab']));
    $tgl_tab = $tgl_lh;
    $edit_by = $_SESSION['id_admin'];
    $edit_at = date("Y-m-d h:i:s");
    // echo$tab;
    $a = mysqli_query($kon, "update tabungan set
            nis='$nis',
            jumlah_tab='$jumlah_tab',
            tgl_tab='$tgl_tab',
            edit_by='$edit_by',
            edit_at='$edit_at'
            where id_tabungan='$_POST[id_tabungan]'
            ");
    if ($a) {
        echo "<script type='text/javascript'>alert(\"Berhasil!\");window.location=\"dtab.php\";</script>";
    } else {
        echo "<script type='text/javascript'>alert(\"Gagal!\");history.back();</script>";
    }
}
$d = date('m/d/Y', strtotime($ssa['tgl_tab']));
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Dashboard
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Tabungan</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <!-- Left col -->
        <section class="col-lg-7 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-money"></i> Edit Tabungan</h3>
                </div>
                <form action="" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name='id_tabungan' class="form-control" value='<?php echo $ssa['id_tabungan']; ?>' readonly>
                    <div class="box-body">
                        <div class="form-group">
                            <label>NIS</label>
                            <select name='nis' class="form-control select2" style="width: 100%;" required>
                                <option selected="selected" value="">Pilihan</option>
                                <?php
                                $s = mysqli_query($kon, "select * from santri where status_santri='Aktif'");
                                while ($ss = mysqli_fetch_array($s)) {
                                    if ($ss['nis'] == $ssa['nis']) {
                                        echo "<option value='$ss[nis]' selected>$ss[nama_lengkap] ($ss[nis])</option>";
                                    } else {
                                        echo "<option value='$ss[nis]'>$ss[nama_lengkap] ($ss[nis])</option>";
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Jumlah</label>
                            <input type="number" min=0 name='jumlah_tab' class="form-control" placeholder="Masukkan Jumlah" value='<?php echo $ssa['jumlah_tab']; ?>' required>
                        </div>
                        <div class="form-group">
                            <label>Tanggal</label>
                            <input type="text" name='tgl_tab' class="form-control pull-right" id="datepicker" placeholder="Masukkan Tanggal" value='<?php echo $d; ?>' required>
                        </div>
                    </div>
                    <div class="box-footer">
                        <input type="submit" name='save' class="btn btn-primary" value="Submit">
                        <input type="Reset" class="btn btn-danger" value="Reset">
                        <a href="dtab.php"><button type="button" class="btn btn-info">Lihat</button></a>
                    </div>
                </form>
                <!-- /.box-body -->
            </div>
        </section>
        <!-- /.Left col -->
    </div>
    <!-- /.row (main row) -->

</section>
<?php require_once('bawah.php'); ?>