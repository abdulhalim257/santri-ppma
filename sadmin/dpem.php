<?php require_once('atas.php');
if (isset($_GET["d"])) {
    $no = 1;
    // unlink('../files/'.$dd['foto']);
    mysqli_query($kon, "delete from pembayaran where id_bayar='$_GET[d]'");
    echo "<script type='text/javascript'>alert(\"Berhasil!\");window.location=\"dpem.php\";</script>";
}
// elseif(isset($_GET["r"])){
//     $p=md5('pembayaran');
//     $a=mysqli_query($kon,"update id_pembayaran set
//     password='$p' where id_pembayaran='$_GET[r]'");
//     echo "<script type='text/javascript'>alert(\"Berhasil!\");window.location=\"did_pembayaran.php\";</script>";
// }
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Dashboard
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">pembayaran</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"><a href='#'><i class="fa fa-cc-mastercard"></i></a> Daftar Pembayaran</h3>

                    <form style='margin-right:5px; margin-top:0px' class='pull-right' action='' method='GET'>
                        <select class='select2' name='j' style='padding:4px'>
                            <?php
                            echo "<option value='all'>- Semua -</option>";
                            echo "<option value='L' ";
                            if (isset($_GET['j']) and $_GET['j'] == "L") {
                                echo "selected";
                            }
                            echo ">Laki-laki</option>";
                            echo "<option value='P'";
                            if (isset($_GET['j']) and $_GET['j'] == "P") {
                                echo "selected";
                            }
                            echo ">Perempuan</option>";
                            ?>
                        </select>
                        <input type="submit" style='margin-top:0px' class='btn btn-success btn-sm' value='Lihat'>
                    </form>
                </div>
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>ID Pembayaran</th>
                                <th>NIS</th>
                                <th>Nama Lengkap</th>
                                <th>JKel</th>
                                <th>Nominal</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            if (isset($_GET['j'])) {
                                if ($_GET['j'] == "all") {
                                    $s = mysqli_query($kon, "select * from pembayaran left join tagihan on pembayaran.id_pembayaran=tagihan.id_pembayaran left join santri on tagihan.nis=santri.nis order by id_bayar desc");
                                } else {
                                    $s = mysqli_query($kon, "select * from pembayaran left join tagihan on pembayaran.id_pembayaran=tagihan.id_pembayaran left join santri on tagihan.nis=santri.nis where jkel='$_GET[j]' order by id_bayar desc");
                                }
                                while ($f = mysqli_fetch_array($s)) {
                                    echo "
                            <tr>
                                <td>$no</td>
                                <td>$f[id_pembayaran]</td>
                                <td>$f[nis]</td>
                                <td>$f[nama_lengkap]</td>
                                <td>$f[jkel]</td>
                                <td>Rp " . number_format($f['nominal'], 2, ',', '.') . "</td>
                                <td>
                                ";
                                    if ($f['status_tagihan'] == 'Belum Lunas') {
                                        echo "
                                <a class='btn btn-info btn-xs' href='tpem.php?t=$f[id_pembayaran]'>
                                <i class='fa fa-plus'></i></a>
                                <a class='btn btn-danger btn-xs' href='?d=$f[id_bayar]' onclick=\"return confirm('Yakin hapus?')\">
                                <i class='fa fa-remove'></i></a>";
                                    }
                                    echo "
                                <a class='btn btn-primary btn-xs' href='ppem.php?p=$f[id_bayar]' target=_blank()>
                                <i class='fa fa-print'></i></a>
                                </td>
                            </tr>
                            ";
                                    $no++;
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </section>
        <!-- /.Left col -->
    </div>
    <!-- /.row (main row) -->

</section>
<?php require_once('bawah.php'); ?>