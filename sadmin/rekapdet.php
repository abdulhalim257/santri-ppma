<?php require_once('atas.php');
$s = mysqli_query($kon, "select * from santri left join jenjang on santri.id_jenjang=jenjang.id_jenjang where santri.nis='$_GET[n]' order by nis desc");
$f = mysqli_fetch_array($s);

// ambil data untuk keterangan isinya nama kategori + sayhraih kos_makan sekolah (3)

$jt = mysqli_query($kon, "select count(*) as jt from jtrans");
$fjt = mysqli_fetch_array($jt);
$kolom = $fjt['jt'] + 3;
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Dashboard
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Rekap</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Rekap</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive" style="border: 0px solid #ddd;">
                                <table>
                                    <tr>
                                        <th>NIS</th>
                                        <th> : </th>
                                        <th><?php echo "$f[nis]"; ?></th>
                                    </tr>
                                    <tr>
                                        <th>Nama Lengkap</th>
                                        <th> : </th>
                                        <th><?php echo "$f[nama_lengkap]"; ?></th>
                                    </tr>
                                    <tr>
                                        <th>Jenis Kelamin</th>
                                        <th> : </th>
                                        <th><?php echo "$f[jkel]"; ?></th>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <h4>Data Pengeluaran</h4>
                            <div class="table-responsive" style="border: 0px solid #ddd;">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Bulan</th>
                                            <th>Keterangan</th>
                                            <th>Kredit</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $no = 1;
                                        //  tgl tabungan paling kecil
                                        $tgltab = mysqli_query($kon, "select tgl_tab from tabungan where nis='$f[nis]' order by tgl_tab asc limit 1");
                                        $jtgltab = mysqli_fetch_array($tgltab);

                                        //  tgl tagihan paling kecil
                                        $tgltag = mysqli_query($kon, "select waktu_tagihan from tagihan where nis='$f[nis]' order by waktu_tagihan asc limit 1");
                                        $jtgltag = mysqli_fetch_array($tgltag);

                                        //  tgl transaksi paling kecil
                                        $tgltrans = mysqli_query($kon, "select tgl_trans from trans where nis='$f[nis]' order by tgl_trans asc limit 1");
                                        $jtgltrans = mysqli_fetch_array($tgltrans);

                                        if (empty($jtgltag['waktu_tagihan'])) {
                                            $b1 = date('Y-m');
                                        } else {
                                            $b1 = $jtgltag['waktu_tagihan'];
                                        }

                                        if (empty($jtgltab['tgl_tab'])) {
                                            $b2 = date('Y-m');
                                        } else {
                                            $b2 = $jtgltab['tgl_tab'];
                                        }

                                        if (empty($jtgltrans['tgl_trans'])) {
                                            $b3 = date('Y-m');
                                        } else {
                                            $b3 = $jtgltrans['tgl_trans'];
                                        }
                                        $tgl = array($b1, $b2, $b3);
                                        $ttgl = substr(min($tgl), 0, 7);
                                        $date = date("Y-m");
                                        $timeStart = strtotime("$ttgl");
                                        $timeEnd = strtotime("$date");
                                        // Menambah bulan ini + semua bulan pada tahun sebelumnya
                                        $numBulan = 1 + (date("Y", $timeEnd) - date("Y", $timeStart)) * 12;
                                        // menghitung selisih bulan
                                        $numBulan += date("m", $timeEnd) - date("m", $timeStart);

                                        for ($i = 0; $i <= $numBulan; $i++) {
                                            $tampilbulan = date('Y-m', strtotime('+' . $i . ' month', strtotime($ttgl)));
                                            $td = substr($tampilbulan, 5, 2);
                                            $td2 = substr($tampilbulan, 0, 4);
                                            switch ($td) {

                                                case '01':
                                                    $tdd = "Januari";
                                                    break;

                                                case '02':
                                                    $tdd = "Februari";
                                                    break;

                                                case '03':
                                                    $tdd = "Maret";
                                                    break;

                                                case '04':
                                                    $tdd = "April";
                                                    break;

                                                case '05':
                                                    $tdd = "Mei";
                                                    break;

                                                case '06':
                                                    $tdd = "Juni";
                                                    break;

                                                case '07':
                                                    $tdd = "Juli";
                                                    break;

                                                case '08':
                                                    $tdd = "Agustus";
                                                    break;

                                                case '09':
                                                    $tdd = "September";
                                                    break;

                                                case '10':
                                                    $tdd = "Oktober";
                                                    break;

                                                case '11':
                                                    $tdd = "November";
                                                    break;

                                                case '12':
                                                    $tdd = "Desember";
                                                    break;

                                                default:
                                                    # code...
                                                    break;
                                            }
                                            //itung syahriah bulan itu
                                            $tag = mysqli_query($kon, "select sum(syahriah) as Syahraih from tagihan where waktu_tagihan='$tampilbulan' and nis='$f[nis]'");
                                            $jtag = mysqli_fetch_array($tag);
                                            if ($jtag['Syahraih'] > 0) {
                                                $tag1 = $jtag['Syahraih'];
                                            } else {
                                                $tag1 = 0;
                                            }
                                            echo "
                                            <tr>
                                                <td>$no</td>
                                                <td rowspan='$kolom'>$tdd $td2</td>
                                                <td>Syahraih</td>
                                                <td>Rp " . number_format($tag1, 0, ',', '.') . "</td>
                                            </tr>";
                                            $no++;
                                            //itung Sekolah bulan itu
                                            $tag = mysqli_query($kon, "select sum(sekolah) as Sekolah from tagihan where waktu_tagihan='$tampilbulan' and nis='$f[nis]'");
                                            $jtag = mysqli_fetch_array($tag);
                                            if ($jtag['Sekolah'] > 0) {
                                                $tag2 = $jtag['Sekolah'];
                                            } else {
                                                $tag2 = 0;
                                            }
                                            echo "
                                            <tr>
                                                <td>$no</td>
                                                <td>Sekolah</td>
                                                <td>Rp " . number_format($tag2, 0, ',', '.') . "</td>
                                            </tr>";
                                            $no++;
                                            //itung Kos Makan bulan itu
                                            $tag = mysqli_query($kon, "select sum(kos_makan) as jKos from tagihan where waktu_tagihan='$tampilbulan' and nis='$f[nis]'");
                                            $jtag = mysqli_fetch_array($tag);
                                            if ($jtag['jKos'] > 0) {
                                                $tag3 = $jtag['jKos'];
                                            } else {
                                                $tag3 = 0;
                                            }
                                            echo "
                                            <tr>
                                                <td>$no</td>
                                                <td>Kos Makan</td>
                                                <td>Rp " . number_format($tag3, 0, ',', '.') . "</td>
                                            </tr>";
                                            $no++;
                                            $tjtrans = mysqli_query($kon, "select * from jtrans");
                                            while ($ftjtrans = mysqli_fetch_array($tjtrans)) {
                                                $jtrans = mysqli_query($kon, "select sum(jumlah_trans) as jtrans from trans where substr(tgl_trans,1,7)='$tampilbulan' and id_jtrans='$ftjtrans[id_jtrans]' and nis='$f[nis]'");
                                                $jtrans1 = mysqli_fetch_array($jtrans);
                                                $tag4 = !empty($jtrans1['jtrans']) ? $jtrans1['jtrans'] : 0;
                                                echo "
                                                <tr>
                                                <td>$no</td>
                                                <td>$ftjtrans[ket_jtrans]</td>
                                                <td>Rp " . number_format($tag4, 0, ',', '.') . "</td>
                                                </tr>";
                                                $no++;
                                            }
                                            //jumlah transsaksi bulan ini
                                            $jtrans = mysqli_query($kon, "select sum(jumlah_trans) as jtrans from trans where substr(tgl_trans,1,7)='$tampilbulan' and nis='$f[nis]'");
                                            $jtrans1 = mysqli_fetch_array($jtrans);
                                            $tag4 = !empty($jtrans1['jtrans']) ? $jtrans1['jtrans'] : 0;

                                            $total_kredit[$i] = $tag1 + $tag2 + $tag3 + $tag4;
                                            echo "
                                            <tr>
                                                <td>$no</td>
                                                <td></td>
                                                <td><b>Total Kredit</b></td>
                                                <td><b>Rp " . number_format($total_kredit[$i], 0, ',', '.') . "</b></td>
                                            </tr>";
                                            $no++;
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h4>Rekap Data Penitipan</h4>
                            <div class="table-responsive" style="border: 0px solid #ddd;">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Bulan</th>
                                            <th>Keterangan</th>
                                            <th>Debet</th>
                                            <th>Kredit</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $no = 1;
                                        //  tgl tabungan paling kecil
                                        $tgltab = mysqli_query($kon, "select tgl_tab from tabungan where nis='$f[nis]' order by tgl_tab asc limit 1");
                                        $jtgltab = mysqli_fetch_array($tgltab);

                                        //  tgl tagihan paling kecil
                                        $tgltag = mysqli_query($kon, "select waktu_tagihan from tagihan where nis='$f[nis]' order by waktu_tagihan asc limit 1");
                                        $jtgltag = mysqli_fetch_array($tgltag);

                                        //  tgl transaksi paling kecil
                                        $tgltrans = mysqli_query($kon, "select tgl_trans from trans where nis='$f[nis]' order by tgl_trans asc limit 1");
                                        $jtgltrans = mysqli_fetch_array($tgltrans);

                                        if (empty($jtgltag['waktu_tagihan'])) {
                                            $b1 = date('Y-m');
                                        } else {
                                            $b1 = $jtgltag['waktu_tagihan'];
                                        }

                                        if (empty($jtgltab['tgl_tab'])) {
                                            $b2 = date('Y-m');
                                        } else {
                                            $b2 = $jtgltab['tgl_tab'];
                                        }

                                        if (empty($jtgltrans['tgl_trans'])) {
                                            $b3 = date('Y-m');
                                        } else {
                                            $b3 = $jtgltrans['tgl_trans'];
                                        }
                                        $tgl = array($b1, $b2, $b3);
                                        $ttgl = substr(min($tgl), 0, 7);
                                        $date = date("Y-m");
                                        $timeStart = strtotime("$ttgl");
                                        $timeEnd = strtotime("$date");
                                        // Menambah bulan ini + semua bulan pada tahun sebelumnya
                                        $numBulan = 1 + (date("Y", $timeEnd) - date("Y", $timeStart)) * 12;
                                        // menghitung selisih bulan
                                        $numBulan += date("m", $timeEnd) - date("m", $timeStart);

                                        $subdebet = 0;
                                        $subkredit = 0;

                                        for ($i = 0; $i <= $numBulan; $i++) {
                                            $tampilbulan = date('Y-m', strtotime('+' . $i . ' month', strtotime($ttgl)));
                                            $td = substr($tampilbulan, 5, 2);
                                            $td2 = substr($tampilbulan, 0, 4);
                                            switch ($td) {

                                                case '01':
                                                    $tdd = "Januari";
                                                    break;

                                                case '02':
                                                    $tdd = "Februari";
                                                    break;

                                                case '03':
                                                    $tdd = "Maret";
                                                    break;

                                                case '04':
                                                    $tdd = "April";
                                                    break;

                                                case '05':
                                                    $tdd = "Mei";
                                                    break;

                                                case '06':
                                                    $tdd = "Juni";
                                                    break;

                                                case '07':
                                                    $tdd = "Juli";
                                                    break;

                                                case '08':
                                                    $tdd = "Agustus";
                                                    break;

                                                case '09':
                                                    $tdd = "September";
                                                    break;

                                                case '10':
                                                    $tdd = "Oktober";
                                                    break;

                                                case '11':
                                                    $tdd = "November";
                                                    break;

                                                case '12':
                                                    $tdd = "Desember";
                                                    break;

                                                default:
                                                    # code...
                                                    break;
                                            }
                                            //jumlah transsaksi bulan ini
                                            $jjtab = mysqli_query($kon, "select sum(jumlah_tab) as jtab from tabungan where substr(tgl_tab,1,7)='$tampilbulan' and nis='$f[nis]'");
                                            $jjtab1 = mysqli_fetch_array($jjtab);
                                            $tjtab = !empty($jjtab1['jtab']) ? $jjtab1['jtab'] : 0;

                                            echo "
                                            <tr>
                                                <td>$no</td>
                                                <td>$tdd $td2</td>
                                                <td>Titip Bulanan</td>
                                                <td>Rp " . number_format($tjtab, 0, ',', '.') . "</td>
                                                <td>Rp " . number_format($total_kredit[$i], 0, ',', '.') . "</td>
                                            </tr>";
                                            $no++;
                                            // subtotal
                                            $subdebet += $tjtab;
                                            $subkredit += $total_kredit[$i];
                                        }
                                        echo "
                                            <tr>
                                                <td><b>XX</b></td>
                                                <td><b></b></td>
                                                <td><b>Jumlah</b></td>
                                                <td><b>Rp " . number_format($subdebet, 0, ',', '.') . "</b></td>
                                                <td><b>Rp " . number_format($subkredit, 0, ',', '.') . "</b></td>
                                            </tr>
                                            <tr>
                                                <td><b>XX</b></td>
                                                <td><b></b></td>
                                                <td><b>Saldo</b></td>
                                                <td colspan=2><b style='text-align:center'>Rp " . number_format($subdebet - $subkredit, 0, ',', '.') . "</b></td>
                                            </tr>
                                            ";
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
        </section>
        <!-- /.Left col -->
    </div>
    <!-- /.row (main row) -->

</section>
<?php require_once('bawah.php'); ?>