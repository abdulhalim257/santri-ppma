<?php require_once('atas.php');
if (isset($_POST["simpan"])) {
    if (empty($_POST['cbulan'])) {
        echo "<script type='text/javascript'>alert(\"Bulan belum dipilih!\");history.back();</script>";
    } else {
        if ($_SESSION['level'] == 'sadmin') {
            if (isset($_GET['j']) and $_GET['j'] == "all" or empty($_GET['j'])) {
                $s = mysqli_query($kon, "select * from santri left join jenjang on santri.id_jenjang=jenjang.id_jenjang order by nis desc");
            } else {
                $s = mysqli_query($kon, "select * from santri left join jenjang on santri.id_jenjang=jenjang.id_jenjang where jkel='$_GET[j]' order by nis desc");
            }
        } else {
            $s = mysqli_query($kon, "select * from santri left join jenjang on santri.id_jenjang=jenjang.id_jenjang $_SESSION[where] order by nis desc");
        }
        $no = 1;
        while ($f = mysqli_fetch_array($s)) {
            $intag = mysqli_query($kon, "select * from tagihan where nis='$f[nis]' and waktu_tagihan='$_POST[cbulan]' ");
            $syh = $_POST['syahriah' . $f['nis']];
            $km = $_POST['kos_makan' . $f['nis']];
            $skl = $_POST['sekolah' . $f['nis']];
            $created_by = $_SESSION['id_admin'];
            $created_at = date("Y-m-d h:i:s");
            if (mysqli_num_rows($intag) > 0) {
                $a = mysqli_query($kon, "update tagihan set
                    syahriah='$syh',
                    kos_makan='$km',
                    sekolah='$skl',
                    created_by='$created_by',
                    created_at='$created_at'
                    where 
                    waktu_tagihan='$_POST[cbulan]' and
                    nis='$f[nis]'
                    ");
            } else {
                // id_tagihan
                // waktu_tagihan
                // nis
                // syahriah
                // kos_makan
                // sekolah
                // created_by
                // created_at
                // ket

                $a = mysqli_query($kon, "insert into tagihan values(
                    NULL,
                    '$_POST[cbulan]',
                    '$f[nis]',
                    '$syh',
                    '$km',
                    '$skl',
                    '$created_by',
                    '$created_at',
                    ''
                    )");
            }
            if ($a) {
                echo "<script type='text/javascript'>alert(\"Berhasil!\");window.location=\"dtag.php\";</script>";
            } else {
                echo "<script type='text/javascript'>alert(\"Gagal!\");history.back();</script>";
            }
            $no++;
        }
    }
}
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Dashboard
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Tagihan</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"><a href='dtag.php'><i class="fa fa-list"></i></a> Tambah Tagihan Santri</h3>
                    <form style='margin-right:5px; margin-top:0px' class='pull-right' action='' method='GET'>
                        <input type="hidden" name="a" id="a">
                        <input type="hidden" name="b" id="b">
                        <input type="hidden" name="c" id="c">
                        <input type="month" name="bulan" value="<?php if (isset($_GET['bulan'])) {
                                                                    echo "$_GET[bulan]";
                                                                } ?>" required>

                        <?php if ($_SESSION['level'] == 'sadmin') { ?>
                            <select class='select2' name='j' style='padding:4px'>
                                <?php
                                echo "<option value='all'>- Semua -</option>";
                                echo "<option value='L' ";
                                if (isset($_GET['j']) and $_GET['j'] == "L") {
                                    echo "selected";
                                }
                                echo ">Laki-laki</option>";
                                echo "<option value='P'";
                                if (isset($_GET['j']) and $_GET['j'] == "P") {
                                    echo "selected";
                                }
                                echo ">Perempuan</option>";
                                ?>
                            </select>
                        <?php } ?>
                        <input type="submit" style='margin-top:0px' class='btn btn-success btn-sm' value='Lihat'>
                    </form>
                </div>
                <div class="box-body">
                    <form action='' method='POST'>
                        <div class="table-responsive" style="border: 0px solid #ddd;">
                            <table id="example3" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>NIS</th>
                                        <th>Nama Lengkap</th>
                                        <th>Jenjang</th>
                                        <th>Syahriah<br><input type='number' name='csyahirah' id='csyahirah' min=0 style="width:95%" value="<?php if (isset($_GET['a'])) {
                                                                                                                                                echo "$_GET[a]";
                                                                                                                                            } else {
                                                                                                                                                echo "0";
                                                                                                                                            } ?>">
                                        </th>
                                        <th>Kos Makan<br><input type='number' name='ckos_makan' id='ckos_makan' min=0 style="width:95%" value="<?php if (isset($_GET['b'])) {
                                                                                                                                                    echo "$_GET[b]";
                                                                                                                                                } else {
                                                                                                                                                    echo "0";
                                                                                                                                                } ?>"></th>
                                        <th>Sekolah<br><input type='number' name='csekolah' id='csekolah' min=0 style="width:95%" value="<?php if (isset($_GET['c'])) {
                                                                                                                                                echo "$_GET[c]";
                                                                                                                                            } else {
                                                                                                                                                echo "0";
                                                                                                                                            } ?>"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no = 1;
                                    if ($_SESSION['level'] == 'sadmin') {
                                        if (isset($_GET['j']) and $_GET['j'] == "all" or empty($_GET['j'])) {
                                            $s = mysqli_query($kon, "select * from santri left join jenjang on santri.id_jenjang=jenjang.id_jenjang order by nis desc");
                                        } else {
                                            $s = mysqli_query($kon, "select * from santri left join jenjang on santri.id_jenjang=jenjang.id_jenjang where jkel='$_GET[j]' order by nis desc");
                                        }
                                    } else {
                                        $s = mysqli_query($kon, "select * from santri left join jenjang on santri.id_jenjang=jenjang.id_jenjang $_SESSION[where] order by nis desc");
                                    }
                                    while ($f = mysqli_fetch_array($s)) {
                                        if (isset($_GET['bulan'])) {
                                            $tag = mysqli_query($kon, "select * from tagihan where nis='$f[nis]' and waktu_tagihan='$_GET[bulan]' ");
                                            $cek = mysqli_num_rows($tag);
                                        } else {
                                            $tag = mysqli_query($kon, "select * from tagihan where nis='$f[nis]' and waktu_tagihan='0' ");
                                        }
                                        $dtag = mysqli_fetch_array($tag);

                                        echo "
                                        <tr>
                                            <td>$no</td>
                                            <td>$f[nis]</td>
                                            <td>$f[nama_lengkap] <b>($f[jkel])</b></td>
                                            <td>$f[jenjang]</td>
                                            <td><input type='number' name='syahriah$f[nis]' min=0 value='";
                                        if (isset($_GET['a']) and ($_GET['a'] != 0)) {
                                            echo "$_GET[a]";
                                        } elseif (isset($dtag['syahriah'])) {
                                            echo "$dtag[syahriah]";
                                        } else {
                                            echo "0";
                                        }
                                        echo "' required></td>
                                            <td><input type='number' name='kos_makan$f[nis]' min=0 value='";
                                        if (isset($_GET['b']) and ($_GET['b'] != 0)) {
                                            echo "$_GET[b]";
                                        } elseif (isset($dtag['kos_makan'])) {
                                            echo "$dtag[kos_makan]";
                                        } else {
                                            echo "0";
                                        }
                                        echo "' required></td>
                                            <td><input type='number' name='sekolah$f[nis]' min=0 value='";
                                        if (isset($_GET['c']) and ($_GET['c'] != 0)) {
                                            echo "$_GET[c]";
                                        } elseif (isset($dtag['sekolah'])) {
                                            echo "$dtag[sekolah]";
                                        } else {
                                            echo "0";
                                        }
                                        echo "' required></td>
                                        </tr>
                                        ";
                                        $no++;
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        <input type="hidden" name="cbulan" value="<?php echo "$_GET[bulan]"; ?>">
                        <button style='margin-top:1px;' type='reset' name='reset' class='btn btn-sm btn-info pull-right'>Reset</button>
                        <button style='margin-top:1px;margin-right:5px;' type='submit' name='simpan' class='btn btn-sm btn-primary pull-right' <?php if (isset($cek) and $cek > 0) {
                                                                                                                                                    echo "onclick=\"return confirm('Data sudah ada, yakin update data?')\"";
                                                                                                                                                } else {
                                                                                                                                                    echo "onclick=\"return confirm('Yakin simpan?')\"";
                                                                                                                                                }
                                                                                                                                                ?>>Simpan</button>
                    </form>
                </div>
                <!-- /.box-body -->
            </div>
        </section>
        <!-- /.Left col -->
    </div>
    <!-- /.row (main row) -->
    <?php require_once('bawah.php'); ?>
    <script>
        $(document).ready(function() {
            $('#a').val($('#csyahirah').val());
            $('#b').val($('#ckos_makan').val());
            $('#c').val($('#csekolah').val());

            $('#csyahirah').change(function() {
                $('#a').val($('#csyahirah').val());
            });
            $('#ckos_makan').change(function() {
                $('#b').val($('#ckos_makan').val());
            });
            $('#csekolah').change(function() {
                $('#c').val($('#csekolah').val());
            });
        });
    </script>