<?php require_once('atas.php');
$s = mysqli_query($kon, "select * from jenjang where id_jenjang='$_GET[e]'");
$f = mysqli_fetch_array($s);
if (isset($_POST["save"])) {
  // echo$tab;
  $a = mysqli_query($kon, "update jenjang set
            jenjang='$_POST[jenjang]',
            syahriah='$_POST[syahriah]'
             where id_jenjang='$_POST[id_jenjang]'");
  if ($a) {
    echo "<script type='text/javascript'>alert(\"Berhasil!\");window.location=\"djenjang.php\";</script>";
  } else {
    echo "<script type='text/javascript'>alert(\"Gagal!\");history.back();</script>";
  }
}
?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Dashboard
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Jenjang Pendidikan</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <!-- Main row -->
  <div class="row">
    <!-- Left col -->
    <section class="col-lg-7 connectedSortable">
      <!-- Custom tabs (Charts with tabs)-->
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-graduation-cap"></i> Tambah Jenjang</h3>
        </div>
        <form action="" method="POST" enctype="multipart/form-data">
          <input type="hidden" name='id_jenjang' class="form-control" value='<?php echo $f['id_jenjang']; ?>' required>
          <div class="box-body">
            <div class="form-group">
              <label>Nama Jenjang</label>
              <input type="text" name='jenjang' class="form-control" placeholder="Masukkan Nama Jenjang" value='<?php echo $f['jenjang']; ?>' required>
            </div>
            <div class="form-group">
              <label>Tagihan per Bulan</label>
              <input type="number" name='syahriah' min=0 class="form-control" placeholder="Masukkan Jumlah Syahriah" value='<?php echo $f['syahriah']; ?>' required>
            </div>
          </div>
          <div class="box-footer">
            <input type="submit" name='save' class="btn btn-primary" value="Submit">
            <input type="Reset" class="btn btn-danger" value="Reset">
            <a href="djenjang.php"><button type="button" class="btn btn-info">Lihat</button></a>
          </div>
        </form>
        <!-- /.box-body -->
      </div>
    </section>
    <!-- /.Left col -->
  </div>
  <!-- /.row (main row) -->

</section>
<?php require_once('bawah.php'); ?>