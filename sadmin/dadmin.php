<?php require_once('atas.php');
if (isset($_GET["d"])) {

    $no = 1;
    $d = mysqli_query($kon, "select * from admin where id_admin='$_GET[d]'");
    $dd = mysqli_fetch_array($d);
    unlink('../files/' . $dd['foto']);
    mysqli_query($kon, "delete from admin where id_admin='$_GET[d]'");
    echo "<script type='text/javascript'>alert(\"Berhasil!\");window.location=\"dadmin.php\";</script>";
} elseif (isset($_GET["r"])) {
    $p = md5($_GET['r']);
    $a = mysqli_query($kon, "update admin set
        password='$p' where id_admin='$_GET[r]'");
    echo "<script type='text/javascript'>alert(\"Berhasil!\");window.location=\"dadmin.php\";</script>";
}
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Dashboard
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Admin</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"><a href='tadmin.php'><i class="fa fa-user-secret"></i></a> Daftar Admin</h3>
                </div>
                <div class="box-body">
                    <div class="table-responsive" style="border: 0px solid #ddd;">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>ID</th>
                                    <th>Username</th>
                                    <th>No Telp</th>
                                    <th>Level</th>
                                    <th>Status</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;
                                $s = mysqli_query($kon, "select * from admin");
                                while ($f = mysqli_fetch_array($s)) {
                                    switch ($f['level']) {
                                        case 'adminl':
                                            $lvl = 'Admin Putra';
                                            break;
                                        case 'adminp':
                                            $lvl = 'Admin Putri';
                                            break;
                                        default:
                                            $lvl = 'Super Admin';
                                            break;
                                    }
                                    echo "
                            <tr>
                                <td>$no</td>
                                <td>$f[id_admin]</td>
                                <td>$f[username]</td>
                                <td>$f[no_telp]</td>
                                <td>$lvl</td>
                                <td>$f[status_admin]</td>
                                <td>
                                <a class='btn btn-info btn-xs' href='eadmin.php?e=$f[id_admin]'>
                                <i class='fa fa-edit'></i></a>
                                <a class='btn btn-danger btn-xs' href='?d=$f[id_admin]' onclick=\"return confirm('Yakin hapus?')\">
                                <i class='fa fa-remove'></i></a>
                                <a class='btn btn-success btn-xs' href='?r=$f[id_admin]'  onclick=\"return confirm('Yakin reset password(sesuai ID Admin)?')\">
                                <i class='fa fa-recycle'></i></a>
                                </td>
                            </tr>
                            ";
                                    $no++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
        </section>
        <!-- /.Left col -->
    </div>
    <!-- /.row (main row) -->

</section>
<?php require_once('bawah.php'); ?>