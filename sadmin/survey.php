<?php require_once('atas.php');
if (isset($_GET["d"])) {
    $no = 1;
    $d = mysqli_query($kon, "select * from survey_set left join id_survey_set_add ='$_GET[d]'");
    $dd = mysqli_fetch_array($d);
    // unlink('../files/'.$dd['foto']);
    mysqli_query($kon, "delete from survey_set_add where survey ='$_GET[d]'");
    echo "<script type='text/javascript'>alert(\"Berhasil!\");window.location=\"dtab.php\";</script>";
}
// elseif(isset($_GET["r"])){
//     $p=md5('jtrans');
//     $a=mysqli_query($kon,"update nis set
//     password='$p' where nis='$_GET[r]'");
//     echo "<script type='text/javascript'>alert(\"Berhasil!\");window.location=\"dnis.php\";</script>";
// membuat survey
// }
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Dashboard
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Survey</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"><a href='survey.php'><i class="fa fa-address-book"></i></a> Daftar survey</h3>
                </div>
                <div class="box-header with-border">
                <a href="survey_set_form.php" class="btn btn-primary btn-sm btn-flat"><i class="fa fa-plus"></i> New</a>
              </div>
                <div class="box-body">
                    <div class="table-responsive" style="border: 0px solid #ddd;">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Judul</th>
                                    <th>Waktu</th>
                                    <th>Kategori</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;
                                $s = mysqli_query($kon, "select * from survey_set order by id_survey_set desc");
                                while ($f = mysqli_fetch_array($s)) {
                                    echo "
                                    <tr>
                                        <td>$no</td>
                                        <td>$f[title]</td>
                                        <td>".tgl_indo($f['start_date'])." sd ".tgl_indo($f['end_date'])."</td>
                                        <td>$f[kategori_survey_set]</td>
                                        <td>$f[status_survey_set]</td>
                                        <td>
                                        <a class='btn btn-info btn-xs' href='survey_set_edit.php?e=$f[id_survey_set]'>
                                        <i class='fa fa-edit'></i></a>
                                        <a class='btn btn-warning btn-xs' href='survey_set_det.php?det=$f[id_survey_set]'>
                                         &nbsp;<i class='fa fa-info'></i>&nbsp;</a>
                                        </td>
                                    </tr>
                                    ";
                                    $no++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
        </section>
        <!-- /.Left col -->
    </div>
    <!-- /.row (main row) -->

</section>

<?php require_once('bawah.php'); ?>