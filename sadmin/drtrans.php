<?php require_once('atas.php');
// if (isset($_GET["d"])) {
//     $no=1;
//     $d=mysqli_query($kon,"select * from santri where nis='$_GET[d]'");
//     $dd=mysqli_fetch_array($d);
//     unlink('../files/'.$dd['foto']);
//     mysqli_query($kon,"delete from santri where nis='$_GET[d]'");
//     echo "<script type='text/javascript'>alert(\"Berhasil!\");window.location=\"dsantri.php\";</script>";
// }
// elseif(isset($_GET["r"])){
//     $p=md5('santri');
//     $a=mysqli_query($kon,"update nis set
//     password='$p' where nis='$_GET[r]'");
//     echo "<script type='text/javascript'>alert(\"Berhasil!\");window.location=\"dnis.php\";</script>";
// }
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Dashboard
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Santri</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"><a href='drtrans.php'><i class="fa fa-user"></i></a> Daftar Transaksi Santri
                    </h3>
                    <form style='margin-right:5px; margin-top:0px' class='pull-right' action='' method='GET'>
                        <input type="date" name="tgl" id="tgl" <?php if (isset($_GET['tgl'])) {
                                                                    echo "value='$_GET[tgl]'";
                                                                } ?>>
                        <select class='select2' name='j' style='padding:4px'>
                            <?php
                            echo "<option value='all'>- Semua -</option>";
                            echo "<option value='L' ";
                            if (isset($_GET['j']) and $_GET['j'] == "L") {
                                echo "selected";
                            }
                            echo ">Laki-laki</option>";
                            echo "<option value='P'";
                            if (isset($_GET['j']) and $_GET['j'] == "P") {
                                echo "selected";
                            }
                            echo ">Perempuan</option>";
                            ?>
                        </select>
                        <input type="submit" style='margin-top:0px' class='btn btn-success btn-sm' value='Lihat'>
                        <input type="reset" style='margin-top:0px' class='btn btn-success btn-sm' value='Lihat'>
                    </form>
                </div>
                <div class="box-body">
                    <div class="table-responsive" style="border: 0px solid #ddd;">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>NIS</th>
                                    <th>Nama Lengkap</th>
                                    <th>TTL</th>
                                    <th>JKel</th>
                                    <th>Status</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;
                                if (isset($_GET['j'])) {
                                    if ($_GET['j'] == "all") {
                                        $s = mysqli_query($kon, "select * from santri order by nis desc");
                                        if (isset($_GET['tgl'])) {
                                            if (!empty($_GET['tgl'])) {
                                                $jtrans = mysqli_query($kon, "select *, sum(jumlah_trans) as jtrans from trans left join santri on trans.nis=santri.nis left join jtrans on trans.id_jtrans=jtrans.id_jtrans where trans.tgl_trans='$_GET[tgl]' order by id_trans desc");
                                                $jtrans1 = mysqli_fetch_array($jtrans);
                                                $c = "oke";
                                                // $jtrans1['jtrans'];
                                            }
                                        }
                                    } else {
                                        $s = mysqli_query($kon, "select * from santri where jkel='$_GET[j]' order by nis desc");
                                        if (isset($_GET['tgl'])) {
                                            if (!empty($_GET['tgl'])) {
                                                $jtrans = mysqli_query($kon, "select *, sum(jumlah_trans) as jtrans from trans left join santri on trans.nis=santri.nis left join jtrans on trans.id_jtrans=jtrans.id_jtrans where santri.jkel='$_GET[j]' and trans.tgl_trans='$_GET[tgl]' order by id_trans desc");
                                                $jtrans1 = mysqli_fetch_array($jtrans);
                                                $c = "oke";
                                            }
                                        }
                                    }

                                    while ($f = mysqli_fetch_array($s)) {
                                        echo "
                            <tr>
                                <td>$no</td>
                                <td>$f[nis]</td>
                                <td>$f[nama_lengkap]</td>
                                <td>$f[tempat_lhr], " . tgl_indo($f['tgl_lhr']) . "</td>
                                <td>$f[jkel]</td>
                                <td>$f[status_santri]</td>
                                <td>";
                                        if (!empty($c)) {
                                            echo "<a class='btn btn-primary btn-xs' href='prtrans.php?p=$f[nis]&tgl=$_GET[tgl]' target=_blank()>";
                                        } else {
                                            echo "<a class='btn btn-primary btn-xs' href='prtrans.php?p=$f[nis]' target=_blank()>";
                                        }
                                        echo "
                                <i class='fa fa-print'></i></a>
                                </td>
                            </tr>
                            ";
                                        $no++;
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                        <?php
                        if (!empty($c)) {
                            echo "
                            <b>
                            <h4>Total Transaksi per " . tgl_indo($_GET['tgl']) . " :   <u>Rp " . number_format($jtrans1['jtrans'], 2, ',', '.') . "</u> 
                            </h4>
                            </b>
                            <a class='btn btn-primary btn-xs' href='pjtrans.php?tgl=$_GET[tgl]&j=$_GET[j]' target=_blank()>
                            <i class='fa fa-print'></i></a>
                            ";
                        }
                        ?>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
        </section>
        <!-- /.Left col -->
    </div>
    <!-- /.row (main row) -->

</section>
<?php require_once('bawah.php'); ?>