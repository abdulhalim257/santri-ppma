<?php require_once('atas.php');

if (isset($_GET["det"])) {
    $qsurvey_set = mysqli_query($kon, "select * from survey_set where id_survey_set='$_GET[det]'");
    $dqsurvey_set = mysqli_fetch_array($qsurvey_set);
}
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Dashboard
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Detail Survey</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <!-- Left col -->
        <section class="col-lg-3 connectedSortable">
            <!-- About Me Box -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Tentang Survey</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <strong><i class="fa fa-pencil margin-r-5"></i> Judul</strong>
                    <p class="text-muted">
                        <?php echo "$dqsurvey_set[title]"; ?>
                    </p>

                    <strong><i class="fa fa-book margin-r-5"></i> Deskripsi</strong>
                    <p class="text-muted">
                        <?php echo "$dqsurvey_set[description]"; ?>
                    </p>

                    <strong><i class="fa fa-calendar margin-r-5"></i> Waktu</strong>
                    <p>
                        <?php echo "" . tgl_indo($dqsurvey_set['start_date']) . " sd " . tgl_indo($dqsurvey_set['end_date']) . "";?>
                    </p>

                    <strong><i class="fa fa-folder margin-r-5"></i> Kategori</strong>
                    <p>
                    <?php echo "$dqsurvey_set[kategori_survey_set]"; ?>
                    </p>

                    <strong><i class="fa fa-eye margin-r-5"></i> Status</strong>
                    <p>
                    <?php if($dqsurvey_set['status_survey_set']=='Y') echo "Aktif"; else echo "Tidak Aktif"; ?></p>
                    <p>
                    <?php echo "<a class='btn btn-primary' href='survey_set_print.php?s=$dqsurvey_set[id_survey_set]&t=excel' target='_blank()'>&nbsp;<i class='fa fa-print'></i>&nbsp; Print Excel</a>"; ?></p>
                </div>
                <!-- /.box-body -->
            </div>
        </section>
        <section class="col-lg-9 connectedSortable">
            <!-- /.box -->
            <div class="box">
                <div class="box-body">
                    <div class="table-responsive" style="border: 0px solid #ddd;">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Responden</th>
                                    <th>Waktu</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;
                                $s = mysqli_query($kon, "select * from answers where id_survey_set='$dqsurvey_set[id_survey_set]'");
                                while ($f = mysqli_fetch_array($s)) {
                                    echo "
                                        <tr>
                                            <td>$no</td>
                                            <td>$f[respon]</td>
                                            <td>" . tgl_indo(substr($dqsurvey_set['date_created'], 0, 10)) . " " . substr($dqsurvey_set['date_created'], 11, 20) . "</td>
                                            <td>
                                            <a class='btn btn-warning btn-xs' href='survey_set_view.php?det=$f[id_answer]' target='_blank()'>
                                            &nbsp;<i class='fa fa-info'></i>&nbsp;</a>
                                            </td>
                                        </tr>
                                    ";
                                    $no++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
        </section>
        <!-- /.Left col -->
    </div>
    <!-- /.row (main row) -->

</section>

<?php require_once('bawah.php'); ?>