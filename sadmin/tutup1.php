<?php
session_start();
require('konek.php');
// Database configuration
$database_name = "ppma";

// Get connection object and set the charset
$kon->set_charset("utf8");


// Get All Table Names From the Database
$tables = array();
$sql = "SHOW TABLES";
$result = mysqli_query($kon, $sql);

while ($row = mysqli_fetch_row($result)) {
    $tables[] = $row[0];
}

$sqlScript = "
SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;\n";
foreach ($tables as $table) {

    // Prepare SQLscript for creating table structure
    $query = "SHOW CREATE TABLE $table";
    $result = mysqli_query($kon, $query);
    $row = mysqli_fetch_row($result);

    $sqlScript .= "DROP TABLE IF EXISTS " . $row[0] . ";";
    $sqlScript .= "\n\n" . $row[1] . ";\n\n";


    $query = "SELECT * FROM $table";
    $result = mysqli_query($kon, $query);

    $columnCount = mysqli_num_fields($result);

    // Prepare SQLscript for dumping data for each table
    for ($i = 0; $i < $columnCount; $i++) {
        while ($row = mysqli_fetch_row($result)) {
            $sqlScript .= "INSERT INTO $table VALUES(";
            for ($j = 0; $j < $columnCount; $j++) {
                $row[$j] = $row[$j];

                if (isset($row[$j])) {
                    $sqlScript .= '"' . $row[$j] . '"';
                } else {
                    $sqlScript .= '""';
                }
                if ($j < ($columnCount - 1)) {
                    $sqlScript .= ',';
                }
            }
            $sqlScript .= ");\n";
        }
    }

    $sqlScript .= "\n";
}

if (!empty($sqlScript)) {
    // Save the SQL script to a backup file
    $backup_file_name = $database_name . 'peroid' . time() . "-" . date('Y-m-d') . '.sql';
    $fileHandler = fopen($backup_file_name, 'w+');
    $number_of_lines = fwrite($fileHandler, $sqlScript);
    fclose($fileHandler);

    // Download the SQL backup file to the browser
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename=' . basename($backup_file_name));
    header('Content-Transfer-Encoding: binary');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($backup_file_name));
    ob_clean();
    flush();
    readfile($backup_file_name);
    exec('rm ' . $backup_file_name);
}


// Database configuration
$database_name = "tutup";

// Get connection object and set the charset
$kon->set_charset("utf8");

$sqlScript = "
SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;\n";

$sqlScript .= "TRUNCATE tabungan;\n
TRUNCATE trans;\n
TRUNCATE tagihan;\n
";

$s = mysqli_query($kon, "select * from santri left join jenjang on santri.id_jenjang=jenjang.id_jenjang order by nis asc");
$notab = 1;
$notrans = 1;
while ($f = mysqli_fetch_array($s)) {
    $jtab = mysqli_query($kon, "select *, sum(jumlah_tab) as jtab from tabungan where nis='$f[nis]'");
    $jtab1 = mysqli_fetch_array($jtab);
    $hjtab = "Rp " . number_format($jtab1['jtab'], 0, ',', '.');

    $jtag = mysqli_query($kon, "select *, sum(syahriah+kos_makan+sekolah) as jtag from tagihan where nis='$f[nis]'");
    $jtag1 = mysqli_fetch_array($jtag);

    $jtrans = mysqli_query($kon, "select *, sum(jumlah_trans) as jtrans from trans where nis='$f[nis]'");
    $jtrans1 = mysqli_fetch_array($jtrans);

    $kredit = $jtag1['jtag'] + $jtrans1['jtrans'];

    $saldo = $jtab1['jtab'] - $kredit;
    $tgl_tab = date('Y-m-d');
    $create_by = $_SESSION['id_admin'];
    $create_at = date("Y-m-d h:i:s");
    $edit_by = '';
    $edit_at = '';


    $tab = date('Ymd');
    if ($saldo > 0) {
        $id_tabungan = $tab . '0000' + $notab;

        $sqlScript .= " insert into tabungan values('$id_tabungan','$f[nis]','$saldo','$tgl_tab','$create_by','$create_at','$edit_by','$edit_at');\n";
        $notab++;
    } elseif ($saldo < 0) {
        $hsaldo = $saldo * (-1);
        $id_trans = $tab . '0000' + $notrans;
        $sqlScript .= " insert into trans values('$id_trans','$f[nis]','$hsaldo','$tgl_tab','1','Saldo awal periode sebelumnya','$create_by','$create_at','$edit_by','$edit_at');\n";
        $notrans++;
    }
}
if (!empty($sqlScript)) {
    // Save the SQL script to a backup file
    $backup_file_name = $database_name . '.sql';
    $fileHandler = fopen($backup_file_name, 'w+');
    $number_of_lines = fwrite($fileHandler, $sqlScript);
    fclose($fileHandler);

    // Download the SQL backup file to the browser
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename=' . basename($backup_file_name));
    header('Content-Transfer-Encoding: binary');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($backup_file_name));
    ob_clean();
    flush();
    readfile($backup_file_name);
    exec('rm ' . $backup_file_name);
}

// Validating SQL file type by extensions
if (!in_array(strtolower(pathinfo('tutup.sql', PATHINFO_EXTENSION)), array(
    "sql"
))) {
    $response = array(
        "type" => "error",
        "message" => "Invalid File Type"
    );
} else {
    $response = restoreMysqlDB('tutup.sql', $kon);
    echo "<script type='text/javascript'>alert(\"Berhasil! Silahkan Login kembali.\");window.location=\"logout.php\";</script>";
}
function restoreMysqlDB($filePath, $kon)
{
    $sql = '';
    $error = '';

    if (file_exists($filePath)) {
        $lines = file($filePath);

        foreach ($lines as $line) {

            // Ignoring comments from the SQL script
            if (substr($line, 0, 2) == '--' || $line == '') {
                continue;
            }

            $sql .= $line;

            if (substr(trim($line), -1, 1) == ';') {
                $result = mysqli_query($kon, $sql);
                if (!$result) {
                    $error .= mysqli_error($kon) . "\n";
                }
                $sql = '';
            }
        } // end foreach

        if ($error) {
            $response = array(
                "type" => "error",
                "message" => $error
            );
        } else {
            $response = array(
                "type" => "success",
                "message" => "Database Restore Completed Successfully."
            );
        }
        exec('rm ' . $filePath);
    } // end if file exists

    // return $response;
}
unlink('tutup.sql');

echo "<script type='text/javascript'>alert(\"Berhasil! Silahkan Login kembali.\");window.location=\"logout.php\";</script>";
