<?php require('konek.php');

if (isset($_POST['print'])) {

    $sql = "SELECT * FROM santri";
    $s = mysqli_query($kon, $sql);

    $jml = mysqli_num_rows($s);
    $n = $jml;
    // include file qrlib.php
    include "../phpqrcode/qrlib.php";

    //Nama Folder file QR Code kita nantinya akan disimpan
    $tempdir = "../qr/";

    //jika folder belum ada, buat folder 
    if (!file_exists($tempdir)) {
        mkdir($tempdir);
    }
    for ($i = 0; $i <= $n; $i++) {
        if (isset($_POST['pilih' . $i])) {

            $nis = $_POST['pilih' . $i];
            $nist = mysqli_query($kon, "SELECT * FROM santri where nis='$nis'");
            $nistt = mysqli_fetch_array($nist);


            #parameter inputan
            $isi_teks = $nistt['nis'];
            $namafile = $nistt['nis'] . ".png";
            $quality = 'H'; //ada 4 pilihan, L (Low), M(Medium), Q(Good), H(High)
            $ukuran = 10; //batasan 1 paling kecil, 10 paling besar
            $padding = 2;

            QRCode::png($isi_teks, $tempdir . $namafile, $quality, $ukuran, $padding);

            echo "
              <table width='376' border=0 style='
              background-image:url(../cdr/kartu.png);
              background-size: 8.5cm 5.5cm;
              width:8.5cm;
              height:5.5cm;
              
              '>
                  <tr>
                      <th width='5' rowspan=7  '></th>
                      <th width='79' height='17'></th>
                      <th width='133'></th>
                      <th width='81'></th>
                  </tr>
                  <tr>
                    <th rowspan=3 style='width:0.1cm;height:0.1cm;'> 
                        <img src='../files/$nistt[foto]' width='79' height='105' style='width:80px;align:top;border:2px solid white;border-radius:5px;'></th>
                      <th height='45'></th>
                      <th></th>
                  </tr>
                  <tr>
                      <th height='35'>$nistt[nama_lengkap]</th>
                      <th rowspan=2><img src='../qr/$nistt[nis].png' width='65' height='65'></th>
                  </tr>
                  <tr>
                      <th height='21' style='font-size:8px'>NIS : $nistt[nis]</th>
                  </tr>
                  <tr>
                      <th height='22' style='font-size:8px'>Alamat : </th>
                      <th align='left' style='font-size:8px'><p>$nistt[alamat]</p></th>
                      <th></th>
                  </tr>
                  <tr>
                      <th height='14' style='font-size:8px; align=left'>TTL : </th>
                      <th align='left' style='font-size:8px;align=left'>$nistt[tempat_lhr], " . tgl_indo($nistt['tgl_lhr']) . "</th>
                      <th></th>
                  </tr>
                  <tr>
                      <th></th>
                      <th></th>
                      <th></th>
                  </tr>
              </table>
              <br>
              ";
        }
    }
}
