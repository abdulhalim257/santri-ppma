<?php require_once('atas.php');
$sa = mysqli_query($kon, "select * from tagihan left join santri on tagihan.nis=santri.nis where id_pembayaran='$_GET[t]' and status_tagihan='Belum Lunas'");
$f = mysqli_fetch_array($sa);
$p = mysqli_query($kon, "select sum(nominal) as tp from pembayaran where id_pembayaran='$_GET[t]'");
$pp = mysqli_fetch_array($p);
$t = $f['syahriah'] - $pp['tp'];
$k = "Rp " . number_format($t, 2, ',', '.');
if ($f) {
} else {
    echo "<script type='text/javascript'>alert(\"Gagal!\");location.href='index.php';</script>";
}
if (isset($_POST["save"])) {
    $d = date("Y-m-d h:i:s");
    if ($_POST['nominal'] > $f['syahriah']) {
        echo "<script type='text/javascript'>alert(\"Gagal!\");history.back();</script>";
    } else {
        $sa = mysqli_query($kon, "select * from tagihan left join santri on tagihan.nis=santri.nis where id_pembayaran='$_POST[id_pembayaran]'");
        $f = mysqli_fetch_array($sa);
        $p = mysqli_query($kon, "select sum(nominal) as tp from pembayaran where id_pembayaran='$_POST[id_pembayaran]'");
        $pp = mysqli_fetch_array($p);
        $ttt = $_POST['nominal'] + $pp['tp'];
        if ($f['syahriah'] == $ttt) {
            mysqli_query($kon, "update tagihan set status_tagihan='Lunas' where id_pembayaran='$_POST[id_pembayaran]'");
        }
        $a = mysqli_query($kon, "insert into pembayaran values(
                '0',
                '$_POST[id_pembayaran]',
                '$_POST[nominal]',
                '$d',
                '$_SESSION[id_admin]'
                )");
        if ($a) {
            echo "<script type='text/javascript'>alert(\"Berhasil!\");window.location=\"dpem.php\";</script>";
        } else {
            echo "<script type='text/javascript'>alert(\"Gagal!\");history.back();</script>";
        }
    }
}
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Dashboard
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Pembayaran</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <!-- Left col -->
        <section class="col-lg-7 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-cc-mastercard"></i> Tambah Pembayaran</h3>
                </div>
                <form action="" method="POST" enctype="multipart/form-data">
                    <div class="box-body">
                        <div class="form-group">
                            <label>ID Pembayaran</label>
                            <input type="text" name='id_pembayaran' class="form-control" placeholder="Masukkan ID Pembayaran" value='<?php echo $f['id_pembayaran']; ?>' readonly>
                        </div>
                        <div class="form-group">
                            <label>NIS</label>
                            <input type="text" name='nis' class="form-control" value='<?php echo $f['nis']; ?>' readonly>
                        </div>
                        <div class="form-group">
                            <label>Nama Lengkap</label>
                            <input type="text" name='nis' class="form-control" value='<?php echo $f['nama_lengkap']; ?>' readonly>
                        </div>
                        <div class="form-group">
                            <label>Syahriah</label>
                            <input type="text" name='syahriah' class="form-control" value='<?php echo "$k"; ?>' readonly>
                        </div>
                        <div class="form-group">
                            <label>Nominal</label>
                            <input type="number" min='0' max='<?php echo $t; ?>' name='nominal' class="form-control" placeholder="Masukkan Nominal" required>
                        </div>
                        <!-- <div class="form-group">
                        <label>Tanggal</label>
                        <input type="text" name='tgl_trans' class="form-control pull-right" id="datepicker" placeholder="Masukkan Tanggal" required>
                    </div> -->
                    </div>
                    <div class="box-footer">
                        <input type="submit" name='save' class="btn btn-primary" value="Submit">
                        <input type="Reset" class="btn btn-danger" value="Reset">
                        <a href="dpem.php"><button type="button" class="btn btn-info">Lihat</button></a>
                    </div>
                </form>
                <!-- /.box-body -->
            </div>
        </section>
        <!-- /.Left col -->
    </div>
    <!-- /.row (main row) -->

</section>
<?php require_once('bawah.php'); ?>