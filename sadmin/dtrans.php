<?php require_once('atas.php');
if (isset($_GET["d"])) {
    $no = 1;
    $d = mysqli_query($kon, "select * from trans where id_trans='$_GET[d]'");
    $dd = mysqli_fetch_array($d);
    // unlink('../files/'.$dd['foto']);
    mysqli_query($kon, "delete from trans where id_trans='$_GET[d]'");
    echo "<script type='text/javascript'>alert(\"Berhasil!\");window.location=\"dtab.php\";</script>";
}
// elseif(isset($_GET["r"])){
//     $p=md5('trans');
//     $a=mysqli_query($kon,"update nis set
//     password='$p' where nis='$_GET[r]'");
//     echo "<script type='text/javascript'>alert(\"Berhasil!\");window.location=\"dnis.php\";</script>";
// }
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Dashboard
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Transaksi</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"><a href='ttrans.php'><i class="fa fa-user"></i></a> | <a href='c.php?id=trans'><i class="fa fa-qrcode"></i></a> Daftar Transaksi</h3>
                    <?php if ($_SESSION['level'] == 'sadmin') { ?>
                        <form style='margin-right:5px; margin-top:0px' class='pull-right' action='' method='GET'>
                            <select class='select2' name='j' style='padding:4px'>
                                <?php
                                echo "<option value='all'>- Semua -</option>";
                                echo "<option value='L' ";
                                if (isset($_GET['j']) and $_GET['j'] == "L") {
                                    echo "selected";
                                }
                                echo ">Laki-laki</option>";
                                echo "<option value='P'";
                                if (isset($_GET['j']) and $_GET['j'] == "P") {
                                    echo "selected";
                                }
                                echo ">Perempuan</option>";
                                ?>
                            </select>
                            <input type="submit" style='margin-top:0px' class='btn btn-success btn-sm' value='Lihat'>
                        </form>
                    <?php } ?>
                </div>
                <div class="box-body">
                    <div class="table-responsive" style="border: 0px solid #ddd;">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Id Trans</th>
                                    <th>NIS</th>
                                    <th>Nama Lengkap</th>
                                    <th>Jumlah</th>
                                    <th>Tanggal</th>
                                    <th>Jenis Trans</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;
                                if ($_SESSION['level'] == 'sadmin') {
                                    if (isset($_GET['j']) and $_GET['j'] == "all" or empty($_GET['j'])) {
                                        $s = mysqli_query($kon, "select * from trans left join santri on trans.nis=santri.nis left join jtrans on trans.id_jtrans=jtrans.id_jtrans order by id_trans desc");
                                    } else {
                                        $s = mysqli_query($kon, "select * from trans left join santri on trans.nis=santri.nis left join jtrans on trans.id_jtrans=jtrans.id_jtrans  where santri.jkel='$_GET[j]' order by id_trans desc");
                                    }
                                } else {
                                    $s = mysqli_query($kon, "select * from trans left join santri on trans.nis=santri.nis left join jtrans on trans.id_jtrans=jtrans.id_jtrans  $_SESSION[where] order by id_trans desc");
                                }


                                while ($f = mysqli_fetch_array($s)) {
                                    $hasil_rupiah = "Rp " . number_format($f['jumlah_trans'], 2, ',', '.');
                                    echo "
                            <tr>
                                <td>$no</td>
                                <td><b>$f[id_trans]</b></td>
                                <td>$f[nis]</td>
                                <td>$f[nama_lengkap]</td>
                                <td>$hasil_rupiah</td>
                                <td>" . tgl_indo($f['tgl_trans']) . "</td>
                                <td>$f[ket_jtrans]</td>
                                <td>
                                <a class='btn btn-info btn-xs' href='etrans.php?e=$f[id_trans]'>
                                <i class='fa fa-edit'></i></a>
                                <a class='btn btn-danger btn-xs' href='?d=$f[id_trans]' onclick=\"return confirm('Yakin hapus?')\">
                                <i class='fa fa-remove'></i></a>
                                <a class='btn btn-primary btn-xs' href='ptrans.php?p=$f[id_trans]' target=_blank()>
                                <i class='fa fa-print'></i></a>
                                </td>
                            </tr>
                            ";
                                    $no++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
        </section>
        <!-- /.Left col -->
    </div>
    <!-- /.row (main row) -->

</section>
<?php require_once('bawah.php'); ?>