<?php require_once('atas.php');
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Dashboard
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Rekap</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Rekap</h3>
                    <?php if ($_SESSION['level'] == 'sadmin') { ?>
                        <form style='margin-right:5px; margin-top:0px' class='pull-right' action='' method='GET'>
                            <select class='select2' name='j' style='padding:4px'>
                                <?php
                                echo "<option value='all'>- Semua -</option>";
                                echo "<option value='L' ";
                                if (isset($_GET['j']) and $_GET['j'] == "L") {
                                    echo "selected";
                                }
                                echo ">Laki-laki</option>";
                                echo "<option value='P'";
                                if (isset($_GET['j']) and $_GET['j'] == "P") {
                                    echo "selected";
                                }
                                echo ">Perempuan</option>";
                                ?>
                            </select>
                            <input type="submit" style='margin-top:0px' class='btn btn-success btn-sm' value='Lihat'>
                        </form>
                    <?php } ?>
                </div>
                <div class="box-body">
                    <div class="table-responsive" style="border: 0px solid #ddd;">
                        <table id="example3" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>NIS</th>
                                    <th>Nama Lengkap</th>
                                    <th>Debit</th>
                                    <th>Kredit</th>
                                    <th>Saldo</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;
                                if ($_SESSION['level'] == 'sadmin') {
                                    if (isset($_GET['j']) and $_GET['j'] == "all" or empty($_GET['j'])) {
                                        $s = mysqli_query($kon, "select * from santri left join jenjang on santri.id_jenjang=jenjang.id_jenjang order by nis desc");
                                    } else {
                                        $s = mysqli_query($kon, "select * from santri left join jenjang on santri.id_jenjang=jenjang.id_jenjang where santri.jkel='$_GET[j]' order by nis desc");
                                    }
                                } else {
                                    $s = mysqli_query($kon, "select * from santri left join jenjang on santri.id_jenjang=jenjang.id_jenjang $_SESSION[where] order by nis desc");
                                }


                                while ($f = mysqli_fetch_array($s)) {

                                    $jtab = mysqli_query($kon, "select *, sum(jumlah_tab) as jtab from tabungan where nis='$f[nis]'");
                                    $jtab1 = mysqli_fetch_array($jtab);
                                    $hjtab = "Rp " . number_format($jtab1['jtab'], 0, ',', '.');

                                    $jtag = mysqli_query($kon, "select *, sum(syahriah+kos_makan+sekolah) as jtag from tagihan where nis='$f[nis]'");
                                    $jtag1 = mysqli_fetch_array($jtag);

                                    $jtrans = mysqli_query($kon, "select *, sum(jumlah_trans) as jtrans from trans where nis='$f[nis]'");
                                    $jtrans1 = mysqli_fetch_array($jtrans);

                                    $kredit = $jtag1['jtag'] + $jtrans1['jtrans'];
                                    $hjtag = "Rp " . number_format($kredit, 0, ',', '.');

                                    $saldo = $jtab1['jtab'] - $kredit;
                                    $hsaldo = "Rp " . number_format($saldo, 0, ',', '.');
                                    echo "
                                <tr>
                                    <td>$no</td>
                                    <td><a href='rekapdet.php?n=$f[nis]'>$f[nis]</a></td>
                                    <td>$f[nama_lengkap]</td>
                                    <td>$hjtab</td>
                                    <td>$hjtag</td>
                                    <td><span class='badge badge-danger'";
                                    if ($saldo > 0) {
                                        echo "style='background-color:white;color:black'";
                                    } elseif ($saldo > -500000) {
                                        echo "style='background-color:green'";
                                    } elseif ($saldo > -1000000) {
                                        echo "style='background-color:#ffa300'";
                                    } else {
                                        echo "style='background-color:#ff0b0b'";
                                    }
                                    echo ">$hsaldo</span></td>
                                </tr>
                                ";
                                    if ($no > 2) {
                                        # code...
                                        $total += $saldo;
                                    } else {
                                        $total = $saldo;
                                    }
                                    $no++;
                                }
                                ?>
                                <tr>
                                    <td colspan=4></td>
                                    <td>
                                        <h4><b>Total</b></h4>
                                    </td>
                                    <td>
                                        <h4><b><?php echo "Rp " . number_format($total, 0, ',', '.'); ?></b></h4>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
        </section>
        <!-- /.Left col -->
    </div>
    <!-- /.row (main row) -->

</section>
<?php require_once('bawah.php'); ?>