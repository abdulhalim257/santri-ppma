<?php require_once('atas.php');
$s = mysqli_query($kon, "select * from trans left join santri on trans.nis=santri.nis left join jtrans on trans.id_jtrans=jtrans.id_jtrans where trans.id_trans='$_GET[e]' order by id_trans desc");
$f = mysqli_fetch_array($s);
if (isset($_POST["save"])) {
    $nis = $_POST['nis'];
    $jt = mysqli_query($kon, "select *, sum(jumlah_tab) as jtab from tabungan where nis='$nis'");
    $jt1 = mysqli_fetch_array($jt);
    $jtr = mysqli_query($kon, "select *, sum(jumlah_trans) as jtrans from trans where nis='$nis' and id_trans <> '$_POST[id_trans]'");
    $jtr1 = mysqli_fetch_array($jtr);
    $saldo = $jt1['jtab'] - $jtr1['jtrans'];
    $jumlah_trans = $_POST['jumlah_trans'];
    $cek = $saldo - $jumlah_trans;
    $tgl_lh = date('Y-m-d', strtotime($_POST['tgl_trans']));
    $tgl_trans = $tgl_lh;
    $id_jtrans = $_POST['id_jtrans'];
    $ket_trans = $_POST['ket_trans'];
    $create_by = $f['create_by'];
    $create_at = $f['create_at'];
    $edit_by = $_SESSION['id_admin'];
    $edit_at = date("Y-m-d h:i:s");
    $id_trans = $_POST['id_trans'];
    // $tab=date('Ymd');
    // $ni=mysqli_query($kon,"select * from trans where id_trans LIKE '%$tab%' order by id_trans desc limit 1");
    // $nit=mysqli_fetch_array($ni);
    // $jni=mysqli_num_rows($ni);
    // if ($jni>0) {
    //     $id_trans=$nit['id_trans']+1;
    // } else {
    //     $id_trans=$tab.'0001';
    // }
    //  echo$cek;
    if ($cek < 0) {
        echo "<script type='text/javascript'>alert(\"Saldo kurang!\");history.back();</script>";
    } elseif ($jumlah_trans > 50000) {
        echo "<script type='text/javascript'>alert(\"Nominal terlalu banyak!\");history.back();</script>";
    } else {
        mysqli_query($kon, "delete from trans where id_trans=$id_trans");
        $a = mysqli_query($kon, "insert into trans values(
                '$id_trans',
                '$nis',
                '$jumlah_trans',
                '$tgl_trans',
                '$id_jtrans',
                '$ket_trans',
                '$create_by',
                '$create_at',
                '$edit_by',
                '$edit_at'
                )");
        if ($a) {
            echo "<script type='text/javascript'>alert(\"Berhasil!\");window.location=\"dtrans.php\";</script>";
        } else {
            echo "<script type='text/javascript'>alert(\"Gagal!\");history.back();</script>";
        }
    }
}
$d = date('m/d/Y', strtotime($f['tgl_trans']));
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Dashboard
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Transaksi</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <!-- Left col -->
        <section class="col-lg-7 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-credit-card"></i> Edit Transaksi</h3>
                </div>
                <form action="" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name='id_trans' class="form-control" value='<?php echo $f['id_trans']; ?>' required>
                    <div class="box-body">
                        <div class="form-group">
                            <label>NIS</label>
                            <select name='nis' class="form-control select2" style="width: 100%;" required>
                                <option selected="selected" value="">Pilihan</option>
                                <?php
                                $s = mysqli_query($kon, "select * from santri where status_santri='Aktif'");
                                while ($ss = mysqli_fetch_array($s)) {
                                    if ($f['nis'] == $ss['nis']) {
                                        echo "<option value='$ss[nis]' selected>$ss[nama_lengkap] ($ss[nis])</option>";
                                    } else {
                                        echo "<option value='$ss[nis]'>$ss[nama_lengkap] ($ss[nis])</option>";
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Jumlah</label>
                            <input type="number" min=0 max=500000 name='jumlah_trans' class="form-control" placeholder="Masukkan Jumlah" value='<?php echo $f['jumlah_trans']; ?>' required>
                        </div>
                        <div class="form-group">
                            <label>Tanggal</label>
                            <input type="text" name='tgl_trans' class="form-control pull-right" id="datepicker" placeholder="Masukkan Tanggal" value='<?php echo $d; ?>' required>
                        </div>
                        <div class="form-group">
                            <label>Jenis Transaksi</label>
                            <select name='id_jtrans' class="form-control select2" style="width: 100%;" required>
                                <option selected="selected" value="">Pilihan</option>
                                <?php
                                $s = mysqli_query($kon, "select * from jtrans where status_jtrans='Aktif'");
                                while ($ss = mysqli_fetch_array($s)) {
                                    if ($f['id_jtrans'] == $ss['id_jtrans']) {
                                        echo "<option value='$ss[id_jtrans]' selected>$ss[ket_jtrans]</option>";
                                    } else {
                                        echo "<option value='$ss[id_jtrans]'>$ss[ket_jtrans]</option>";
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Keterangan</label>
                            <textarea name="ket_trans" class="form-control" placeholder="Masukkan Keterangan"><?php echo $f['ket_trans']; ?></textarea>
                        </div>
                    </div>
                    <div class="box-footer">
                        <input type="submit" name='save' class="btn btn-primary" value="Submit">
                        <input type="Reset" class="btn btn-danger" value="Reset">
                        <a href="dtrans.php"><button type="button" class="btn btn-info">Lihat</button></a>
                    </div>
                </form>
                <!-- /.box-body -->
            </div>
        </section>
        <!-- /.Left col -->
    </div>
    <!-- /.row (main row) -->

</section>
<?php require_once('bawah.php'); ?>