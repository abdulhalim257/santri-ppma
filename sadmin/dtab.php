<?php require_once('atas.php');
if (isset($_GET["d"])) {
    $no = 1;
    $d = mysqli_query($kon, "select * from tabungan where id_tabungan='$_GET[d]'");
    $dd = mysqli_fetch_array($d);
    // unlink('../files/'.$dd['foto']);
    mysqli_query($kon, "delete from tabungan where id_tabungan='$_GET[d]'");
    echo "<script type='text/javascript'>alert(\"Berhasil!\");window.location=\"dtab.php\";</script>";
}
// elseif(isset($_GET["r"])){
//     $p=md5('Tabungan');
//     $a=mysqli_query($kon,"update nis set
//     password='$p' where nis='$_GET[r]'");
//     echo "<script type='text/javascript'>alert(\"Berhasil!\");window.location=\"dnis.php\";</script>";
// }
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Dashboard
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Tabungan</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"><a href='ttab.php'><i class="fa fa-user"></i></a> | <a href='c.php?id=tab'><i class="fa fa-qrcode"></i></a> Daftar Tabungan</h3>
                    <?php if ($_SESSION['level'] == 'sadmin') { ?>
                        <form style='margin-right:5px; margin-top:0px' class='pull-right' action='' method='GET'>
                            <select class='select2' name='j' style='padding:4px'>
                                <?php
                                echo "<option value='all'>- Semua -</option>";
                                echo "<option value='L' ";
                                if (isset($_GET['j']) and $_GET['j'] == "L") {
                                    echo "selected";
                                }
                                echo ">Laki-laki</option>";
                                echo "<option value='P'";
                                if (isset($_GET['j']) and $_GET['j'] == "P") {
                                    echo "selected";
                                }
                                echo ">Perempuan</option>";
                                ?>
                            </select>
                            <input type="submit" style='margin-top:0px' class='btn btn-success btn-sm' value='Lihat'>
                        </form>
                    <?php } ?>
                </div>
                <div class="box-body">
                    <div class="table-responsive" style="border: 0px solid #ddd;">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Id Tabungan</th>
                                    <th>NIS</th>
                                    <th>Nama Lengkap</th>
                                    <th>Jumlah</th>
                                    <th>Tanggal</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;
                                if ($_SESSION['level'] == 'sadmin') {
                                    if (isset($_GET['j']) and $_GET['j'] == "all" or empty($_GET['j'])) {
                                        $s = mysqli_query($kon, "select * from tabungan left join santri on tabungan.nis=santri.nis order by id_tabungan desc");
                                    } else {
                                        $s = mysqli_query($kon, "select * from tabungan left join santri on tabungan.nis=santri.nis where santri.jkel='$_GET[j]' order by id_tabungan desc");
                                    }
                                } else {
                                    $s = mysqli_query($kon, "select * from tabungan left join santri on tabungan.nis=santri.nis $_SESSION[where] order by id_tabungan desc");
                                }


                                while ($f = mysqli_fetch_array($s)) {
                                    $hasil_rupiah = "Rp " . number_format($f['jumlah_tab'], 2, ',', '.');
                                    echo "
                                <tr>
                                    <td>$no</td>
                                    <td>$f[id_tabungan]</td>
                                    <td>$f[nis]</td>
                                    <td>$f[nama_lengkap]</td>
                                    <td>$hasil_rupiah</td>
                                    <td>" . tgl_indo($f['tgl_tab']) . "</td>
                                    <td>
                                    <a class='btn btn-info btn-xs' href='etab.php?e=$f[id_tabungan]'>
                                    <i class='fa fa-edit'></i></a>
                                    <a class='btn btn-danger btn-xs' href='?d=$f[id_tabungan]' onclick=\"return confirm('Yakin hapus?')\">
                                    <i class='fa fa-remove'></i></a>
                                    <a class='btn btn-primary btn-xs' href='ptab.php?p=$f[id_tabungan]' target=_blank()>
                                    <i class='fa fa-print'></i></a>
                                    </td>
                                </tr>
                                ";
                                    $no++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
        </section>
        <!-- /.Left col -->
    </div>
    <!-- /.row (main row) -->

</section>
<?php require_once('bawah.php'); ?>