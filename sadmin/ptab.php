<?php require('konek.php');
$s = mysqli_query($kon, "select * from tabungan left join admin on tabungan.create_by=admin.id_admin where id_tabungan='$_GET[p]'");
$ss = mysqli_fetch_array($s);

$sa = mysqli_query($kon, "select *,sum(jumlah_tab) as jtab from tabungan t left join santri s on t.nis=s.nis where t.nis='$ss[nis]' and t.id_tabungan < '$ss[id_tabungan]'");
$ssa = mysqli_fetch_array($sa);
$saldo = $ss['jumlah_tab'] + $ssa['jtab'];

$jt = mysqli_query($kon, "select sum(jumlah_trans) as jtrans from trans where nis='$ss[nis]' and tgl_trans <= '$ss[tgl_tab]'");
$jtt = mysqli_fetch_array($jt);
$jtrans = $jtt['jtrans'];
$tsa = $saldo - $jtt['jtrans'];
// <body onload=window.print()>
echo "
    <body>
    <table>
        <tr>
            <td COLSPAN=3 style='border-bottom:1px solid;TEXT-ALIGN:center'>
                <h4 style='margin-block-start:0px;margin-block-end:0px;'>Pondok Pesantren Manba'ul Anwar</h4>
                <h5 style='margin-block-start:0px;margin-block-end:0px;'>Jl. Dieng Km. 05 Krasak Mojotengah</h5>
            </td>
        </tr>
        <tr>
            <td>ID Tabungan</td>
            <td>:</td>
            <td>$ss[id_tabungan]</td>
        </tr>
        <tr>
            <td>NIS</td>
            <td>:</td>
            <td>$ssa[nis]</td>
        </tr>
        <tr>
            <td>Nama</td>
            <td>:</td>
            <td>$ssa[nama_lengkap]</td>
        </tr>
        <tr>
            <td style='border-bottom:1px solid'>Tanggal</td>
            <td style='border-bottom:1px solid'>:</td>
            <td style='border-bottom:1px solid'>" . tgl_indo($ss['tgl_tab']) . "</td>
        </tr>
        <tr>
            <td>Saldo Awal</td>
            <td> : </td>
            <td> Rp " . number_format($ssa['jtab'], 2, ',', '.') . "</td>
        </tr>
        <tr>
            <td>Setor Terakhir</td>
            <td> : </td>
            <td> Rp " . number_format($ss['jumlah_tab'], 2, ',', '.') . "</td>
        </tr>
        <tr>
            <td style='border-bottom:1px solid'>Saldo Akhir</td>
            <td style='border-bottom:1px solid'> : </td>
            <td style='border-bottom:1px solid'> Rp " . number_format($saldo, 2, ',', '.') . "</td>
        </tr>
        <tr>
            <td style='border-bottom:1px solid'>Jumlah Pengeluaran</td>
            <td style='border-bottom:1px solid'> : </td>
            <td style='border-bottom:1px solid'> Rp " . number_format($jtrans, 2, ',', '.') . "</td>
        </tr>
        <tr>
            <td style='border-bottom:1px solid'>Total Saldo</td>
            <td style='border-bottom:1px solid'> : </td>
            <td style='border-bottom:1px solid'> Rp " . number_format($tsa, 2, ',', '.') . "</td>
        </tr>
        <tr>
            <td style='height:50px'></td>
            <td></td>
            <td align=center>Wonosobo, " . tgl_indo(date('Y-m-d')) . "<br>Admin</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td align=center>$ss[username]</td>
        </tr>
    </table>
    <h6>Nb. * Nota ini harap disimpan, dan jangan sampai hilang!.</h6>
    ";
