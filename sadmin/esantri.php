<?php require_once('atas.php');
$s = mysqli_query($kon, "select * from santri where nis='$_GET[e]'");
$f = mysqli_fetch_array($s);
if (isset($_POST["save"])) {
    $nama_lengkap = $_POST['nama_lengkap'];
    $tempat_lhr = $_POST['tempat_lhr'];
    $tgl_lh = date('Y-m-d', strtotime($_POST['tgl_lhr']));
    $tgl_lhr = $tgl_lh;
    $jke = $_POST['jkel'];
    $alamat = $_POST['alamat'];
    $nm_ayah = $_POST['nm_ayah'];
    $nm_ibu = $_POST['nm_ibu'];
    $no_telp_wali = $_POST['no_telp_wali'];
    // $foto=$_POST['foto'];
    $edit_by = $_SESSION['id_admin'];
    $edit_waktu = date("Y-m-d h:i:s");
    $status_santri = $_POST['status_santri'];
    $id_jenjang = $_POST['id_jenjang'];
    // echo "$ang<br>$nis";

    $file_name       = $_FILES['file']['name'];
    $explode         = explode('.', $file_name);
    $file_ext        = $explode[count($explode) - 1];
    $file_size       = $_FILES['file']['size'];
    $file_tmp        = $_FILES['file']['tmp_name'];

    $maxsize = 1024 * 1024; // maksimal 200 KB (1KB = 1024 Byte)
    $valid_ext = array('jpg', 'jpeg', 'png');
    // echo $tgl_lhr;
    if (empty($file_name)) {
        $a = mysqli_query($kon, "update santri set
                nama_lengkap='$nama_lengkap',
                tempat_lhr='$tempat_lhr',
                tgl_lhr='$tgl_lhr',
                jkel='$jke',
                alamat='$alamat',
                nm_ayah='$nm_ayah',
                nm_ibu='$nm_ibu',
                no_telp_wali='$no_telp_wali',
                edit_by='$edit_by',
                edit_waktu='$edit_waktu',
                status_santri='$status_santri',
                id_jenjang='$id_jenjang'
                where nis='$_POST[nis]'");
        if ($a) {
            echo "<script type='text/javascript'>alert(\"Berhasil!\");window.location=\"dsantri.php\";</script>";
        } else {
            echo "<script type='text/javascript'>alert(\"Gagal!\");history.back();</script>";
        }
    } else {
        if (in_array($file_ext, $valid_ext) === true) {
            if ($file_size < 10440700) {
                $t = time() . '_' . $file_name;
                $lokasi = '../files/' . $t;
                $a = mysqli_query($kon, "update santri set
                    nama_lengkap='$nama_lengkap',
                    tempat_lhr='$tempat_lhr',
                    tgl_lhr='$tgl_lhr',
                    jkel='$jke',
                    alamat='$alamat',
                    nm_ayah='$nm_ayah',
                    nm_ibu='$nm_ibu',
                    no_telp_wali='$no_telp_wali',
                    foto='$t',
                    edit_by='$edit_by',
                    edit_waktu='$edit_waktu',
                    status_santri='$status_santri',
                    id_jenjang='$id_jenjang'
                    where nis='$_POST[nis]'");
                unlink('../files/' . $f['foto']);
                if ($a) {
                    move_uploaded_file($file_tmp, $lokasi);
                    echo "<script type='text/javascript'>alert(\"Berhasil!\");window.location=\"dsantri.php\";</script>";
                } else {
                    echo "<script type='text/javascript'>alert(\"Data sudah ada!\");history.back();</script>";
                }
            } else {
                echo "<script type='text/javascript'>alert(\"Gagal upload foto!\");history.back();</script>";
            }
        } else {
            echo "<script type='text/javascript'>alert(\"Gagal upload foto!\");history.back();</script>";
        }
        # code...
    }
}
$d = date('m/d/Y', strtotime($f['tgl_lhr']));
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Dashboard
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Santri</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <!-- Left col -->
        <section class="col-lg-7 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-user"></i> Edit Santri</h3>
                </div>
                <form action="" method="POST" enctype="multipart/form-data">
                    <div class="box-body">
                        <div class="form-group">
                            <label>NIS</label>
                            <input type="text" name='nis' class="form-control" value='<?php echo $f['nis']; ?>' readonly>
                        </div>
                        <div class="form-group">
                            <label>Nama Lengkap</label>
                            <input type="text" name='nama_lengkap' class="form-control" placeholder="Masukkan Nama Lengkap" value='<?php echo $f['nama_lengkap']; ?>' required>
                        </div>
                        <div class="form-group">
                            <label>Tempat Lahir</label>
                            <input type="text" name='tempat_lhr' class="form-control" placeholder="Masukkan Tempat Lahir" value='<?php echo $f['tempat_lhr']; ?>' required>
                        </div>
                        <div class="form-group">
                            <label>Tanggal Lahir</label>
                            <input type="text" name='tgl_lhr' class="form-control pull-right" id="datepicker" value='<?php echo $d; ?>' required>
                        </div>
                        <div class="form-group">
                            <label>Jenis Kelamin</label><br>
                            <input type="radio" name="jkel" class="flat-red" value="L" <?php if ($f['jkel'] == 'L') {
                                                                                            echo "checked";
                                                                                        } ?>> Laki-laki
                            <input type="radio" name="jkel" class="flat-red" value="P" <?php if ($f['jkel'] == 'P') {
                                                                                            echo "checked";
                                                                                        } ?>> Perempuan
                        </div>
                        <div class="form-group">
                            <label>Alamat</label>
                            <input type="text" name='alamat' class="form-control" placeholder="Masukkan Alamat" value='<?php echo $f['alamat']; ?>' required>
                        </div>
                        <div class="form-group">
                            <label>Nama Ayah</label>
                            <input type="text" name='nm_ayah' class="form-control" placeholder="Masukkan Nama Ayah" value='<?php echo $f['nm_ayah']; ?>' required>
                        </div>
                        <div class="form-group">
                            <label>Nama Ibu</label>
                            <input type="text" name='nm_ibu' class="form-control" placeholder="Masukkan Nama Ibu" value='<?php echo $f['nm_ibu']; ?>' required>
                        </div>
                        <div class="form-group">
                            <label>No Telp Wali</label>
                            <input type="text" name='no_telp_wali' class="form-control" placeholder="Masukkan No Telp Wali" value='<?php echo $f['no_telp_wali']; ?>' required>
                        </div>
                        <div class="form-group">
                            <label>Jenjang Pendidikan</label>
                            <select name='id_jenjang' class="form-control select2" style="width: 100%;" required>
                                <option selected="selected" value="">Pilihan</option>
                                <?php
                                $s = mysqli_query($kon, "select * from jenjang order by jenjang asc");
                                while ($ss = mysqli_fetch_array($s)) {
                                    if ($f['id_jenjang'] == $ss['id_jenjang']) {
                                        echo "<option value='$ss[id_jenjang]' selected>$ss[jenjang]</option>";
                                    } else {
                                        echo "<option value='$ss[id_jenjang]'>$ss[jenjang]</option>";
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Status</label>
                            <select name='status_santri' class="form-control select2" style="width: 100%;" required>
                                <option selected="selected" value="">Pilihan</option>
                                <option value="Aktif" <?php if ($f['status_santri'] == 'Aktif') {
                                                            echo "selected";
                                                        } ?>>Aktif</option>
                                <option value="Tidak Aktif" <?php if ($f['status_santri'] == 'Tidak Aktif') {
                                                                echo "selected";
                                                            } ?>>Tidak Aktif</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Foto</label>*<br>
                            <img src='../files/<?php echo $f['foto']; ?>' width='200px'>
                            <input name='file' type="file" class="form-control" placeholder="Pilih Foto" accept=".jpg,.jpeg,.png">
                        </div>
                        * Jika tidak dirubah biarkan saja.<br>
                    </div>
                    <div class="box-footer">
                        <input type="submit" name='save' class="btn btn-primary" value="Submit">
                        <input type="Reset" class="btn btn-danger" value="Reset">
                        <a href="dsantri.php"><button type="button" class="btn btn-info">Lihat</button></a>
                    </div>
                </form>
                <!-- /.box-body -->
            </div>
        </section>
        <!-- /.Left col -->
    </div>
    <!-- /.row (main row) -->

</section>
<?php require_once('bawah.php'); ?>