<?php require_once('atas.php');
if (!empty($_FILES)) {
    // Validating SQL file type by extensions
    if (!in_array(strtolower(pathinfo($_FILES["backup_file"]["name"], PATHINFO_EXTENSION)), array(
        "png"
    ))) {
        echo "<script type='text/javascript'>alert(\"Format salah! (harus .png).\");window.location=\"kartu.php\";</script>";
    } else {
        if (is_uploaded_file($_FILES["backup_file"]["tmp_name"])) {

            $backup_file_name =  'baruupfoto' . time() . "-" . date('Y-m-d') . '.png';

            move_uploaded_file($_FILES["backup_file"]["tmp_name"], "../cdr/" . $backup_file_name);

            // echo "<script type='text/javascript'>alert(\"Berhasil!.\");window.location=\"kartu.php\";</script>";
        }
    }
}

if (isset($_GET['d'])) {
    unlink("../cdr/" . $_GET['d']);
    echo "<script type='text/javascript'>alert(\"Berhasil!\");window.location=\"kartu.php\";</script>";
}
if (isset($_GET['u'])) {
    // Validating SQL file type by extensions
    if (!in_array(strtolower(pathinfo($_GET['u'], PATHINFO_EXTENSION)), array(
        "png"
    ))) {
        $response = array(
            "type" => "error",
            "message" => "Invalid File Type"
        );
    } else {

        $backup_file_name =  'fotobackup' . time() . "-" . date('Y-m-d') . '.png';
        // rename kartu lama menjadi backup
        rename("../cdr/kartu.png", "../cdr/$backup_file_name");
        // rename kartu dipilih menjadi kartu.png
        rename("../cdr/$_GET[u]", "../cdr/kartu.png");
        echo "<script type='text/javascript'>alert(\"Berhasil!\");window.location=\"kartu.php\";</script>";
    }
}

// function restoreMysqlDB($filePath, $kon)
// {
//     $sql = '';
//     $error = '';

//     if (file_exists($filePath)) {
//         $lines = file($filePath);

//         foreach ($lines as $line) {

//             // Ignoring comments from the SQL script
//             if (substr($line, 0, 2) == '--' || $line == '') {
//                 continue;
//             }

//             $sql .= $line;

//             if (substr(trim($line), -1, 1) == ';') {
//                 $result = mysqli_query($kon, $sql);
//                 if (!$result) {
//                     $error .= mysqli_error($kon) . "\n";
//                 }
//                 $sql = '';
//             }
//         } // end foreach

//         if ($error) {
//             $response = array(
//                 "type" => "error",
//                 "message" => $error
//             );
//         } else {
//             $response = array(
//                 "type" => "success",
//                 "message" => "Database Restore Completed Successfully."
//             );
//         }
//         exec('rm ' . $filePath);
//     } // end if file exists

//     // return $response;
// }
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Dashboard
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Backup Database</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <!-- Left col -->
        <section class="col-lg-7 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-download"></i> Upload Kartu</h3>
                </div>
                <div class="box-body">
                    <p>Pilih file kartu yang sudah anda edit, kemudian klik upload. Jika anda ingin menggunakan foto yang anda upload. Klik icon Centang pada tabel dibawah.</p>
                    <div class="table-responsive" style="border: 0px solid #ddd;">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tanggal</th>
                                    <th>Foto</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;
                                $dir = "../cdr/";

                                // Membuka direktori dan membaca dan menampilkan isinya

                                if (is_dir($dir)) {

                                    if ($dh = opendir($dir)) {

                                        while (($file = readdir($dh)) !== false) {

                                            $info = pathinfo($dir . $file);
                                            $exten = $info['extension'];
                                            if ($exten == 'png') {
                                                $tgl = substr($file, 29, 2);
                                                $td = substr($file, 26, 2);
                                                switch ($td) {

                                                    case '01':
                                                        $tdd = "Januari";
                                                        break;

                                                    case '02':
                                                        $tdd = "Februari";
                                                        break;

                                                    case '03':
                                                        $tdd = "Maret";
                                                        break;

                                                    case '04':
                                                        $tdd = "April";
                                                        break;

                                                    case '05':
                                                        $tdd = "Mei";
                                                        break;

                                                    case '06':
                                                        $tdd = "Juni";
                                                        break;

                                                    case '07':
                                                        $tdd = "Juli";
                                                        break;

                                                    case '08':
                                                        $tdd = "Agustus";
                                                        break;

                                                    case '09':
                                                        $tdd = "September";
                                                        break;

                                                    case '10':
                                                        $tdd = "Oktober";
                                                        break;

                                                    case '11':
                                                        $tdd = "November";
                                                        break;

                                                    case '12':
                                                        $tdd = "Desember";
                                                        break;

                                                    default:
                                                        # code...
                                                        break;
                                                }
                                                echo "
                                                <tr>
                                                <td>$no</td> 
                                                <td>";
                                                if ($file != 'kartu.png') {
                                                    echo "$tgl $tdd " . substr($file, 21, 4) . "";
                                                } else {
                                                    echo "Terpakai";
                                                }
                                                echo "</td> 
                                                <td><img src='../cdr/$file' height=100px><br>$file</td>
                                                <td>";
                                                if ($file != 'kartu.png') {
                                                    echo "
                                                    <a class='btn btn-success btn-xs' href='kartu.php?u=$file' onclick=\"return confirm('Yakin ingin menggunakan foto ini?')\">
                                                    <i class='fa fa-get-pocket'></i></a>
                                                    <a class='btn btn-danger btn-xs' href='kartu.php?d=$file' onclick=\"return confirm('Yakin hapus?')\">
                                                    <i class='fa fa-remove'></i></a>
                                                    ";
                                                } else {
                                                    echo "Terpakai";
                                                }
                                                echo "
                                                </td> 
                                                </tr>
                                                ";
                                                $no++;
                                            }
                                        }

                                        closedir($dh);
                                    }
                                }

                                ?>
                            </tbody>
                        </table>
                    </div>
                    <form method="post" action="" enctype="multipart/form-data" id="frm-restore">
                        <div class="form-group">
                            <div><b>Pilih File Foto</b>
                                <p>Pastikan foto sesuai ukuran</p>
                            </div>
                            <div>
                                <input type="file" name="backup_file" class="input-file" required />
                            </div>
                        </div>
                </div>
                <div class="box-footer">
                    <input type="submit" name='ganii' class="btn btn-danger" value="Upload" <?php echo "onclick=\"return confirm('Yakin ingin upload template Kartu Santri?')\""; ?>>
                    </form>
                </div>
                <!-- /.box-body -->
            </div>
        </section>
        <!-- Left col -->
        <section class="col-lg-5 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title"><a href="kartu.php"><i class="fa fa-refresh"></i></a> Tampilan Kartu Santri</h3>
                </div>
                <div class="box-body">
                    <p>Jika anda ingin mencetak kartu santri, Silahkan klik <a href='dsantri.php'>disini!</a>. Kemudian centang daftar nama yang ingin anda cetak. Data kartu santri ini dipilih secara acak untuk contoh tampilan kartu santri.</p>
                    <?php
                    $sql = "SELECT * FROM santri";
                    $s = mysqli_query($kon, $sql);

                    $jml = mysqli_num_rows($s);
                    $n = $jml;
                    // include file qrlib.php
                    include "../phpqrcode/qrlib.php";

                    //Nama Folder file QR Code kita nantinya akan disimpan
                    $tempdir = "../qr/";

                    //jika folder belum ada, buat folder
                    if (!file_exists($tempdir)) {
                        mkdir($tempdir);
                    }

                    $nist = mysqli_query($kon, "SELECT * FROM santri order by rand() limit 1");
                    $nistt = mysqli_fetch_array($nist); #parameter inputan 
                    $isi_teks = $nistt['nis'];
                    $namafile = $nistt['nis'] . ".png";
                    $quality = 'H'; //ada 4 pilihan, L (Low), M(Medium), Q(Good), H(High) 
                    $ukuran = 10; //batasan 1 paling kecil, 10 paling besar 
                    $padding = 2;
                    QRCode::png($isi_teks, $tempdir . $namafile, $quality, $ukuran, $padding);
                    echo "
                        <table width='376' border=0 style='
                        background-image:url(../cdr/kartu.png);
                        background-size: 8.5cm 5.5cm;
                        width:8.5cm;
                        height:5.5cm;
                        
                        '>
                            <tr>
                                <th width='5' rowspan=7  '></th>
                                <th width='79' height='17'></th>
                                <th width='133'></th>
                                <th width='81'></th>
                            </tr>
                            <tr>
                                <th rowspan=3 style='width:0.1cm;height:0.1cm;'> <img src='../files/$nistt[foto]' width='79' height='105' style='width:80px;align:top;border:2px solid white;border-radius:5px;'></th>
                                <th height='45'></th>
                                <th></th>
                            </tr>
                            <tr>
                                <th height='35'>$nistt[nama_lengkap]</th>
                                <th rowspan=2><img src='../qr/$nistt[nis].png' width='65' height='65'></th>
                            </tr>
                            <tr>
                                <th height='21' style='font-size:8px'>NIS : $nistt[nis]</th>
                            </tr>
                            <tr>
                                <th height='22' style='font-size:8px'>Alamat : </th>
                                <th align='left' style='font-size:8px'><p>$nistt[alamat]</p></th>
                                <th></th>
                            </tr>
                            <tr>
                                <th height='14' style='font-size:8px; align=left'>TTL : </th>
                                <th align='left' style='font-size:8px;align=left'>$nistt[tempat_lhr], " . tgl_indo($nistt['tgl_lhr']) . "</th>
                                <th></th>
                            </tr>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </table>
                        <br>
                        ";
                    ?>
                </div>
                <div class="box-footer">
                    <p>Klik <a href='../cdr/Desain Kartu Santri.cdr'>disini</a> untuk mengambil template kartu santri(format .cdr)</p>
                </div>
            </div>
        </section>
        <!-- /.Left col -->
    </div>
    <!-- /.row (main row) -->

</section>
<?php require_once('bawah.php'); ?>