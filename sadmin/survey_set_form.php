<?php require_once('atas.php');
if (isset($_POST["save"])) {
	$title = $_POST['title'];
	$description = $_POST['description'];
	$id_admin = $_SESSION['id_admin'];
  $kategori_survey_set=$_POST['kategori_survey_set'];
  $status_survey_set=$_POST['status_survey_set'];
	$start_date = date("Y-m-d", strtotime(substr($_POST['tgl_survey'], 0, 11)));
	$end_date = date("Y-m-d", strtotime(substr($_POST['tgl_survey'], 12, 11)));
	$date_created = date('Y-m-d H:i:s');
  $slug = str_replace(" ", "-", $title);
	$components = addslashes($_POST['components']);
	// echo "$_POST[tgl_survey]<br>
  // title : $title<br>
  // description : $description<br>
  // id_admin : $id_admin<br>
  // start_date : $start_date<br>
  // end_date : $end_date<br>
  // date_created : $date_created<br>
  // slug : $slug<br>
  // kategori_survey_set : $kategori_survey_set<br>
  // status_survey_set : $status_survey_set	<br>
  // component : $components <br>
	// ";

	$sql = mysqli_query($kon,"INSERT into survey_set values(NULL,
  '$title',
  '$slug',
  '$description',
  '$id_admin',
  '$start_date',
  '$end_date',
  '$date_created',
  '$kategori_survey_set',
  '$status_survey_set')");
	
  $qsurvey_set = mysqli_query($kon, "select * from survey_set order by id_survey_set desc limit 1");
  $dqsurvey_set = mysqli_fetch_array($qsurvey_set);
  
	$sql = mysqli_query($kon,"INSERT into questions values(NULL,
  '',
  '$components',
  '$dqsurvey_set[id_survey_set]',
  '$id_admin',
  '$date_created'
  )");
  
  echo "<script type='text/javascript'>alert(\"Berhasil!\");window.location=\"survey.php\";</script>";
}

$valquestion = "{\"components\": []}";
?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Dashboard
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Survey</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <!-- Main row -->
  <div class="row">
    <!-- Left col -->
    <section class="col-lg-12 connectedSortable">
      <!-- Custom tabs (Charts with tabs)-->
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-user-plus"></i> Tambah Survey</h3>
        </div>
        <form action="" method="POST" enctype="multipart/form-data">
          <div class="box-body">
            <div class="form-group">
              <label for="title">Judul</label>

              <input type="text" class="form-control" name="title" required>
            </div>
            <div class="form-group">
              <label>Deskripsi</label>

              <textarea class="textarea" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" id="description" name="description" required></textarea>
            </div>
            <div class="form-group">
              <label>Tanggal Kuesioner </label>

              <input type="text" class="form-control" id="reservation" name="tgl_survey" required>
            </div>
            <div class="form-group">
              <label>Kategori Survey</label><br>
              <input type="radio" name="kategori_survey_set" class="flat-red" value="survey" checked> Survey
              <input type="radio" name="kategori_survey_set" class="flat-red" value="PSB"> PSB
            </div>
            <div class="form-group">
              <label>Status Aktif</label><br>
              <input type="radio" name="status_survey_set" class="flat-red" value="Y" checked> Ya
              <input type="radio" name="status_survey_set" class="flat-red" value="T"> Tidak
            </div>
            <div class="form-group">
              <label>Pertanyaan</label><br>

              <textarea name="components" id="components" cols="30" rows="10" hidden><?php echo $valquestion; 
                                                                                      ?></textarea>
              <div id='builder'></div>
            </div>
          </div>
          <div class="box-footer">
            <input type="submit" name='save' class="btn btn-primary" value="Simpan" onclick=\"return confirm('Yakin?')\">
            <input type="Reset" class="btn btn-danger" value="Reset">
            <a href="survey.php"><button type="button" class="btn btn-info">Lihat</button></a>
          </div>
        </form>
        <!-- /.box-body -->
      </div>
    </section>
    <!-- /.Left col -->
  </div>
  <!-- /.row (main row) -->

</section>

<script>
        var jsonElement = document.getElementById('components');

        /**
         * CARA 1
         */
        var form_fields = jsonElement.value;
        const obj = JSON.parse(form_fields);
        Formio.builder(document.getElementById('builder'), obj, {})

            .then((form) => {
                form.on('change', function() {
                    jsonElement.value = JSON.stringify(form.schema);
                })
            });




        /**
         * CARA 2
         */

        /* var builder = new Formio.FormBuilder(document.getElementById("builder"), {
            display: 'form',
            components: [],
        });

        var onBuild = function(build) {
            jsonElement.value = '';
            jsonElement.value = JSON.stringify(builder.instance.schema);
        };

        var onReady = function() {
            builder.instance.on('change', onBuild);
        };

        builder.instance.ready.then(onReady); */

        // $("#submit").click(function(e) {
        //     e.preventDefault();

        //     var form_fields = $("#components").val();

        //     console.log(form_fields);

        //     $.ajax({
        //         type: 'POST',
        //         url: '/',
        //         data: "form_field = " + form_fields,
        //         success: function() {
        //             alert('success')
        //         },
        //         error: function() {
        //             alert('fail');
        //         }
        //     });
        // });
    </script>
<?php require_once('bawah.php'); ?>