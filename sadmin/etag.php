<?php require_once('atas.php');
if (isset($_POST["simpan"])) {
    if (empty($_GET['e'])) {
        echo "<script type='text/javascript'>alert(\"Bulan belum dipilih!\");history.back();</script>";
    } else {
        if ($_SESSION['level'] == 'sadmin') {
            if ($_GET['j'] == "all") {
                $s = mysqli_query($kon, "select * from santri left join jenjang on santri.id_jenjang=jenjang.id_jenjang order by nis desc");
            } else {
                $s = mysqli_query($kon, "select * from santri left join jenjang on santri.id_jenjang=jenjang.id_jenjang where jkel='$_GET[j]' order by nis desc");
            }
        } else {
            $s = mysqli_query($kon, "select * from santri left join jenjang on santri.id_jenjang=jenjang.id_jenjang $_SESSION[where] order by nis desc");
        }
        $no = 1;
        while ($f = mysqli_fetch_array($s)) {
            $intag = mysqli_query($kon, "select * from tagihan where nis='$f[nis]' and waktu_tagihan='$_POST[e]' ");
            $syh = $_POST['syahriah' . $f['nis']];
            $km = $_POST['kos_makan' . $f['nis']];
            $skl = $_POST['sekolah' . $f['nis']];
            $created_by = $_SESSION['id_admin'];
            $created_at = date("Y-m-d h:i:s");
            if (mysqli_num_rows($intag) > 0) {
                $a = mysqli_query($kon, "update tagihan set
                    syahriah='$syh',
                    kos_makan='$km',
                    sekolah='$skl',
                    created_by='$created_by',
                    created_at='$created_at'
                    where 
                    waktu_tagihan='$_POST[e]' and
                    nis='$f[nis]'
                    ");
            } else {
                $a = mysqli_query($kon, "insert into tagihan values(
                    NULL,
                    '$_POST[e]',
                    '$f[nis]',
                    '$syh',
                    '$km',
                    '$skl',
                    '$created_by',
                    '$created_at',
                    ''
                    )");
            }
            if ($a) {
                echo "<script type='text/javascript'>alert(\"Berhasil!\");window.location=\"dtag.php\";</script>";
            } else {
                echo "<script type='text/javascript'>alert(\"Gagal!\");history.back();</script>";
            }
            $no++;
        }
    }
}
if (isset($_POST["updateKet"])) {
    $a = mysqli_query($kon, "update tagihan set
        ket='$_POST[ket]'
        where 
        id_tagihan='$_POST[id_tagihan]'
        ");
    if ($a) {
        echo "<script type='text/javascript'>alert(\"Berhasil!\");window.location=\"etag.php?e=$_POST[e]&j=$_POST[j]\";</script>";
    } else {
        echo "<script type='text/javascript'>alert(\"Gagal!\");history.back();</script>";
    }
}
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Dashboard
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Tagihan</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"><a href='dtag.php'><i class="fa fa-edit"></i></a> Edit Tagihan Santri</h3>
                    <form style='margin-right:5px; margin-top:0px' class='pull-right' action='' method='GET'>
                        <input type="hidden" name="a" id="a">
                        <input type="hidden" name="b" id="b">
                        <input type="hidden" name="c" id="c">
                        <input type="hidden" name="e" value="<?php echo "$_GET[e]"; ?>" required>
                        <input type='hidden' name='j' value='<?php echo "$_GET[j]"; ?>' required>
                        <input type="submit" style='margin-top:0px' class='btn btn-success btn-sm' value='Lihat'>
                    </form>
                </div>
                <div class="box-body">
                    <form action='' method='POST'>
                        <div class="table-responsive" style="border: 0px solid #ddd;">
                            <table id="example3" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>NIS</th>
                                        <th>Nama Lengkap</th>
                                        <th>Jenjang</th>
                                        <th>Syahriah<br><input type='number' name='csyahirah' id='csyahirah' min=0 style="width:95%" value="<?php if (isset($_GET['a'])) {
                                                                                                                                                echo "$_GET[a]";
                                                                                                                                            } else {
                                                                                                                                                echo "0";
                                                                                                                                            } ?>">
                                        </th>
                                        <th>Kos Makan<br><input type='number' name='ckos_makan' id='ckos_makan' min=0 style="width:95%" value="<?php if (isset($_GET['b'])) {
                                                                                                                                                    echo "$_GET[b]";
                                                                                                                                                } else {
                                                                                                                                                    echo "0";
                                                                                                                                                } ?>"></th>
                                        <th>Sekolah<br><input type='number' name='csekolah' id='csekolah' min=0 style="width:95%" value="<?php if (isset($_GET['c'])) {
                                                                                                                                                echo "$_GET[c]";
                                                                                                                                            } else {
                                                                                                                                                echo "0";
                                                                                                                                            } ?>"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no = 1;
                                    if ($_SESSION['level'] == 'sadmin') {
                                        if ($_GET['j'] == "all") {
                                            $s = mysqli_query($kon, "select * from santri left join jenjang on santri.id_jenjang=jenjang.id_jenjang order by nis desc");
                                        } else {
                                            $s = mysqli_query($kon, "select * from santri left join jenjang on santri.id_jenjang=jenjang.id_jenjang where jkel='$_GET[j]' order by nis desc");
                                        }
                                    } else {
                                        $s = mysqli_query($kon, "select * from santri left join jenjang on santri.id_jenjang=jenjang.id_jenjang $_SESSION[where] order by nis desc");
                                    }
                                    while ($f = mysqli_fetch_array($s)) {
                                        $tag = mysqli_query($kon, "select * from tagihan where nis='$f[nis]' and waktu_tagihan='$_GET[e]' ");
                                        $cek = mysqli_num_rows($tag);
                                        $dtag = mysqli_fetch_array($tag);
                                        echo "
                                        <tr>
                                            <td>$no</td>
                                            <td>$f[nis]</td>
                                            <td>$f[nama_lengkap] <b>($f[jkel])</b>
                                            ";
                                        if (!empty($dtag['id_tagihan'])) {
                                            echo "<a href='#' type='button' data-toggle='modal' data-target='#myModal$dtag[id_tagihan]'>
                                            <i class='fa fa-info-circle'></i></a>";
                                        }
                                        echo "</td>
                                            <td>$f[jenjang]</td>
                                            <td><input type='number' name='syahriah$f[nis]' min=0 value='";
                                        if (isset($_GET['a']) and ($_GET['a'] != 0)) {
                                            echo "$_GET[a]";
                                        } elseif (isset($dtag['syahriah'])) {
                                            echo "$dtag[syahriah]";
                                        } else {
                                            echo "0";
                                        }
                                        echo "' required></td>
                                            <td><input type='number' name='kos_makan$f[nis]' min=0 value='";
                                        if (isset($_GET['b']) and ($_GET['b'] != 0)) {
                                            echo "$_GET[b]";
                                        } elseif (isset($dtag['kos_makan'])) {
                                            echo "$dtag[kos_makan]";
                                        } else {
                                            echo "0";
                                        }
                                        echo "' required></td>
                                            <td><input type='number' name='sekolah$f[nis]' min=0 value='";
                                        if (isset($_GET['c']) and ($_GET['c'] != 0)) {
                                            echo "$_GET[c]";
                                        } elseif (isset($dtag['sekolah'])) {
                                            echo "$dtag[sekolah]";
                                        } else {
                                            echo "0";
                                        }
                                        echo "' required></td>
                                        </tr>
                                        ";
                                        $no++;
                                    ?>
                                    <?php
                                        if (!empty($dtag['id_tagihan'])) {
                                            $id = $dtag['id_tagihan'];
                                            $ket = mysqli_query($kon, "select * from tagihan where id_tagihan='$id'");
                                            $eket = mysqli_fetch_array($ket);
                                            echo "
                                            <div class='modal fade' id='myModal$id' role='dialog'>
                                                <div class='modal-dialog'>
                                                    <div class='modal-content'>
                                                        <form method='POST' action=''>
                                                            <div class='modal-header'>
                                                                <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                                                                    <span aria-hidden='true'>&times;</span></button>
                                                                <h4 class='modal-title'>Edit Keterangan</h4>
                                                            </div>
                                                            <div class='modal-body'>
                                                                <input type='hidden' name='id_tagihan' value='$eket[id_tagihan]'>
                                                                <input type='hidden' name='e' value='$_GET[e]'>
                                                                <input type='hidden' name='j' value='$_GET[j]'>
                                                                <div class='form-group'>
                                                                    <label>Keterangan</label>
                                                                    <textarea name='ket' class='form-control' rows='3'>$eket[ket]</textarea>
                                                                </div>
                                                            </div>
                                                            <div class='modal-footer'>
                                                                <button type='button' class='btn btn-default pull-left' data-dismiss='modal'>Close</button>
                                                                <button type='submit' class='btn btn-primary' name='updateKet'>Save changes</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        ";
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        <input type="hidden" name="e" value="<?php echo "$_GET[e]"; ?>">
                        <button style='margin-top:1px;' type='reset' name='reset' class='btn btn-sm btn-info pull-right'>Reset</button>
                        <button style='margin-top:1px;margin-right:5px;' type='submit' name='simpan' class='btn btn-sm btn-primary pull-right' <?php if (isset($cek) and $cek > 0) {
                                                                                                                                                    echo "onclick=\"return confirm('Data sudah ada, yakin update data?')\"";
                                                                                                                                                } else {
                                                                                                                                                    echo "onclick=\"return confirm('Yakin simpan?')\"";
                                                                                                                                                }
                                                                                                                                                ?>>Simpan</button>
                    </form>
                </div>
                <!-- /.box-body -->
            </div>
        </section>
        <!-- /.Left col -->
    </div>
    <!-- /.row (main row) -->
    <?php require_once('bawah.php'); ?>
    <script>
        $(document).ready(function() {
            $('#a').val($('#csyahirah').val());
            $('#b').val($('#ckos_makan').val());
            $('#c').val($('#csekolah').val());

            $('#csyahirah').change(function() {
                $('#a').val($('#csyahirah').val());
            });
            $('#ckos_makan').change(function() {
                $('#b').val($('#ckos_makan').val());
            });
            $('#csekolah').change(function() {
                $('#c').val($('#csekolah').val());
            });
        });
    </script>