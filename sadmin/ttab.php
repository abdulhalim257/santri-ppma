<?php require_once('atas.php');
if (isset($_POST["save"])) {
    $nis = $_POST['nis'];
    $jumlah_tab = $_POST['jumlah_tab'];
    $tgl_lh = date('Y-m-d', strtotime($_POST['tgl_tab']));
    $tgl_tab = $tgl_lh;
    $create_by = $_SESSION['id_admin'];
    $create_at = date("Y-m-d h:i:s");
    $edit_by = '';
    $edit_at = '';
    $tab = date('Ymd');
    $ni = mysqli_query($kon, "select * from tabungan where id_tabungan LIKE '%$tab%' order by id_tabungan desc limit 1");
    $nit = mysqli_fetch_array($ni);
    $jni = mysqli_num_rows($ni);
    if ($jni > 0) {
        $id_tabungan = $nit['id_tabungan'] + 1;
    } else {
        $id_tabungan = $tab . '0001';
    }
    // echo$tab;
    $a = mysqli_query($kon, "insert into tabungan values(
            '$id_tabungan',
            '$nis',
            '$jumlah_tab',
            '$tgl_tab',
            '$create_by',
            '$create_at',
            '$edit_by',
            '$edit_at'
            )");
    if ($a) {
        echo "<script type='text/javascript'>alert(\"Berhasil!\");window.location=\"dtab.php\";</script>";
    } else {
        echo "<script type='text/javascript'>alert(\"Gagal!\");history.back();</script>";
    }
}
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Dashboard
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Tabungan</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <!-- Left col -->
        <section class="col-lg-7 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-money"></i> Tambah Tabungan</h3>
                </div>
                <form action="" method="POST" enctype="multipart/form-data">
                    <div class="box-body">
                        <div class="form-group">
                            <label>NIS</label>
                            <select name='nis' class="form-control select2" style="width: 100%;" required>
                                <option selected="selected" value="">Pilihan</option>
                                <?php
                                if ($_SESSION['level'] == 'sadmin') {
                                    $s = mysqli_query($kon, "select * from santri where status_santri='Aktif'");
                                } else {
                                    $s = mysqli_query($kon, "select * from santri $_SESSION[where] and status_santri='Aktif'");
                                }
                                while ($ss = mysqli_fetch_array($s)) {

                                    echo "<option value='$ss[nis]' ";
                                    if (isset($_POST['nis']) and $ss['nis'] == $_POST['nis']) {
                                        echo "selected";
                                    }
                                    echo ">$ss[nama_lengkap] ($ss[nis])</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Jumlah</label>
                            <input type="number" min=0 name='jumlah_tab' class="form-control" placeholder="Masukkan Jumlah" required>
                        </div>
                        <div class="form-group">
                            <label>Tanggal</label>
                            <input type="text" name='tgl_tab' class="form-control pull-right" id="datepicker" placeholder="Masukkan Tanggal" required>
                        </div>
                    </div>
                    <div class="box-footer">
                        <input type="submit" name='save' class="btn btn-primary" value="Submit">
                        <input type="Reset" class="btn btn-danger" value="Reset">
                        <a href="dtab.php"><button type="button" class="btn btn-info">Lihat</button></a>
                    </div>
                </form>
                <!-- /.box-body -->
            </div>
        </section>
        <!-- /.Left col -->
    </div>
    <!-- /.row (main row) -->

</section>
<?php require_once('bawah.php'); ?>