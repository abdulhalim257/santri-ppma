<!-- Add -->
<?php require_once('atas.php');
if (isset($_GET["d"])) {
    $no = 1;
    $d = mysqli_query($kon, "select * from survey_set left join id_survey_set_add ='$_GET[d]'");
    $dd = mysqli_fetch_array($d);
    // unlink('../files/'.$dd['foto']);
    mysqli_query($kon, "delete from survey_set_add where survey ='$_GET[d]'");
    echo "<script type='text/javascript'>alert(\"Berhasil!\");window.location=\"dtab.php\";</script>";
}
<div class="modal fade" id="addnew">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><b>Tambah Kuesioner </b></h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" method="POST" action="survey_set_add.php" enctype="multipart/form-data">
          <div class="form-group">
            <label for="title" class="col-sm-3 control-label">Judul</label>

            <div class="col-sm-9">
              <input type="text" class="form-control" id="title" name="title" required>
            </div>
          </div>
          <div class="form-group">
            <label for="description" class="col-sm-3 control-label">Deskripsi</label>

            <div class="col-sm-9">
              <textarea class="textarea" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" id="description" name="description" required></textarea>
            </div>
          </div>
          <div class="form-group">
            <label for="tgl_survey" class="col-sm-3 control-label">Tanggal Kuesioner </label>

            <div class="col-sm-9">
              <input type="text" class="form-control" id="reservation" name="tgl_survey" required>
            </div>
          </div>
          <!-- <div class="form-group">
            <label for="Status survey_set" class="col-sm-12 control-label">

              <p>* Selain kolom ini dilengkapi survey_set</p>
            </label>
          </div> -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
        <button type="reset" class="btn btn-danger btn-flat" name="reset"><i class="fa fa-refresh"></i> Reset</button>
        <button type="submit" class="btn btn-primary btn-flat" name="add"><i class="fa fa-save"></i> Save</button>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- Edit -->
<div class="modal fade" id="edit">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><b>Edit Kuesioner Set</b></h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" method="POST" action="survey_set_edit.php" enctype="multipart/form-data">
          <input type="hidden" class="nim" name="id">
          <div class="form-group">
            <label for="nim" class="col-sm-3 control-label">Nim *</label>

            <div class="col-sm-9">
              <input type="text" class="form-control" id="edit_nim" name="nim" required>
            </div>
          </div>
          <div class="form-group">
            <label for="password" class="col-sm-3 control-label">Password *</label>

            <div class="col-sm-9">
              <input type="hidden" class="form-control" id="edit_passworddulu" name="passworddulu" required>
              <input type="password" class="form-control" id="edit_password" name="password" required>
            </div>
          </div>
          <div class="form-group">
            <label for="nama_lengkap" class="col-sm-3 control-label">Nama Lengkap</label>

            <div class="col-sm-9">
              <input type="text" class="form-control" id="edit_nama_lengkap" name="nama_lengkap">
            </div>
          </div>
          <div class="form-group">
            <label for="jenis_kelamin" class="col-sm-3 control-label">Jenis Kelamin</label>

            <div class="col-sm-9">
              <select class="form-control" name="jenis_kelamin" id="edit_jenis_kelamin">
                <option value="" selected>- Pilih Jenis Kelamin -</option>
                <option value="L">Laki-laki</option>
                <option value="P">Perempuan</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label for="alamat" class="col-sm-3 control-label">Alamat</label>

            <div class="col-sm-9">
              <input type="text" class="form-control" id="edit_alamat" name="alamat">
            </div>
          </div>
          <div class="form-group">
            <label for="tgl_lahir" class="col-sm-3 control-label">Tanggal Lahir</label>

            <div class="col-sm-9">
              <input type="date" class="form-control" id="edit_tgl_lahir" name="tgl_lahir">
            </div>
          </div>
          <div class="form-group">
            <label for="email" class="col-sm-3 control-label">Email</label>

            <div class="col-sm-9">
              <input type="email" class="form-control" id="edit_email" name="email">
            </div>
          </div>
          <div class="form-group">
            <label for="no_hp" class="col-sm-3 control-label">No Hp</label>

            <div class="col-sm-9">
              <input type="text" class="form-control" id="edit_no_hp" name="no_hp">
            </div>
          </div>
          <div class="form-group">
            <label for="tahun_masuk" class="col-sm-3 control-label">Tahun Masuk</label>

            <div class="col-sm-9">
              <input type="number" class="form-control" id="edit_tahun_masuk" name="tahun_masuk" max=<?php echo date('Y'); ?>>
            </div>
          </div>
          <div class="form-group">
            <label for="tahun_lulus" class="col-sm-3 control-label">Tahun Lulus</label>

            <div class="col-sm-9">
              <input type="number" class="form-control" id="edit_tahun_lulus" name="tahun_lulus" max=<?php echo date('Y'); ?>>
            </div>
          </div>

          <div class="form-group">
            <label for="prodi" class="col-sm-3 control-label">Program Studi</label>

            <div class="col-sm-9">
              <select class="form-control select2" name="id_prodi" id="edit_id_prodi" style="width:100%">
                <option value="">- Pilih Prodi - </option>
                <?php
                $ssql = "SELECT * FROM prodi where status_prodi='Y'";
                $query = $conn->query($ssql);
                while ($srow = $query->fetch_assoc()) {
                  echo "<option value='$srow[id_prodi]'>$srow[nama_prodi]</option>";
                }
                ?>
              </select>
            </div>

          </div>
          <div class="form-group">
            <label for="Status survey_set" class="col-sm-3 control-label">Status Kuesioner Set</label>

            <div class="col-sm-9">
              <select class="form-control" name="status_survey_set" id="edit_status_survey_set" required>
                <option value="" selected>- Pilih Status -</option>
                <option value="Y">Ya</option>
                <option value="T">Tidak</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label for="Status survey_set" class="col-sm-12 control-label">

              <p>* Biarkan saja jika tidak diganti</p>
            </label>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
        <button type="submit" class="btn btn-success btn-flat" name="edit"><i class="fa fa-check-square-o"></i> Update</button>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- Delete -->
<div class="modal fade" id="delete">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><b>Deleting...</b></h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" method="POST" action="survey_set_delete.php">
          <input type="hidden" class="id_survey_set" name="id">
          <div class="text-center">
            <p>Hapus Kuesioner </p>
            <h2 id="del_survey_set" class="bold"></h2>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-flat pull-left" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
        <button type="submit" class="btn btn-danger btn-flat" name="delete"><i class="fa fa-trash"></i> Delete</button>
        </form>
      </div>
    </div>
  </div>
</div>