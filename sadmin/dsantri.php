<?php require_once('atas.php');
if (isset($_GET["d"])) {
    $no = 1;
    $d = mysqli_query($kon, "select * from santri where nis='$_GET[d]'");
    $dd = mysqli_fetch_array($d);
    unlink('../files/' . $dd['foto']);
    mysqli_query($kon, "delete from santri where nis='$_GET[d]'");
    echo "<script type='text/javascript'>alert(\"Berhasil!\");window.location=\"dsantri.php\";</script>";
}
if (isset($_GET["r"])) {
    $no = 1;
    $d = mysqli_query($kon, "select * from santri where nis='$_GET[r]'");
    $dd = mysqli_fetch_array($d);
    $pass = md5($dd['nis']);
    mysqli_query($kon, "update santri set password='$pass' where nis='$dd[nis]'");
    echo "<script type='text/javascript'>alert(\"Berhasil!\");window.location=\"dsantri.php\";</script>";
}
// elseif(isset($_GET["r"])){
//     $p=md5('santri');
//     $a=mysqli_query($kon,"update nis set
//     password='$p' where nis='$_GET[r]'");
//     echo "<script type='text/javascript'>alert(\"Berhasil!\");window.location=\"dnis.php\";</script>";
// }
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Dashboard
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Santri</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"><a href='tsantri.php'><i class="fa fa-user"></i></a> Daftar Santri</h3>
                    <form style='margin-right:5px; margin-top:0px' class='pull-right' action='' method='GET'>
                        <select class='select2' name='j' style='padding:4px'>
                            <?php
                            echo "<option value='all'>- Semua -</option>";
                            echo "<option value='L' ";
                            if (isset($_GET['j']) and $_GET['j'] == "L") {
                                echo "selected";
                            }
                            echo ">Laki-laki</option>";
                            echo "<option value='P'";
                            if (isset($_GET['j']) and $_GET['j'] == "P") {
                                echo "selected";
                            }
                            echo ">Perempuan</option>";
                            ?>
                        </select>
                        <input type="submit" style='margin-top:0px' class='btn btn-success btn-sm' value='Lihat'>
                    </form>
                </div>
                <div class="box-body">
                    <form action='print_kartu.php' method='POST' target='_blank()'>
                        <div class="table-responsive" style="border: 0px solid #ddd;">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th><input type='checkbox' onchange='checkAll(this)' name='chk[]' /></th>
                                        <th>NIS</th>
                                        <th>Nama Lengkap</th>
                                        <th>TTL</th>
                                        <th>JKel</th>
                                        <th>Jenjang</th>
                                        <th>Status</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no = 1;
                                    if (isset($_GET['j'])) {
                                        if ($_GET['j'] == "all") {
                                            $s = mysqli_query($kon, "select * from santri left join jenjang on santri.id_jenjang=jenjang.id_jenjang order by nis desc");
                                        } else {
                                            $s = mysqli_query($kon, "select * from santri left join jenjang on santri.id_jenjang=jenjang.id_jenjang where jkel='$_GET[j]' order by nis desc");
                                        }

                                        while ($f = mysqli_fetch_array($s)) {
                                            echo "
                            <tr>
                                <td>$no</td>
                                <td><input type='checkbox' name='pilih" . $no . "' value='$f[nis]' /></td>
                                <td>$f[nis]</td>
                                <td>$f[nama_lengkap]</td>
                                <td>$f[tempat_lhr], " . tgl_indo($f['tgl_lhr']) . "</td>
                                <td>$f[jkel]</td>
                                <td>$f[jenjang]</td>
                                <td>$f[status_santri]</td>
                                <td>
                                <a class='btn btn-info btn-xs' href='esantri.php?e=$f[nis]'>
                                <i class='fa fa-edit'></i></a>
                                <a class='btn btn-danger btn-xs' href='?d=$f[nis]' onclick=\"return confirm('Yakin hapus?')\">
                                <i class='fa fa-remove'></i></a>
                                <a class='btn btn-primary btn-xs' href='?r=$f[nis]' onclick=\"return confirm('Reset password(sesuai NIS)?')\">
                                <i class='fa fa-refresh'></i></a>
                                </td>
                            </tr>
                            ";
                                            $no++;
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                            <button style='margin-top:1px' type='submit' name='print' class='btn btn-sm btn-info'>Print Kartu</button>
                    </form>
                </div>
            </div>
            <!-- /.box-body -->
    </div>
</section>
<!-- /.Left col -->
</div>
<!-- /.row (main row) -->

</section>
<script type="text/javascript">
    function checkAll(ele) {
        var checkboxes = document.getElementsByTagName('input');
        if (ele.checked) {
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].type == 'checkbox' && !(checkboxes[i].disabled)) {
                    checkboxes[i].checked = true;
                }
            }
        } else {
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].type == 'checkbox') {
                    checkboxes[i].checked = false;
                }
            }
        }
    }
</script>
<?php require_once('bawah.php'); ?>