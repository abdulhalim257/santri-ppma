<?php require_once('atas.php');
if (!empty($_FILES)) {
    // Validating SQL file type by extensions
    if (!in_array(strtolower(pathinfo($_FILES["backup_file"]["name"], PATHINFO_EXTENSION)), array(
        "sql"
    ))) {
        $response = array(
            "type" => "error",
            "message" => "Invalid File Type"
        );
    } else {
        if (is_uploaded_file($_FILES["backup_file"]["tmp_name"])) {

            $backup_file_name =  'ppmabackup' . time() . "-" . date('Y-m-d') . '.sql';

            move_uploaded_file($_FILES["backup_file"]["tmp_name"], $backup_file_name);
            $response = restoreMysqlDB($_FILES["backup_file"]["name"], $kon);
            echo "<script type='text/javascript'>alert(\"Berhasil! Silahkan Login kembali.\");window.location=\"logout.php\";</script>";
        }
    }
}

if (isset($_GET['d'])) {
    unlink($_GET['d']);
    echo "<script type='text/javascript'>alert(\"Berhasil!\");window.location=\"db.php\";</script>";
}
if (isset($_GET['e'])) {
    // Validating SQL file type by extensions
    if (!in_array(strtolower(pathinfo($_GET['e'], PATHINFO_EXTENSION)), array(
        "sql"
    ))) {
        $response = array(
            "type" => "error",
            "message" => "Invalid File Type"
        );
    } else {
        $response = restoreMysqlDB($_GET['e'], $kon);
        echo "<script type='text/javascript'>alert(\"Berhasil! Silahkan Login kembali.\");window.location=\"logout.php\";</script>";
    }
}

function restoreMysqlDB($filePath, $kon)
{
    $sql = '';
    $error = '';

    if (file_exists($filePath)) {
        $lines = file($filePath);

        foreach ($lines as $line) {

            // Ignoring comments from the SQL script
            if (substr($line, 0, 2) == '--' || $line == '') {
                continue;
            }

            $sql .= $line;

            if (substr(trim($line), -1, 1) == ';') {
                $result = mysqli_query($kon, $sql);
                if (!$result) {
                    $error .= mysqli_error($kon) . "\n";
                }
                $sql = '';
            }
        } // end foreach

        if ($error) {
            $response = array(
                "type" => "error",
                "message" => $error
            );
        } else {
            $response = array(
                "type" => "success",
                "message" => "Database Restore Completed Successfully."
            );
        }
        exec('rm ' . $filePath);
    } // end if file exists

    // return $response;
}
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Dashboard
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Backup Database</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <!-- Left col -->
        <section class="col-lg-4 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-download"></i> Backup Database</h3>
                </div>
                <div class="box-body">
                    <div class="form-group">
                        <p>Klik Backup jika anda ingin membackup database dari aplikasi ini. Kemudian simpan file dipenyimpanan anda secara aman.</p>
                    </div>
                </div>
                <div class="box-footer">
                    <a name='backup' class="btn btn-primary" <?php echo "onclick=\"return confirm('Yakin ingin backup database?')\""; ?> href="db_download.php">Backup</a>
                </div>
                <!-- /.box-body -->
            </div>
        </section>
        <!-- Left col -->
        <section class="col-lg-8 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title"><a href="db.php"><i class="fa fa-refresh"></i></a> Restore Database</h3>
                </div>
                <div class="box-body">
                    <p>Jika anda melakukan <b>Restore</b> maka seluruh data pada saat ini akan terganti sesuai data pada file backup!. Anda bisa klik pada tabel yg ada atau mengupload file melalui form dibawah.</p>
                    <div class="table-responsive" style="border: 0px solid #ddd;">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tanggal</th>
                                    <th>File</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;
                                $dir = "../sadmin/";

                                // Membuka direktori dan membaca dan menampilkan isinya

                                if (is_dir($dir)) {

                                    if ($dh = opendir($dir)) {

                                        while (($file = readdir($dh)) !== false) {

                                            $info = pathinfo($dir . $file);
                                            $exten = $info['extension'];
                                            if ($exten == 'sql') {
                                                $tgl = substr($file, 29, 2);
                                                $td = substr($file, 26, 2);
                                                switch ($td) {

                                                    case '01':
                                                        $tdd = "Januari";
                                                        break;

                                                    case '02':
                                                        $tdd = "Februari";
                                                        break;

                                                    case '03':
                                                        $tdd = "Maret";
                                                        break;

                                                    case '04':
                                                        $tdd = "April";
                                                        break;

                                                    case '05':
                                                        $tdd = "Mei";
                                                        break;

                                                    case '06':
                                                        $tdd = "Juni";
                                                        break;

                                                    case '07':
                                                        $tdd = "Juli";
                                                        break;

                                                    case '08':
                                                        $tdd = "Agustus";
                                                        break;

                                                    case '09':
                                                        $tdd = "September";
                                                        break;

                                                    case '10':
                                                        $tdd = "Oktober";
                                                        break;

                                                    case '11':
                                                        $tdd = "November";
                                                        break;

                                                    case '12':
                                                        $tdd = "Desember";
                                                        break;

                                                    default:
                                                        # code...
                                                        break;
                                                }
                                                echo "
                                                <tr>
                                                <td>$no</td> 
                                                <td>$tgl $tdd " . substr($file, 21, 4) . "</td> 
                                                <td>$file</td> 
                                                <td>
                                                <a class='btn btn-info btn-xs' href='db.php?e=$file' onclick=\"return confirm('Yakin ingin restore database?')\">
                                                <i class='fa fa-refresh'></i></a>
                                                <a class='btn btn-danger btn-xs' href='db.php?d=$file' onclick=\"return confirm('Yakin hapus?')\">
                                                <i class='fa fa-remove'></i></a>
                                                </td> 
                                                </tr>
                                                ";
                                                $no++;
                                            }
                                        }

                                        closedir($dh);
                                    }
                                }

                                ?>
                            </tbody>
                        </table>
                    </div><br>
                    <form method="post" action="" enctype="multipart/form-data" id="frm-restore">
                        <div class="form-group">
                            <div><b>Pilih File Backup</b> </div>
                            <div>
                                <input type="file" name="backup_file" class="input-file" required />
                            </div>
                        </div>
                </div>
                <div class="box-footer">
                    <input type="submit" name='restore' class="btn btn-danger" value="Restore" <?php echo "onclick=\"return confirm('Yakin ingin restore database?')\""; ?>>
                    </form>
                </div>
            </div>
        </section>
        <!-- /.Left col -->
    </div>
    <!-- /.row (main row) -->

</section>
<?php require_once('bawah.php'); ?>