<?php 
session_start();
require('konek.php');
?>
<!-- Content Header (Page header) -->

<style>
        body {
            font-family: "Times New Roman", Times, serif;
            font-size: 12px;
        }

        table {
            border-collapse: collapse;
            /* table-layout:fixed;width: 630px; */
        }

        table th {
            padding: 5px;
            word-wrap: break-word;
            /* width: 20%; */
        }

        table td {
            padding: 5px;
            word-wrap: break-word;
            /* width: 20%; */
        }

        h4 {
            text-align: center;
            text-transform: uppercase;
        }
    </style>
<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"><a href='dtag.php'><i class="fa fa-edit"></i></a> Tagihan Santri</h3>
                    Bulan : <?php echo"$_GET[e]"; ?>
                </div>
                <div class="box-body">
                    <form action='' method='POST'>
                    <div class='table-responsive' style='border: 0px solid #ddd;'>
                        <table border='1' width='100%' align=center>
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>NIS</th>
                                        <th>Nama Lengkap</th>
                                        <th>Jenjang</th>
                                        <th>Syahriah</th>
                                        <th>Kos Makan</th>
                                        <th>Sekolah</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no = 1;
                                    if ($_SESSION['level'] == 'sadmin') {
                                        if ($_GET['j'] == "all") {
                                            $s = mysqli_query($kon, "select * from santri left join jenjang on santri.id_jenjang=jenjang.id_jenjang order by nis desc");
                                        } else {
                                            $s = mysqli_query($kon, "select * from santri left join jenjang on santri.id_jenjang=jenjang.id_jenjang where jkel='$_GET[j]' order by nis desc");
                                        }
                                    } else {
                                        $s = mysqli_query($kon, "select * from santri left join jenjang on santri.id_jenjang=jenjang.id_jenjang $_SESSION[where] order by nis desc");
                                    }
                                    while ($f = mysqli_fetch_array($s)) {
                                        $tag = mysqli_query($kon, "select * from tagihan where nis='$f[nis]' and waktu_tagihan='$_GET[e]' ");
                                        $cek = mysqli_num_rows($tag);
                                        $dtag = mysqli_fetch_array($tag);
                                        echo "
                                        <tr>
                                            <td>$no</td>
                                            <td>$f[nis]</td>
                                            <td>$f[nama_lengkap] <b>($f[jkel])</b></td>
                                            <td>$f[jenjang]</td>
                                            <td>";
                                        if (isset($_GET['a']) and ($_GET['a'] != 0)) {
                                            echo "$_GET[a]";
                                        } elseif (isset($dtag['syahriah'])) {
                                            echo "$dtag[syahriah]";
                                        } else {
                                            echo "0";
                                        }
                                        echo "</td>
                                            <td>";
                                        if (isset($_GET['b']) and ($_GET['b'] != 0)) {
                                            echo "$_GET[b]";
                                        } elseif (isset($dtag['kos_makan'])) {
                                            echo "$dtag[kos_makan]";
                                        } else {
                                            echo "0";
                                        }
                                        echo "</td>
                                            <td>";
                                        if (isset($_GET['c']) and ($_GET['c'] != 0)) {
                                            echo "$_GET[c]";
                                        } elseif (isset($dtag['sekolah'])) {
                                            echo "$dtag[sekolah]";
                                        } else {
                                            echo "0";
                                        }
                                        echo "</td>
                                        </tr>
                                        ";
                                        $no++;
                                    ?>
                                    <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </form>
                </div>
                <!-- /.box-body -->
            </div>
        </section>
        <!-- /.Left col -->
    </div>
    <!-- /.row (main row) -->
