<?php
require('konek.php');
session_start();
if (empty($_SESSION['level'])) {
    echo "<script type='text/javascript'>alert(\"Login dulu!\");window.location=\"../index.php\";</script>";
}

$aksi = $_GET['t'];
if (isset($aksi) and isset($_GET['s'])) {
    $sql = "SELECT * FROM survey_set where id_survey_set='$_GET[s]'";
    $sql1 = "SELECT * FROM questions where id_survey_set='$_GET[s]'";

    $query1 = mysqli_query($kon, $sql1);
    $row1 = mysqli_fetch_array($query1);
    $num = mysqli_num_rows($query1);
    if ($num > 0) {
        $valquestion = $row1['question'];
    } else {
        $valquestion = "";
    }
    if ($aksi == 'pdf') {
        $b = "onLoad=window.print()";
        // $b = "";
    } else {
        $b = "";
        header("Content-type: application/vnd-ms-excel");
        header("Content-Disposition: attachment; filename=printlaporansurvey.xls");
    }
} else {
    echo "<script type='text/javascript'>window.location=\"survey.php\";</script>";
}
$query = mysqli_query($kon, $sql);
$row = mysqli_fetch_array($query);
$num = mysqli_num_rows($query);
if ($num < 1) {
    echo "<script type='text/javascript'>window.location=\"survey.php\";</script>";
}
?>
<?php //ob_start(); 
?>
<html>

<head>
    <title>Cetak PDF</title>
    <style>
        body {
            font-family: "Times New Roman", Times, serif;
            font-size: 12px;
        }

        table {
            border-collapse: collapse;
            /* table-layout:fixed;width: 630px; */
        }

        table th {
            padding: 5px;
            word-wrap: break-word;
            /* width: 20%; */
        }

        table td {
            padding: 5px;
            word-wrap: break-word;
            /* width: 20%; */
        }

        h4 {
            text-align: center;
            text-transform: uppercase;
        }
    </style>
</head>

<body <?php echo "$b"; ?>>
    <h2 style='text-align: center;text-transform:uppercase;'>Laporan survey</h2>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <div class="table-responsive" style="border: 0px solid #ddd;">
                        <table border='0' width='100%' align=center>
                            <tr>
                                <th width='10%'>Judul</th>
                                <th width='1%'>:</th>
                                <td><?php echo $row['title']; ?></td>
                            </tr>
                            <tr>
                                <th>Deskripsi</th>
                                <th>:</th>
                                <td><?php echo $row['description']; ?></td>
                            </tr>
                            <tr>
                                <th>Mulai</th>
                                <th>:</th>
                                <td><?php echo tgl_indo($row['start_date']); ?></td>
                            </tr>
                            <tr>
                                <th>Selesai</th>
                                <th>:</th>
                                <td><?php echo tgl_indo($row['end_date']); ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header with-border">
                    <h2 class="box-title">Daftar Responden</h2>
                </div>
                <div class="box-body">
                    <div class="table-responsive" style="border: 0px solid #ddd;">
                        <table border='1' width='100%' align=center>
                            <thead>
                                <th>No</th>
                                <th>Responden</th>
                                <th>Waktu</th>
                            </thead>
                            <tbody>
                                <?php
                                $sql = "SELECT * FROM answers as an where an.id_survey_set='$row[id_survey_set]'";
                                $query = mysqli_query($kon, $sql);
                                $no = 1;
                                while ($row = mysqli_fetch_array($query)) {
                                    echo "
                                        <tr>
                                            <td>" . $no . "</td>
                                            <td>" . $row['respon'] . "</td>
                                            <td>" . tgl_indo(substr($row['date_created'], 0, 10)) . "   " . substr($row['date_created'], 11, 10) . "</td>
                                        </tr>
                                        ";
                                    $no++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">

        <?php
        // $query = mysqli_query($kon, $sql);
        // $row = mysqli_fetch_array($query);

        $piechart = "SELECT * FROM questions as an where an.id_survey_set='$_GET[s]'";
        $querypiechart = mysqli_query($kon, $piechart);
        $nrowpiechart = mysqli_num_rows($querypiechart);
        $rowpiechart = mysqli_fetch_array($querypiechart);

        $json = json_decode($rowpiechart['question'], true);

        $s = 0;
        for ($i = 0; $i < 500; $i++) {
            if (!empty($json["components"][$i]["type"]) && $json["components"][$i]["key"] != "submit") {
                $key = $json["components"][$i]["key"];
                $s++;
            }
        }
        echo "
        <div class='col-xs-12'>
            <div class='box'>
                <div class='box-header with-border'>
                    <h3 class='box-title'>Rekap Survey</h3>
                </div>
                <div class='box-body'>
                    <div class='table-responsive' style='border: 0px solid #ddd;'>
                        <table border='1' width='100%' align=center>
                            <thead>
                            <tr>
                                <th rowspan='2'>No</th>
                                <th rowspan='2'>Responden</th>
                                <th colspan='$s'>Pertanyaan</th>
                            </tr>
                            <tr>
                            ";

        for ($i = 0; $i < 500; $i++) {
            if (!empty($json["components"][$i]["type"]) && $json["components"][$i]["key"] != "submit") {
                echo "<th>" . $json["components"][$i]["label"] . "</th>";
            }
        }

        echo "
                            </tr>
                </thead>
                <tbody>
                ";


        $danswers = "SELECT * FROM answers an where an.id_survey_set='$rowpiechart[id_survey_set]'";
        $querydanswers = mysqli_query($kon, $danswers);
        

        $no = 1;
        while ($rowdanswers = mysqli_fetch_array($querydanswers)) {
            echo "<tr>";
            echo "<td>$no</td>";
            echo "<td>$rowdanswers[respon]</td>";
            $s = 0;
            
            for ($i = 0; $i < 500; $i++) {
                if (!empty($json["components"][$i]["key"]) and $json["components"][$i]["key"] != "submit") {
                    $jsondanswers = json_decode($rowdanswers['answer'], true);
                    $key = $json["components"][$i]["key"];
                    if($json["components"][$i]["type"]=="radio"){
                        for ($z = 0; $z < 500; $z++) {
                            if (!empty($json["components"][$i]["values"][$z]["value"])) {
                                if ($json["components"][$i]["values"][$z]["value"]==$jsondanswers["data"][$key]) {
                                    echo "<td>" . $json["components"][$i]["values"][$z]["label"] . "</td>";
                                }
                            }
                        }
                    }elseif ($json["components"][$i]["type"] != "file") {
                        echo "<td>" . $jsondanswers["data"][$key] . "</td>";
                    } else {
                        // deteski posisi base64,
                        $deteksi = strpos($jsondanswers["data"][$key][0]["url"], "base64,");
                        $hasildeteksi = $deteksi + 7;
                        $img1 = substr($jsondanswers["data"][$key][0]["url"], 0, $hasildeteksi);
                        $img = str_replace($img1, '', $jsondanswers["data"][$key][0]["url"]);
                        $img = str_replace(' ', '+', $img);
                        $data = base64_decode($img);
                        $file = "images/".$jsondanswers["data"][$key][0]["name"];
                        $success = file_put_contents($file, $data);

                        $ext = pathinfo("images/".$jsondanswers["data"][$key][0]["name"], PATHINFO_EXTENSION);
                        if ($ext == 'gif' || $ext == 'png' || $ext == 'jpg'|| $ext == 'jpeg') {
                            echo "<td><img src='images/".$jsondanswers["data"][$key][0]["name"]."' width='200px'> <br> <a href='http://$_SERVER[SERVER_NAME]/sadmin/images/".$jsondanswers["data"][$key][0]["name"]."'> Download disini </a></td>";
                            // $_SERVER['SERVER_NAME']
                        }else{
                            echo"<td><a href='http://$_SERVER[SERVER_NAME]/sadmin/images/".$jsondanswers["data"][$key][0]["name"]."'> Download disini </a></td>";
                        }
                    }
                }
            }
            echo "</tr>";
            $no++;
        }
        echo "</tbody>
            </table>
        </div>
        </div>
        </div>
        </div>
        ";
        ?>
    </div>
</body>

</html>