<?php require_once('atas.php');
if (isset($_GET["d"])) {
    $no = 1;
    $d = mysqli_query($kon, "select * from jenjang where id_jenjang='$_GET[d]'");
    $dd = mysqli_fetch_array($d);
    // unlink('../files/'.$dd['foto']);
    mysqli_query($kon, "delete from jenjang where id_jenjang='$_GET[d]'");
    echo "<script type='text/javascript'>alert(\"Berhasil!\");window.location=\"djenjang.php\";</script>";
}
// elseif(isset($_GET["r"])){
//     $p=md5('jenjang');
//     $a=mysqli_query($kon,"update nis set
//     password='$p' where nis='$_GET[r]'");
//     echo "<script type='text/javascript'>alert(\"Berhasil!\");window.location=\"dnis.php\";</script>";
// }
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Dashboard
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Jenjang</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"><a href='tjenjang.php'><i class="fa fa-graduation-cap"></i></a> Daftar Jenjang Pendidikan</h3>
                </div>
                <div class="box-body">
                    <div class="table-responsive" style="border: 0px solid #ddd;">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Id Jenjang</th>
                                    <th>Nama Jenjang</th>
                                    <th>Tagihan per Bulan</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;
                                $s = mysqli_query($kon, "select * from jenjang order by id_jenjang desc");
                                while ($f = mysqli_fetch_array($s)) {
                                    echo "
                            <tr>
                                <td>$no</td>
                                <td>$f[id_jenjang]</td>
                                <td>$f[jenjang]</td>
                                <td>Rp " . number_format($f['syahriah'], 2, ',', '.') . "</td>
                                <td>
                                <a class='btn btn-info btn-xs' href='ejenjang.php?e=$f[id_jenjang]'>
                                <i class='fa fa-edit'></i></a>
                                <a class='btn btn-danger btn-xs' href='?d=$f[id_jenjang]' onclick=\"return confirm('Yakin hapus?')\">
                                <i class='fa fa-remove'></i></a>
                                </td>
                            </tr>
                            ";
                                    $no++;
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
        </section>
        <!-- /.Left col -->
    </div>
    <!-- /.row (main row) -->

</section>
<?php require_once('bawah.php'); ?>