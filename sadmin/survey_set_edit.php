<?php require_once('atas.php');
if (isset($_POST["save"])) {
  $id_survey_set = $_POST['id_survey_set'];
  $title = $_POST['title'];
  $description = $_POST['description'];
  $id_admin = $_SESSION['id_admin'];
  $kategori_survey_set = $_POST['kategori_survey_set'];
  $status_survey_set = $_POST['status_survey_set'];
  $start_date = date("Y-m-d", strtotime(substr($_POST['tgl_survey'], 0, 11)));
  $end_date = date("Y-m-d", strtotime(substr($_POST['tgl_survey'], 12, 11)));
  $date_created = date('Y-m-d H:i:s');
  $slug = str_replace(" ", "-", $title);

  $question = addslashes($_POST['components']);

  // echo "
  // title : $title<br>
  // description : $description<br>
  // id_admin : $id_admin<br>
  // start_date : $start_date<br>
  // end_date : $end_date<br>
  // slug : $slug<br>
  // kategori_survey_set : $kategori_survey_set<br>
  // status_survey_set : $status_survey_set	<br>
  // question : $question <br>
  // echo "
  // date_created : $date_created<br>
  // ";
  $sql = mysqli_query($kon, "UPDATE survey_set set
							title='$title',
							slug='$slug',
							description='$description',
							id_admin='$id_admin',
							start_date='$start_date',
							end_date='$end_date',
							date_created='$date_created',
							kategori_survey_set='$kategori_survey_set',
							status_survey_set='$status_survey_set'
							where id_survey_set='$id_survey_set'");


  $sql1 = mysqli_query($kon, "UPDATE questions set
      form_type='',
      question='$question',
      id_admin='$id_admin',
      date_created='$date_created'
      where id_survey_set='$id_survey_set' ");
  // if($sql1){
  //   echo"oke";
  // }else{
  //   $error = mysqli_error($kon);
  //   print("Error Occurred: ".$error);
  //   echo"asdokeasd";
  // }
  echo "<script type='text/javascript'>alert(\"Berhasil!\");window.location=\"survey.php\";</script>";
}

if (isset($_GET["e"])) {
  $ssurvey_set = mysqli_query($kon, "select * from survey_set where id_survey_set='$_GET[e]' ");
  $dssurvey_set = mysqli_fetch_array($ssurvey_set);

  $dtgl_survey = date("m/d/Y", strtotime($dssurvey_set['start_date'])) . " - " . date("m/d/Y", strtotime($dssurvey_set['end_date']));

  $squestions = mysqli_query($kon, "select * from questions where id_survey_set='$_GET[e]' ");
  $dsquestions = mysqli_fetch_array($squestions);
  $valquestion = $dsquestions['question'];
} else {
  echo "<script type='text/javascript'>window.location=\"survey.php\";</script>";
}
?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Dashboard
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Survey</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <!-- Main row -->
  <div class="row">
    <!-- Left col -->
    <section class="col-lg-12 connectedSortable">
      <!-- Custom tabs (Charts with tabs)-->
      <div class="box box-info">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-user-plus"></i> Edit Survey</h3>
        </div>
        <form id="idForm" method="POST" action="" enctype="multipart/form-data">
          <!-- <form action="" method="POST" enctype="multipart/form-data"> -->
          <div class="box-body">
            <div class="form-group">
              <label for="title">Judul</label>

              <input type="hidden" class="form-control" name="id_survey_set" value="<?php echo "$dssurvey_set[id_survey_set]"; ?>" required>

              <input type="text" class="form-control" name="title" value="<?php echo "$dssurvey_set[title]"; ?>" required>
            </div>
            <div class="form-group">
              <label>Deskripsi</label>

              <textarea class="textarea" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" id="description" name="description" required><?php echo "$dssurvey_set[description]"; ?></textarea>
            </div>
            <div class="form-group">
              <label>Tanggal Kuesioner </label>

              <input type="text" class="form-control" id="reservation" name="tgl_survey" value="<?php echo "$dtgl_survey"; ?>" required>
            </div>
            <div class="form-group">
              <label>Kategori Survey</label><br>
              <input type="radio" name="kategori_survey_set" class="flat-red" value="survey" <?php if ($dssurvey_set['kategori_survey_set'] == 'survey') {
                                                                                                echo "checked";
                                                                                              } ?>> Survey
              <input type="radio" name="kategori_survey_set" class="flat-red" value="PSB" <?php if ($dssurvey_set['kategori_survey_set'] == 'PSB') {
                                                                                            echo "checked";
                                                                                          } ?>> PSB
            </div>
            <div class="form-group">
              <label>Status Aktif</label><br>
              <input type="radio" name="status_survey_set" class="flat-red" value="Y" <?php if ($dssurvey_set['status_survey_set'] == 'Y') {
                                                                                        echo "checked";
                                                                                      } ?>> Ya
              <input type="radio" name="status_survey_set" class="flat-red" value="T" <?php if ($dssurvey_set['status_survey_set'] == 'T') {
                                                                                        echo "checked";
                                                                                      } ?>> Tidak
            </div>
            <div class="form-group">
              <label>Pertanyaan</label><br>

              <textarea name="components" id="components" cols="30" rows="10" hidden><?php echo $valquestion; ?></textarea>

              <div id='builder'></div>
            </div>
          </div>
          <div class="box-footer">
            <input type="submit" name='save' class="btn btn-primary" value="Simpan" onclick=\"return confirm('Yakin?')\">
            <input type="Reset" class="btn btn-danger" value="Reset">
            <a href="survey.php"><button type="button" class="btn btn-info">Lihat</button></a>
          </div>
        </form>
        <!-- /.box-body -->
      </div>
    </section>
    <!-- /.Left col -->
  </div>
  <!-- /.row (main row) -->

</section>

<script>
  var jsonElement = document.getElementById('components');

  /**
   * CARA 1
   */
  var form_fields = jsonElement.value;
  const obj = JSON.parse(form_fields);
  Formio.builder(document.getElementById('builder'), obj, {})

    .then((form) => {
      form.on('change', function() {
        jsonElement.value = JSON.stringify(form.schema);
      })
    });
</script>
<?php require_once('bawah.php'); ?>