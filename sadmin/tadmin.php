<?php require_once('atas.php');
if (isset($_POST["save"])) {
    $id_admin = time();
    $no_telp = $_POST['no_telp'];
    $username = $_POST['username'];
    $password1 = md5($_POST['password1']);
    $password2 = md5($_POST['password2']);
    $level = $_POST['level'];
    // $foto=$_POST['foto'];
    $status_admin = 'aktif';

    $file_name       = $_FILES['file']['name'];
    $explode         = explode('.', $file_name);
    $file_ext        = $explode[count($explode) - 1];
    $file_size       = $_FILES['file']['size'];
    $file_tmp        = $_FILES['file']['tmp_name'];

    $maxsize = 1024 * 1024; // maksimal 200 KB (1KB = 1024 Byte)
    $valid_ext = array('jpg', 'jpeg', 'png');
    if ($password1 == $password2) {
        if (in_array($file_ext, $valid_ext) === true) {
            if ($file_size < 10440700) {
                $t = time() . '_' . $file_name;
                $lokasi = '../files/' . $t;
                $a = mysqli_query($kon, "insert into admin values(
                        '$id_admin',
                        '$no_telp',
                        '$username',
                        '$password1',
                        '$level',
                        '$t',
                        '$status_admin')");
                if ($a) {
                    move_uploaded_file($file_tmp, $lokasi);
                    echo "<script type='text/javascript'>alert(\"Berhasil!\");window.location=\"dadmin.php\";</script>";
                } else {
                    echo "<script type='text/javascript'>alert(\"Data sudah ada!\");history.back();</script>";
                }
            } else {
                echo "<script type='text/javascript'>alert(\"Gagal upload foto!\");history.back();</script>";
            }
        } else {
            echo "<script type='text/javascript'>alert(\"Gagal upload foto!\");history.back();</script>";
        }
    } else {
        echo "<script type='text/javascript'>alert(\"Password tidak sama!\");history.back();</script>";
    }
}
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Dashboard
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Admin</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <!-- Left col -->
        <section class="col-lg-7 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-user-plus"></i> Tambah Admin</h3>
                </div>
                <form action="" method="POST" enctype="multipart/form-data">
                    <div class="box-body">
                        <div class="form-group">
                            <label>No Telepon</label>
                            <input type="text" name='no_telp' class="form-control" placeholder="Masukkan No Telepon" required>
                        </div>
                        <div class="form-group">
                            <label>Username</label>
                            <input type="username" name='username' class="form-control" placeholder="Masukkan Username" required>
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input type="text" name='password1' class="form-control" placeholder="Masukkan Password" required>
                        </div>
                        <div class="form-group">
                            <label>Konfirmasi Password</label>
                            <input type="password" name='password2' class="form-control" placeholder="Masukkan Password" required>
                        </div>
                        <div class="form-group">
                            <label>Level</label>
                            <select name='level' class="form-control select2" style="width: 100%;" required>
                                <option selected="selected" value="">Pilihan</option>
                                <option value="adminl">Admin Putra</option>
                                <option value="adminp">Admin Putri</option>
                                <option value="sadmin">Super Admin</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Foto</label>
                            <input name='file' type="file" class="form-control" placeholder="Pilih Foto" accept=".jpg,.jpeg,.png" required>
                        </div>
                    </div>
                    <div class="box-footer">
                        <input type="submit" name='save' class="btn btn-primary" value="Submit">
                        <input type="Reset" class="btn btn-danger" value="Reset">
                        <a href="dadmin.php"><button type="button" class="btn btn-info">Lihat</button></a>
                    </div>
                </form>
                <!-- /.box-body -->
            </div>
        </section>
        <!-- /.Left col -->
    </div>
    <!-- /.row (main row) -->

</section>
<?php require_once('bawah.php'); ?>